<?php
require '_modele/modele.php';
$bdd = getBdd($domaine,'site');

header('Content-Type: text/xml');
$xml = '<?xml version="1.0" encoding="UTF-8"?>';
$xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

// Page d'accueil

$xml .=  '
<url>
      <loc>'.$url_base.'</loc>
      <changefreq>daily</changefreq>
      <priority>0.9</priority>
</url>';

// Pages spéciales : à propos, aide...
$xml .=  '<url><loc>'.$url_base.'/rechercher.html</loc></url>';

$xml .=  '<url><loc>'.$url_base.'/inscription-eleve.html</loc></url>';
$xml .=  '<url><loc>'.$url_base.'/inscription-professeur.html</loc></url>';
$xml .=  '<url><loc>'.$url_base.'/compte-acceder.html</loc></url>';

$xml .=  '<url><loc>'.$url_base.'/apropos-presentation.html</loc></url>';
$xml .=  '<url><loc>'.$url_base.'/apropos-contribuez.html</loc></url>';
$xml .=  '<url><loc>'.$url_base.'/apropos-mentions.html</loc></url>';

$xml .=  '<url><loc>'.$url_base.'/plan-site.html</loc></url>';

// Liste des sujets dans les différentes catégories
$xml .=  "<url><loc>".$url_base."/philo/corriges-dissertation.html</loc></url>";
$xml .=  "<url><loc>".$url_base."/philo/corriges-commentaire.html</loc></url>";
$xml .=  "<url><loc>".$url_base."/francais/commentaires-composes.html</loc></url>";

$reponse = $bdd->query("SELECT * FROM `rubriques` WHERE `nb_ressources`!='0'");
while ($val = $reponse->fetch()) {
	$nomdecode = $val['nomdecode'];
  $type = $val['type'];
	$matiere = $val['site'];

	if ($matiere=='philo') {
		$xml .='
		<url>
			<loc>'.$url_base.'/philo/corriges-'.$type.'-'.$nomdecode.'.html</loc>
		</url>';
	}
	elseif ($matiere=='francais') {
		$xml .='
		<url>
			<loc>'.$url_base.'/francais/commentaires-composes-'.$nomdecode.'.html</loc>
		</url>';
	}
}

// Liste des liens directs vers les ressources disponibles internes
$reponse = $bdd->query("SELECT * FROM `sujets` WHERE `ressources`!='' OR `ressources_ext`!=''");

while ($val = $reponse->fetch()) {
  $id = $val['id'];
  $type = $val['type'];
  $motscles = $val['motscles'];
	$matiere = $val['site'];
	$ressources = unserialize($val['ressources']);
  $ressources_ext = unserialize($val['ressources_ext']);

  if ($ressources == '') { $ressources = []; }
  if ($ressources_ext == '') { $ressources_ext = []; }
  $ressources_all = array_merge($ressources, $ressources_ext);

	foreach ($ressources_all as $cle=>$valeur) {
		$ressources_tab[$cle]=explode('_', $ressources_all[$cle]);

		$xml .='
		<url>
			<loc>'.$url_base.'/'.$matiere.'/'.$type.'-'.$id.'-'.$motscles.'-r'.$ressources_tab[$cle][1].'.html</loc>
		</url>';
	}
}


// Fin du listing des urls et affichage de la liste
$xml .= '</urlset>';
echo $xml;
?>
