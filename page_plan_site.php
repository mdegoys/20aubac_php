<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';

$erreur = request_var('erreur', 0);
$request_uri = request_var('REQUEST_URI','');
$http_referer = request_var('HTTP_REFERER','');
$ip_visiteur = request_var('REMOTE_ADDR','');
	
$bdd = getBdd($domaine,'site');

if (isset($erreur) AND $erreur==404) {
	//Si on arrive à la récupérer, on entre url fautive et paramètres dans bdd pour comprendre soucis
	if ($request_uri!='' AND $request_uri!='/erreur-404.html') {
		
		$url_visitee = 'https://' . $http_host . $request_uri;
		
		if (isset($http_referer)) { $url_origine = $http_referer; } else { $url_origine=''; }
		$date = time();

		$reponse = $bdd->prepare("SELECT `url_visitee` FROM `erreurs404` WHERE `url_visitee` = :url_visitee AND `url_origine` = :url_origine AND `matiere`='".$matiere."'"); 
		$reponse->bindParam(':url_visitee', $url_visitee);
		$reponse->bindParam(':url_origine', $url_origine);
		$reponse->execute(); 
		$nb_resultats_erreurs = $reponse->rowCount();	

		if ($nb_resultats_erreurs==0) { 
			$reponse = $bdd->prepare("INSERT INTO `erreurs404` (`url_visitee`,`url_origine`,`nb_occurrences`,`matiere`,`date_creation`,`premiere_ip`) 
			VALUES(:url_visitee,:url_origine,'1','".$matiere."','".$date."','".$ip_visiteur."')"); 
			$reponse->bindParam(':url_visitee', $url_visitee);
			$reponse->bindParam(':url_origine', $url_origine);
			$reponse->execute(); 
		}
		else { 
			$reponse = $bdd->prepare("UPDATE `erreurs404` SET `nb_occurrences`=`nb_occurrences`+1, `date_maj`='".$date."', `derniere_ip`='".$ip_visiteur."', `corrige`='0' WHERE `url_visitee` = :url_visitee AND `url_origine` = :url_origine AND `matiere`='".$matiere."'");
			$reponse->bindParam(':url_visitee', $url_visitee);
			$reponse->bindParam(':url_origine', $url_origine);								
			$reponse->execute(); 
		}
	}
}
	
// Affichage
require '_vue/vue_plan_site.php';