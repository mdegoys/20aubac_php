<?php
$nivmembrerequis = 3;
require '_controleur/controleur_session.php';
require '_modele/modele.php';

$bdd = getBdd($domaine,'site');

// 1. Les sujets et ressources
// Nombre de sujets total
$reponse0 = $bdd->query("SELECT * FROM `sujets`");
$count0 = $reponse0->rowCount();

// Nombre de sujets total validés et donc recherchables
$reponse1 = $bdd->query("SELECT * FROM `sujets` WHERE `topic_id` !=0");
$count1 = $reponse1->rowCount();

// Nb de ressources consultables
$count2 = 0;

while ($val1 = $reponse1->fetch()) {
	if ($val1['ressources']!='') {
	    $ressources = unserialize($val1['ressources']);
	    $count2 = $count2+count($ressources);
	}
}

// Affichage
require '_vue/vue_admin_statistiques.php';
