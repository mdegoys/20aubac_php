<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_membres.php');


$submit = (isset($_POST['submit'])) ? true : false;

$pseudo = request_var('pseudo', '', true);
$email = trim(strtolower(request_var('email', '', true)));
$cgu = request_var('cgu', '', 0);

//$compte = request_var('compte', 'eleve', true);
//$niveau = request_var('niveau', '', true);
//if ($niveau!='') { $niveaumembre=$compte.'_'.$niveau; } else { $niveaumembre=$compte; }

// les 4 infos demandées en plus si le membre est un professeur
//$nom = request_var('nom', '', true);
//$prenom = request_var('prenom', '', true);
//$telephone = request_var('telephone', '', true);
//$presentation = request_var('presentation', '', true);

//$emailing = request_var('emailing', 0);

$bdd = getBdd($domaine,'site');

/* if ($submit==true) {
    $erreurs = array();

	// vérification champs remplis
	if ($pseudo=='' OR $email=='') {
	    $erreurs["champs_vides"] = "Vous devez remplir tous les champs";
	}

	// on regarde le nombre de caractères contenus dans le pseudo (doit être compris entre 3 et 20)
	$taille_pseudo = strlen($pseudo);
	if ($taille_pseudo<3 OR $taille_pseudo>20) {
	    $erreurs["taille_pseudo"] = "Votre nom d'utilisateur doit contenir entre 3 et 20 caractères";
	}

	// on vérifie que le pseudo ne contient que des caractères alpha-numériques, tirets ou underscores
	if(preg_match('/[^a-z_\-0-9]/i', $pseudo)) {
	    $erreurs["caracteres_pseudo"] = "Votre nom d'utilisateur doit uniquement contenir : des lettres sans accents, des chiffres ou les caractères - et _";
	}

	// nombre total de résultats pour le même pseudo
	$reponse = $bdd->prepare("SELECT * FROM `membres` WHERE `pseudo`=:pseudo");
	$reponse->execute(array("pseudo" => $pseudo));
	$total_pseudo = $reponse->rowCount();

	if ($total_pseudo >=1) {
	    $erreurs["unicite_pseudo"] = "Le nom d'utilisateur choisi existe déjà : veuillez en choisir un autre";
	}

	// nombre total de résultats pour le même email
	$reponse = $bdd->prepare("SELECT * FROM `membres` WHERE `email`=:email");
	$reponse->execute(array("email" => $email));
	$total_email = $reponse->rowCount();

	if ($total_email >=1) {
	    $erreurs["unicite_email"] = "Votre adresse email est déjà enregistrée : veuillez retrouver le compte associé";
	}

	// vérification validité adresse email
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
	    $erreurs["validite_email"] = "Votre adresse email n'est pas valide";
	}

	// vérification conditions acceptées
	if ($cgu==0){
	    $erreurs["cgu"] = "Les conditions d'utilisation doivent être lues et acceptées";
	}

    if (empty($erreurs)) {

		$mdp = genmdp();
		$mdp_hash = phpbb_hash($mdp);
		$date_ins = time();

		$req = $bdd->prepare("INSERT INTO membres (pseudo,mdp,email,date_ins)
		VALUES (:pseudo,:mdp,:email,:date_ins)");
		$req->execute(array(
			"pseudo" => $pseudo,
			"mdp" => $mdp_hash,
			"email" => $email,
			"date_ins" => $date_ins
			));


		// on crée le compte phpbb associé
//		require($phpbb_root_path . "includes/functions_user." . $phpEx);
//
//		$user->session_begin();
//		$auth->acl($user->data);
//		$user->setup("ucp");
//		$error=array();
//
//	    $data = array(
//	    "username" => $pseudo,
//	    "password" => $mdp,
//	    "email" => $email
//	    );
//
//	    $group_name =  "REGISTERED";
//	    $sql = 'SELECT group_id
//			   FROM ' . GROUPS_TABLE . "
//			   WHERE group_name = '" . $db->sql_escape($group_name) . "'
//		       AND group_type = " . GROUP_SPECIAL;
//	    $result = $db->sql_query($sql);
//	    $row = $db->sql_fetchrow($result);
//	    $db->sql_freeresult($result);
//	    if (!$row)
//	    {
//			trigger_error("NO_GROUP");
//	    }
//	    $group_id = $row["group_id"];
//	    $user_row = array(
//		"username" => $data["username"],
//		"user_password" => phpbb_hash($data["password"]),
//		"user_email" => $data["email"],
//		"group_id" => (int) $group_id,
//		"user_timezone" => (float) $config["board_timezone"],
//		"user_lang" => basename($user->lang_name),
//		"user_type" => USER_NORMAL,
//		"user_actkey" => "",
//		"user_ip" => $user->ip,
//		"user_regdate" => time(),
//		"user_inactive_reason" => 0,
//		"user_inactive_time" => 0,
//	    );
//
//	    $user_id = user_add($user_row);
//
//	    if ($user_id===false)
//	    {
//		   trigger_error("NO_USER", E_USER_ERROR);
//	    }
//
//	    // infos sur la fonction : http://wiki.phpbb.com/display/DEV/Function.group+user+add
//	    if ($compte=='eleve') {	$user_group = group_user_add(10, $user_id, false, false, true); }
//		elseif ($compte=='professeur') { $user_group = group_user_add(9, $user_id, false, false, true); }

	    $message="Bonjour,\n";
	    $message.="Merci pour votre inscription sur 20aubac !\n\n";

	    $message.="Voici vos identifiants de connexion :\n\n";

	    $message.="Nom d'utilisateur : ".$pseudo."\n";
	    $message.="Mot de passe : ".$mdp."\n\n";

//	    if ($compte=='eleve') {
//			$message.="Vous pouvez ainsi directement à l'aide de vos identifiants :\n";
//			$message.="- <a href=\"".$url_base."/membres-ajouterressource.html\">Ajouter une ressource pour accéder à celles des autres gratuitement</a>\n\n";
//	    }
//
//	    else if ($compte=='professeur') {
//			$message.="Attention : vous devez encore valider votre compte professeur en soumettant un document justifiant votre statut, en réponse à cet email ou depuis votre espace membre.\n";
//			$message.="Sans action de votre part, votre compte sera supprimé au bout de 30 jours.\n\n";
//
//			$message.="De même nous vous invitons à créer un compte dès maintenant chez notre partenaire Rentabiliweb afin d'être rémunéré en droits d'auteurs lors de la publication de corrigés.\n\n";
//		}

	    $message.="Merci à vous et à bientôt,\n";
	    $message.="L'équipe de 20aubac\n\n";

	    $message .= 'P.-S. : Pensez à noter votre mot de passe et à supprimer cet email pour des raisons de sécurité.'."\n";
		$message .= 'Vous pouvez générer un nouveau mot de passe à tout instant depuis cette page : '.$url_base.'/compte-recuperer.html';

		envoi_email($email,"Compte créé sur 20aubac",$message);
	}
}*/

// Affichage
require '_vue/vue_compte_creer.php';
