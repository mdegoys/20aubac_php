<?php
/*
Principe de la page :
Ajouter un corrigé/commentaire externe sur un nouveau sujet ou un sujet existant
Il suffit d'indiquer une url, le nom de l'auteur et le niveau du professeur

A implémenter par la suite : ouverture de la fonction à tous les membres avec rémunération en crédits pour élèves + modération du contenu suggéré
*/

$nivmembrerequis = 3;
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_ressources.php');

$bdd = getBdd($domaine,'site');

$ressource_id = request_var('ressource_id', 0);
$sujet_id = request_var('sujet_id', 0);

// On récupère les infos non modifiables sur la ressource/le sujet
if ($ressource_id!=0) {

	$reponse = $bdd->query("SELECT * FROM `ressources` WHERE id = '".$ressource_id."'");
	while ($val = $reponse->fetch()) {
	$etat = $val['etat'];
	$sujet_id = $val['sujet_id'];
	$total_notes = $val['total_notes'];
	$total_votes = $val['total_votes'];

		$reponse2 = $bdd->query("SELECT * FROM `sujets` WHERE id = '".$sujet_id."'");
		while ($val2 = $reponse2->fetch()) {
			$sujet = $val2['sujet'];
			$ressources_ext = unserialize($val2['ressources_ext']);
			$type = $val2['type'];
			$topic_id = $val2['topic_id'];
			$matiere = $val2['matiere'];
		}
	}
}
else if ($sujet_id!=0) {
	$etat = 1;

	$reponse = $bdd->query("SELECT * FROM `sujets` WHERE id = '".$sujet_id."'");
	while ($val = $reponse->fetch()) {
		$ressources_ext = unserialize($val["ressources_ext"]);
		$type = $val['type'];
		$topic_id = $val['topic_id'];
		$matiere = $val['matiere'];
	}
}
else {
	$type = request_var('type', 'dissertation', true);
	$matiere = request_var('matiere', 'philo', true);
	$etat = 1;
	$topic_id = 0;
}

$submit = (isset($_POST['submit'])) ? true : false;

$sujet = request_var('sujet', '', true);
$textesource = request_var('textesource', '', true);
$oeuvre = request_var('oeuvre', '', true);
$passage = request_var('passage', '', true);
$themes = request_var('themes', '', true);
$edition = request_var('edition', '', true);
$rubrique_autre = request_var('rubrique_autre', '', true);
$rubrique = request_var('rubrique', '', true);
if ($rubrique!='autre') $rubrique_autre ='';
$url = request_var('url', '', true);
$auteur = request_var('auteur', '', true);
$niveau_auteur = request_var('niveau_auteur', '', true);

$sujet2 = request_var('sujet2', '', true);
$textesource2 = request_var('textesource2', '', true);
$oeuvre2 = request_var('oeuvre2', '', true);
$passage2 = request_var('passage2', '', true);
$themes2 = request_var('themes2', '', true);
$edition2 = request_var('edition2', '', true);
$rubrique_autre2 = request_var('rubrique_autre2', '', true);
$rubrique2 = request_var('rubrique2', '', true);
if ($rubrique2!='autre') $rubrique_autre2 ='';
$url2 = request_var('url2', '', true);
$auteur2 = request_var('auteur2', '', true);
$niveau_auteur2 = request_var('niveau_auteur2', '', true);

$motscles = request_var('motscles', '', true);
$annale = request_var('annale', '', true);

$note_admin = request_var('note_admin', '', true);


if ($submit==true) {
	// En cas de submit, on teste la validité des champs soumis
	$erreurs = array();

	 // Si les champs sujets sont désactivés (disabled) dans le cas d'un sujet déjà validé, on a pas besoin de les vérifier
	if ($topic_id!=0) {
		if	(
				($ressource_id==0 AND $url=='')
			OR ($ressource_id!=0 AND $url2=='')
			)	{
		   $erreurs["champs_vides"] = "Vous devez remplir tous les champs non déjà pré-remplis";
		}
	}
	else if  // Sinon on distingue les 6 cas : type (dissertation/commentaire) sujet existant (oui/non), ressource existante (oui/non)
		(
		($type=='dissertation' AND $sujet_id==0 AND ($sujet=='' OR $url=='' OR !isset($rubrique) OR $rubrique==''))
	OR ($type=='dissertation' AND $sujet_id!=0 AND $ressource_id==0 AND ($sujet2=='' OR $url=='' OR !isset($rubrique2) OR $rubrique2==''))
	OR ($type=='dissertation' AND $sujet_id!=0 AND $ressource_id!=0 AND ($sujet2=='' OR $url2=='' OR !isset($rubrique2) OR $rubrique2==''))
  	OR ($type=='commentaire' AND $sujet_id==0 AND ($url=='' OR $textesource=='' OR !isset($rubrique) OR $rubrique=='' OR ($rubrique=='autre' AND $rubrique_autre=='')))
	OR ($type=='commentaire' AND $sujet_id!=0 AND $ressource_id==0 AND($url=='' OR $textesource2=='' OR !isset($rubrique2) OR $rubrique2=='' OR ($rubrique2=='autre' AND $rubrique_autre2=='')))
	OR ($type=='commentaire' AND $sujet_id!=0 AND $ressource_id!=0 AND($url2=='' OR $textesource2=='' OR !isset($rubrique2) OR $rubrique2=='' OR ($rubrique2=='autre' AND $rubrique_autre2=='')))
	)
	{ $erreurs["champs_vides"] = "Vous devez remplir tous les champs"; }

	// On teste la longueur du titre (maximum défini = 146)
	if ($topic_id==0) {
	    if (
		($type=='dissertation' AND $sujet_id==0 AND strlen($sujet)>146)
		OR ($type=='dissertation' AND $sujet_id!=0 AND strlen($sujet2)>146)
		) {
		    $erreurs['titre_troplong'] = 'Le titre de votre sujet est trop long, veuillez le réduire';
		}
	    else if (
			($type=='commentaire' AND $sujet_id==0 AND (strlen($rubrique)+strlen($rubrique_autre)+strlen($oeuvre)+strlen($passage)+strlen($themes))>138)
			OR ($type=='commentaire' AND $sujet_id!=0 AND (strlen($rubrique2)+strlen($rubrique_autre2)+strlen($oeuvre2)+strlen($passage2)+strlen($themes2))>138)
			) {
		    $erreurs['titre_troplong'] = 'Le titre de votre sujet est trop long, veuillez réduire un ou plusieurs des éléments suivants : nom de l\'auteur, de l\'oeuvre, passage et thèmes de l\'extrait';
		}
	}

	// On teste le format de l'URL
	if (($ressource_id==0 AND !filter_var($url, FILTER_VALIDATE_URL)) OR ($ressource_id!=0 AND !filter_var($url2, FILTER_VALIDATE_URL))) {
        $erreurs['format_url'] = 'L\'adresse/URL soumise n\'a pas le bon format :  merci de la corriger';
	}

	// Pour un nouvelle URL/URL modifiée on teste si elle est pas djà présente
	if ($ressource_id==0) {
		$reponse = $bdd->prepare("SELECT * FROM `ressources` WHERE `url`=:url");
	    $reponse->execute(array("url" => $url));
	}
	elseif ($ressource_id!=0) {
		$reponse = $bdd->prepare("SELECT * FROM `ressources` WHERE `url`=:url AND `id`!='".$ressource_id."'");
	    $reponse->execute(array("url" => $url2));
	}
	$total_url = $reponse->rowCount();

	if ($total_url >=1) {
	    $erreurs['unicite_url'] = 'L\'adresse/URL est déjà présente dans la base de donnée';
	}

	// Si tout est bon on distingue les 3 cas :	1/ création d'1 sujet/URL, 2/ modification d'1 sujet/création d'1 URL, 3/ modification d'1 sujet/d'1 URL
	if (empty($erreurs)) {

		//Premier cas : création d'un sujet/ressource
		if ($sujet_id==0) {
			$date_ajout = time();

			// Dans le cas d'un commentaire, on constitue le titre du sujet
			if ($type=='commentaire') {
				// Si thèmes non indiqués, on prend les premiers mots du texte étudié
				if ($themes=='')  { $themes = "«".resume_xmots($textesource,"5")."...»"; }
			    $sujet = creation_sujet($rubrique,$rubrique_autre,$oeuvre,$passage,$themes,$textesource);
			}

			$motscles = motscles($sujet);

			// On créé le sujet dans la base de données...
			$req = $bdd->prepare("INSERT INTO sujets (sujet,matiere,motscles,annale,type,date_ajout,textesource,oeuvre,passage,themes,edition,rubrique,rubrique_autre)
			VALUES (:sujet,:matiere,:motscles,:annale,:type,:date_ajout,:textesource,:oeuvre,:passage,:themes,:edition,:rubrique,:rubrique_autre)");
			$req->execute(array(
				'sujet' => $sujet,
				'matiere' => $matiere,
				'motscles' => $motscles,
				'annale' => $annale,
				'type' => $type,
				'date_ajout' => $date_ajout,
				'textesource' => $textesource,
				'oeuvre' => $oeuvre,
				'passage' => $passage,
				'themes' => $themes,
				'edition' => $edition,
				'rubrique' => $rubrique,
				'rubrique_autre' => $rubrique_autre
				));

			$sujet_id = $bdd->lastInsertId();

			// ...puis on créé la ressource avec les 3 infos données...
			$etat = 2;

			$req = $bdd->prepare('INSERT INTO `ressources` (sujet_id,url,date_ajout,auteur,niveau_auteur,etat)
			VALUES (:sujet_id,:url,:date_ajout,:auteur,:niveau_auteur,:etat)');
			$req->execute(array(
				'sujet_id' => $sujet_id,
				'url' => $url,
				'date_ajout' => $date_ajout,
				'auteur' => $auteur,
				'niveau_auteur' => $niveau_auteur,
				'etat' => $etat
				));

			// ... et enfin on récupère l'id de la ressource
			$ressource_id = $bdd->lastInsertId();

			// On prévient du délai et possibilité de modifier URL/sujet entre-temps si besoin est
			$message_confirmation=  'Votre URL (id : '.$ressource_id.') et sujet (id : '.$sujet_id.') associés ont bien été enregistrés et seront validés sous 72h.';

		// Deuxième cas : création d'1 ressource (mais sujet déjà existant)
		} else if ($sujet_id!=0 AND $ressource_id==0) {

			$date_ajout = time();
			$date_maj = time();

			// on ne modifie données du sujet que si celui-ci n'a pas déjà été publié (isset($topic_id) AND ($topic_id==0 OR $topic_id==""))
			if ($topic_id==0) {

				//Dans le cas d'un commentaire, on constitue le titre du sujet
				if ($type=='commentaire') {
				$sujet2 = creation_sujet($rubrique2,$rubrique_autre2,$oeuvre2,$passage2,$themes2,$textesource2);}

				if ($motscles=='') { $motscles=motscles($sujet2); }

				if ($type=='dissertation') {
				$req = $bdd->prepare("UPDATE `sujets` SET sujet = :sujet,  rubrique = :rubrique, motscles = :motscles, date_maj = :date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
				$req->execute(array(
					'sujet' => $sujet2,
					'rubrique' => $rubrique2,
					'motscles' => $motscles,
					'date_maj' => $date_maj
					));

				} else if ($type=='commentaire') {
				$req = $bdd->prepare("UPDATE `sujets` SET sujet =:sujet, rubrique =:rubrique, rubrique_autre =:rubrique_autre, textesource =:textesource, oeuvre =:oeuvre, passage =:passage, themes =:themes, edition =:edition, motscles =:motscles, annale =:annale, date_maj =:date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
				$req->execute(array(
					'sujet' => $sujet2,
					'rubrique' => $rubrique2,
					'rubrique_autre' => $rubrique_autre2,
					'textesource' => $textesource2,
					'oeuvre' => $oeuvre2,
					'passage' => $passage2,
					'themes' => $themes2,
					'edition' => $edition2,
					'motscles' => $motscles,
					'annale' => $annale,
					'date_maj' => $date_maj
					));
				}
			}

			// On met à jour également le champ rubrique à la condition expresse que celui-ci n'ait pas déjà été renseigné (on devrait l'avoir récupéré dès lors dans le formulaire)
			if ($rubrique2!='') { $bdd->exec("UPDATE `sujets` SET `rubrique` = IF (rubrique = '', '".$rubrique2."', rubrique) WHERE `sujets`.`id` ='".$sujet_id."'"); }

			$etat = 2;
			$req = $bdd->prepare("INSERT INTO `ressources` (sujet_id,url,date_ajout,auteur,niveau_auteur,etat)
			VALUES (:sujet_id,:url,:date_ajout,:auteur,:niveau_auteur,:etat)");
			$req->execute(array(
				'sujet_id' => $sujet_id,
				'url' => $url,
				'date_ajout' => $date_ajout,
				'auteur' => $auteur,
				'niveau_auteur' => $niveau_auteur,
				'etat' => $etat
				));

			// Une fois l'URL créée, on le confirme par un message.
			$ressource_id = $bdd->lastInsertId();
			$message_confirmation = 'Vos données ont bien été enregistrées - nouvelle id ressource : '.$ressource_id.'.';

		// 3. Troisième cas : modification d'1 URL déjà existante et éventuellement validation par l'admin
		} else if ($sujet_id!=0 AND $ressource_id!=0) {

			// 3.1 On modifie la base de donnée avec les infos renseignées (quelque soit le cas, même si pas d'acceptation/refus => cas traités en 2)
			$date_maj = time();

			// On met à jour la ressource
			$req = $bdd->prepare('UPDATE `ressources` SET `url` =:url, `auteur` =:auteur, `niveau_auteur` =:niveau_auteur, `date_maj` =:date_maj WHERE `ressources`.`id` =\''.$ressource_id.'\'');
			$req->execute(array(
				'url' => $url2,
				'auteur' => $auteur2,
				'niveau_auteur' => $niveau_auteur2,
				'date_maj' => $date_maj
			));

			// On ne modifie données du sujet que si celui-ci n'a pas déjà été publié
			if ($topic_id==0) {

				//Dans le cas d'un commentaire, on constitue le titre du sujet
				if ($type=='commentaire') {
				    $sujet2 = creation_sujet($rubrique2,$rubrique_autre2,$oeuvre2,$passage2,$themes2,$textesource2);
				}

				if ($motscles=='') { $motscles = motscles($sujet2);	}

				if ($type=='dissertation') {
					$req = $bdd->prepare('UPDATE `sujets` SET sujet = :sujet,  rubrique = :rubrique, motscles = :motscles, annale = :annale, date_maj = :date_maj WHERE `sujets`.`id` =\''.$sujet_id.'\'');
					$req->execute(array(
						'sujet' => $sujet2,
						'rubrique' => $rubrique2,
						'motscles' => $motscles,
						'annale' => $annale,
						'date_maj' => $date_maj
					));

				} else if ($type=='commentaire') {
					$req = $bdd->prepare('UPDATE `sujets`  SET sujet = :sujet,  rubrique = :rubrique, rubrique_autre =:rubrique_autre, textesource =:textesource, oeuvre = :oeuvre, passage = :passage, themes = :themes, edition = :edition, motscles = :motscles, annale = :annale, date_maj = :date_maj WHERE `sujets`.`id` =\''.$sujet_id.'\'');
					$req->execute(array(
						'sujet' => $sujet2,
						'rubrique' => $rubrique2,
						'rubrique_autre' => $rubrique_autre2,
						'textesource' => $textesource2,
						'oeuvre' => $oeuvre2,
						'passage' => $passage2,
						'themes' => $themes2,
						'edition' => $edition2,
						'motscles' => $motscles,
						'annale' => $annale,
						'date_maj' => $date_maj
				));
				}
			}
			// On met à jour également le champ rubrique à la condition expresse que celui-ci n'ait pas déjà été renseigné (on devrait l'avoir récupéré dès lors dans le formulaire)
			if ($rubrique2!='') { $bdd->exec('UPDATE `sujets` SET `rubrique` = IF (rubrique = \'\', \''.$rubrique2.'\', rubrique) WHERE `sujets`.`id` =\''.$sujet_id.'\''); }

			$message_confirmation = 'La modification a bien été enregistrée.';

			// On détermine l'état de la ressource, par défaut elle reste la même...
			$etat2 = $etat;

			// ... et en fonction d'une nouvelle note éventuelle on détermine le nouvel état (etat2)
			if ($nivmembre==3 AND $note_admin!='') {
					$total_notes = $note_admin*3;
					$total_votes = 3;

				// on regarde si critère d'acceptation ou refus remplis	et on change etat
				if ($note_admin<=2) { $etat2 = 1; } else if ($note_admin>=3) { $etat2 = 3; }
			}

			// 3.2 On regarde si acceptation ou refus et on envoie email + créé topic + crédite compte si acceptation OU on envoie juste email si refus
			if (($etat2==3 AND $etat==2) OR ($etat2==1 AND $etat==2)) {

				// Quelque que soit le cas (acceptation ou refus) on modifie les données de la ressource liés à la modération/notation/etat/extrait/date_validation

				$req = $bdd->prepare('UPDATE `ressources` SET url =:url, note_admin =:note_admin, total_notes = :total_notes, total_votes = :total_votes, date_maj =:date_maj, etat = :etat WHERE `ressources`.`id` =\''.$ressource_id.'\'');
				$req->execute(array(
					'url' => $url2,
					'note_admin' => $note_admin,
					'total_notes' => $total_notes,
					'total_votes' => $total_votes,
					'date_maj' => $date_maj,
					'etat' => $etat2
				));

				// 3.2 a) On accepte l'URL
				if ($etat2==3 AND $etat==2)  {

				    // On met à jour la table sujets avec la nouvelle ressource ajoutée
					// On rajoute la nouvelle id en début de tableau pour ressource professeur, en fin pour ressource élève
					if ($niveau_auteur2=='professeur' AND is_array($ressources_ext)) { array_unshift($ressources_ext,'professeur_'.$ressource_id); }
					else { $ressources_ext[] = $niveau_auteur2.'_'.$ressource_id; }

					if (isset($ressources_ext) AND $ressources_ext!='') {
						$req = $bdd->prepare('UPDATE `sujets` SET ressources_ext = :ressources_ext WHERE `sujets`.`id` =\''.$sujet_id.'\'');
						$req->execute(array('ressources_ext' => serialize($ressources_ext)));
					}

					// On crée le topic correspondant et on met à jour la table sujets
					if ($topic_id==0) {

						$date_valid=$date_maj;

						$bdd->exec('UPDATE `sujets` SET `date_valid`=\''.$date_valid.'\' WHERE `id`=\''.$sujet_id.'\'');
					}
				}

				// 3.2 b) On refuse l'URL => cas déjà compris en 3.2, rien à ajouter

				// On confirme le changement d'état
				if (isset($confirmation_admin)) {
					$confirmation_admin.='Etats (etat1 => etat2) :'.$etat.' => '.$etat2;
				} else {
					$confirmation_admin='Etats (etat1 => etat2) :'.$etat.' => '.$etat2;
				}

			} // Fin cas acceptation ou refus
		} // FIN des trois cas : création sujet ou création ressource ou modification ressource
	} // FIN du cas $s==0 (pas d'erreur)
} // FIN du submit

if (isset($sujet_id) AND $sujet_id!=0) {
	$reponse = $bdd->query('SELECT * FROM sujets WHERE id = \''.$sujet_id.'\'');
	while ($val = $reponse->fetch()) {
		$sujet = $val['sujet'];
		$motscles = $val['motscles'];
		$rubrique = $val['rubrique'];
		$rubrique_autre = $val['rubrique_autre'];
		$annale = $val['annale'];
		$textesource = $val['textesource'];
		$oeuvre = $val['oeuvre'];
		$passage = $val['passage'];
		$themes = $val['themes'];
		$edition = $val['edition'];
	}
}

if ($ressource_id!=0) {
	$reponse = $bdd->query('SELECT * FROM `ressources` WHERE id = \''.$ressource_id.'\'');
	while ($val = $reponse->fetch()) {
		$url = $val['url'];
		$auteur = $val['auteur'];
		$niveau_auteur = $val['niveau_auteur'];
		$note_admin = $val['note_admin'];
	}
}

// Affichage
require '_vue/vue_admin_ajouterurl.php';
