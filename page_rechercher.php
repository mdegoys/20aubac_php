<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_ressources.php');

$matiere = request_var('m', '', true);
$recherche = request_var('r', '', true);
$page_resultat = request_var('p', 1);

// on determine debut du limit
if ($recherche!='') {
    $nb_aff = 10;
    $debut = ($page_resultat - 1) * $nb_aff;
}

$bdd = getBdd($domaine,'site');

if ($recherche!='') {

//1. traitement requête et préparation recherche

	// on enlève tous les accents
	$mots = minusculesSansAccents ($recherche);

	// on enlève les "slashes" et les espaces entourant notre chaîne de caractères
	$mots=stripslashes(trim($mots));

	// on remplace les signes spéciaux par des espaces
	$mots = str_replace('\\', ' ', $mots);
	$mots = str_replace('(', ' ', $mots);
	$mots = str_replace('^', ' ', $mots);
	$mots = str_replace('$', ' ', $mots);
	$mots = str_replace(')', ' ', $mots);
	$mots = str_replace('|', ' ', $mots);
	$mots = str_replace('*', ' ', $mots);
	$mots = str_replace(' ?', '', $mots);
	$mots = str_replace('?', '', $mots);
	$mots = str_replace('!', ' ', $mots);
	$mots = str_replace('"', ' ', $mots);
	$mots = str_replace('\'', ' ', $mots);
	$mots = str_replace('[', ' ', $mots);
	$mots = str_replace(']', ' ', $mots);
	$mots = str_replace('{', ' ', $mots);
	$mots = str_replace('}', ' ', $mots);
	$mots = str_replace('.', ' ', $mots);
	$mots = str_replace(':', ' ', $mots);
	$mots = str_replace(';', ' ', $mots);
	$mots = str_replace('«', ' ', $mots);
	$mots = str_replace('»', ' ', $mots);
	$mots = str_replace('<', ' ', $mots);
	$mots = str_replace('>', ' ', $mots);
	$mots = str_replace('-', ' ', $mots);
	$mots = str_replace('+', ' ', $mots);

	$mots = str_replace(' et ', ' ', $mots);
	$mots = str_replace(' le ', ' ', $mots);
	$mots = str_replace(' la ', ' ', $mots);
	$mots = str_replace(' les ', ' ', $mots);
	$mots = str_replace(' l ', ' ', $mots);
	$mots = str_replace('   ', ' ', $mots);
	$mots = str_replace('  ', ' ', $mots);

	// on place les differents mots dans un tableau
	/* $tab = explode(' ', $mots); */

	// on compte le nb d'élément du tableau.
	/* $nb_mots = count($tab); */


//2. On regarde le nombre de résultat pour chaque matiere et on force celle-ci si elle obtient tous les résultat et qu'aucune matiere n'avait été déjà choisiz
$resultat_mysql_francais = rechercheSujets($mots,'francais');
$nb_resultats_francais = $resultat_mysql_francais ->rowCount();

$resultat_mysql_philo = rechercheSujets($mots,'philo');
$nb_resultats_philo = $resultat_mysql_philo ->rowCount();

if ($nb_resultats_francais==0 AND $nb_resultats_philo>0 AND $matiere=='') { $matiere = 'philo'; $nb_resultats = $nb_resultats_philo; }
else if ($nb_resultats_francais>0 AND $nb_resultats_philo==0 AND $matiere=='') { $matiere = 'francais'; $nb_resultats = $nb_resultats_francais; }
else {
	$resultat_mysql = rechercheSujets($mots,'');
	$nb_resultats = $resultat_mysql ->rowCount();
}


// 3. Insertion recherche dans base de données des résultats (ou incrémentation si entrée déjà existante) de la recherche pour statistiques
if ($nb_resultats<500) {
$reponse = $bdd->prepare("SELECT termes FROM `recherches` WHERE `termes` LIKE :termes AND `site`='".$matiere."'");
$reponse->bindParam(':termes', $mots); $reponse->execute(); $nb_resultats_recherches = $reponse->rowCount();

if ($nb_resultats_recherches==0) { $reponse = $bdd->prepare("INSERT INTO `recherches` (`termes`,`nb`,`reponses`,`site`) VALUES(:termes,'1','".$nb_resultats."','".$matiere."')");
$reponse->bindParam(':termes', $mots); $reponse->execute(); }

else { $bdd->prepare("UPDATE `recherches` SET `nb`=`nb`+1, `reponses`=".$nb_resultats." WHERE `termes` LIKE :termes AND `site`='".$matiere."'");
$reponse->bindParam(':termes', $mots); $reponse->execute(); }}
}

// Affichage
require '_vue/vue_rechercher.php';
