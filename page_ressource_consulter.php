<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_ressources.php');

// On récupère les infos de matiere d'id ressource ou de l'action soumettre (pour soumettre un brouillon)
$matiere = request_var('matiere', '', true);
$ressource_id = request_var('ressource_id', 0);
$soumettre = request_var('soumettre', 0);

$bdd = getBdd($domaine,'site');

/* Si ressource demandée on récupère infos liées */
if ($ressource_id!=0) {

	$reponse = $bdd->query('SELECT * FROM `ressources` WHERE id = \''.$ressource_id.'\'');
	while ($val = $reponse->fetch()) {
		$ressource_id = $val['id'];
		$sujet_id = $val['sujet_id'];
		$ressource = $val['ressource'];
		$url = $val['url'];
		$description = $val['description'];
		$nb_mots = $val['nb_mots'];
		$auteur = $val['auteur'];
    $niveau_auteur = $val['niveau_auteur'];
    $nb_consultations = $val['nb_consultations'];
		$total_notes = $val['total_notes'];
		$total_votes = $val['total_votes'];
		$date_maj_ressource = $val['date_maj'];
		$date_ajout_ressource = $val['date_ajout'];
		$etat = $val['etat'];
	}

	$reponse = $bdd->query('SELECT * FROM sujets WHERE id = \''.$sujet_id.'\'');
	while ($val = $reponse->fetch()) {
		$sujet = $val['sujet'];
		$type = $val['type'];
		$motscles = $val['motscles'];
		$matiere = $val['site'];
		$rubrique = $val['rubrique'];
		$rubrique_autre = $val['rubrique_autre'];
		$annale = $val['annale'];
		$textesource = $val['textesource'];
		$oeuvre = $val['oeuvre'];
		$passage = $val['passage'];
		$themes = $val['themes'];
		$edition = $val['edition'];
		$nb_hits = $val['nb_hits'];
		$date_ajout_sujet = $val['date_ajout'];
		$date_maj_sujet = $val['date_maj'];
		$ressources = unserialize($val['ressources']);
		$ressources_ext = unserialize($val['ressources_ext']);
		$topic_id = $val['topic_id'];
	}


	/* Test des droits d'accès à la page :
	Si la ressource est à l'état de brouillon et que l'on est ni admin ni auteur
	=> on redirige vers page du sujet*/
	if ($nivmembre!=3 AND $pseudomembre!=$auteur AND $etat<3) {
		header('Location: '.$url_base.'/'.$matiere.'/'.$type.'-'.$sujet_id.'-'.$motscles.'.html');
	}

	// Statistiques : on augmente nombre de consultations par 1 (si pas déjà session visite pour ce sujet_id existante)
	if (!isset(${'consultation_'.$ressource_id}) OR ${'consultation_'.$ressource_id}!=1) {
		$_SESSION['consultation_'.$ressource_id] = 1;
		$bdd->exec("UPDATE `ressources` SET nb_consultations = nb_consultations +1,  nb_consultations_30j = nb_consultations_30j +1 WHERE id='".$ressource_id."'");

		// $peutvoter = 1;
	}

	else if (!isset(${'vote_'.$ressource_id}) OR ${'vote_'.$ressource_id}!=1) {
		// $peutvoter = 1;
	}

	// On teste si soumission et on change l'état de la ressource dans ce cas (clauses sur auteur et état rajoutées pour éviter de soumettre ressources des autres, ou ressources pas en brouillon)
	if ($soumettre==1) {
		$reponse = $bdd->query("UPDATE `ressources`
		SET  `etat` = '2', `note_admin` = '0', `total_notes` = '0', `total_votes` = '0', `refus`='', `refus_autre`=''
		WHERE  `ressources`.`id` ='".$ressource_id."' AND `auteur`='".$pseudomembre."' AND `etat`='1'");

		// On s'envoie un email
		$message = 'Bonjour,'."\n\n";

		$message .= 'Une ressource vient d\'être proposée en '.$matiere."\n";
		$message .= 'id de la ressource : '.$ressource_id."\n";
		$message .= 'Titre de la ressource : '.$sujet."\n";
		$message .= 'Auteur de la ressource : '.$auteur.' ('.$niveau_auteur.')'."\n\n";

		$message .= $url_base."\n\n";

		envoi_email('contact@20aubac.fr','Ressource proposée - '.$matiere,$message);

		// On change l'état pour l'affichage de la page
		$etat = 2;
	}


} // Fin $ressource_id défini

/*
Test sur présence des id nécessaires ou sur existence de ces ids dans bdd
Si ressource_id==0 OU le titre du sujet est vide (= le sujet ne correspond à rien ou présente un soucis de données)
=> on redirige vers page des ressources*/
if ($ressource_id==0 OR $sujet=='') {
	header('Location: '.$url_base.'/philo/corriges-dissertation.html');
}

// Affichage
require '_vue/vue_ressource_consulter.php';
