<?php
include ('config.php');

$page = request_var('page', 'accueil', true);

// Si la page n'existe pas on redirige vers erreur 404, sinon on redirige vers celle-ci
if (file_exists('page_'.$page.'.php')) {
	include ('page_'.$page.'.php');
}
else if (file_exists('pagesolo_'.$page.'.php')) {
    include ('pagesolo_'.$page.'.php');
}
else {
    header('Location:'.$url_base.'/erreur-404.html');
}
?>
