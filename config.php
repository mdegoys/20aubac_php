<?php
// HTTP_HOST = Contenu de l'en-tête Host: de la requête courante, si elle existe (Source : php.net)
$tab_host = explode ('.', $_SERVER['HTTP_HOST']);
$domaine = $tab_host[0];

// Predefined path.
define('CHEMIN_SCRIPT', '/srv/data/web/vhosts/'.$domaine.'.20aubac.fr/htdocs');

$url_base = 'https://'.$domaine.'.20aubac.fr';

$groupe_commentaire_francais = array('XVIème siècle', 'XVIIème siècle', 'XVIIIème siècle','XIXème siècle','XXème siècle');
$groupe_commentaire_philo = array('Antiquité', 'Philosophie médievale', 'Renaissance','17ème siècle','18ème siècle','19ème siècle','20ème siècle');
$groupe_dissertation_philo = array('Le sujet', 'La culture', 'La connaissance et la raison','La politique','La morale');

$couleurs = array('standard' => '#3366FF', 'philo' => '#6666FF', 'francais' => '#FF0040');

define('IN_PHPBB', true);
$phpbb_root_path =  CHEMIN_SCRIPT .'/forum/';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Pour afficher les erreurs et avertissements dans la version test :
if ($domaine=='test') {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}
