<?php
/*
Created:        Aug 2006
Last Mod:       Mar 18 2007
This page handles the database update if the user
does NOT have Javascript enabled.
---------------------------------------------------------
ryan masuga, masugadesign.com
ryan@masugadesign.com
Licensed under a Creative Commons Attribution 3.0 License.
http://creativecommons.org/licenses/by/3.0/
See readme.txt for full credit details.
--------------------------------------------------------- */

header('Cache-Control: no-cache');
header('Pragma: nocache');

require '_modele/modele.php';
$bdd = getBdd($domaine,'site');

//getting the values
$vote_sent = request_var('j', 0);
$id_sent = request_var('q', 0);
$units = request_var('c', 0);
$peutvoter = request_var('v', 0);

if ($vote_sent > $units) die('Désolé mais le vote semble ne pas être valide.'); // kill the script because normal users will never see this.

//connecting to the database to get some information
$reponse = $bdd->query("SELECT sujet_id, total_votes, total_notes FROM `ressources` WHERE id='".$id_sent."' ");
while ($val = $reponse->fetch()) {
	$sujet_id = $val['sujet_id'];
	$count = $val['total_votes']+3; //how many votes total
	$current_rating = $val['total_notes']; //total number of rating added together and stored
	$sum = $vote_sent+$current_rating; // add together the current vote value and the total vote value
	$tense = ($count==1) ? 'vote' : 'votes'; //plural form votes/vote
}

$reponse = $bdd->query("SELECT type, motscles, site FROM `sujets` WHERE id='".$sujet_id."' ");
while ($val = $reponse->fetch()) {
	$type = $val['type'];
	$motscles = $val['motscles'];
	$matiere = $val['site'];
}

// checking to see if the first vote has been tallied
// or increment the current number of votes
($sum==0 ? $added=0 : $added=$count-2);

// if it is an array i.e. already has entries then push in another value
if ($peutvoter==1) {

	// $reponse = $bdd->query("SELECT * FROM `membres` WHERE `pseudo`='".$pseudomembre."'");
	// while ($val = $reponse->fetch()) {
	// 	$aconsulte = unserialize($val['aconsulte']);
	// }

	if (($vote_sent >= 1 && $vote_sent <= $units)) { // keep votes within range
		// $aconsulte[''.$id_sent.''] = $vote_sent;

	  $bdd->exec("UPDATE `ressources` SET `total_votes`='".$added."', `total_notes`='".$sum."' WHERE id='".$id_sent."'");
		// $bdd->exec("UPDATE `membres` SET `aconsulte` = '".addslashes(serialize($aconsulte))."' WHERE `pseudo`='".$pseudomembre."'");

	}
} //end for the "if ($peutvoter=="1")"

header('Location: '.$matiere.'/'.$type.'-'.$sujet_id.'-'.$motscles.'-r'.$id_sent.'.html');
?>
