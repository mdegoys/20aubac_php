/**
 * autosave plugin
 *
 * Copyright (c) 2009-2014 Fil (fil@rezo.net)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

/*
 * Usage: $("form").autosave({options...});
 * to use with SPIP's action/session.php
 */

(function($){
	$.fn.autosave = function(opt) {
		opt = $.extend({
			url: window.location,
			confirm: false,
			confirmstring: 'Sauvegarder ?'
		},opt);
		var save_changed = function(){
			$('form.autosavechanged')
			.each(function(){
				if (!opt.confirm || confirm(opt.confirmstring)) {
					var contenu = $(this).serialize();
					// ajoutons un timestamp
					var d=new Date();
					contenu = contenu + "&__timestamp=" + d.getTime();
					$.post(opt.url, {
						'action': 'session',
						'var': 'autosave_' + $('input[name=autosave]', this).val(),
						'val': contenu
					});
				}
			}).removeClass('autosavechanged');
		}
		$(window)
		.bind('unload',save_changed);
		return this
		.bind('keyup', function() {
			$(this).addClass('autosavechanged');
		})
		.bind('change', function() {
			$(this).addClass('autosavechanged');
			save_changed();
		})
		.bind('submit',function() {
			save_changed();
			/* trop agressif : exemple du submit previsu forum, ou des submit suivant/precedent d'un cvt multipage
			on sauvegarde toujours, et le serveur videra quand il faudra */
			/*$(this).removeClass('autosavechanged')*/;
		});
	}
})(jQuery);


/*
     FILE ARCHIVED ON 02:58:29 Dec 01, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 08:44:29 Oct 08, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 249.691 (3)
  esindex: 0.007
  captures_list: 264.773
  CDXLines.iter: 10.508 (3)
  PetaboxLoader3.datanode: 264.341 (5)
  exclusion.robots: 0.176
  exclusion.robots.policy: 0.167
  RedisCDXSource: 1.444
  PetaboxLoader3.resolve: 63.953 (2)
  load_resource: 138.279
*/