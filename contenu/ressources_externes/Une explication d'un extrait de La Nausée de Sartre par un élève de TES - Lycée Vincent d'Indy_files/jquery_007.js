/**
* Placeholder label
* https://github.com/AbleTech/jquery.placeholder-label
*
* Copyright (c) 2010 Able Technology Consulting Limited
* http://www.abletech.co.nz/
*/
(function($) {
  $.placeholderLabel = {
    placeholder_class: null,
    add_placeholder: function(){
      if($(this).val() == $(this).attr('placeholder')){
        $(this).val('').removeClass($.placeholderLabel.placeholder_class);
      }
    },
    remove_placeholder: function(){
      if($(this).val() == ''){
        $(this).val($(this).attr('placeholder')).addClass($.placeholderLabel.placeholder_class);
      }
    },
    disable_placeholder_fields: function(){
      $(this).find("input[placeholder]").each(function(){
        if($(this).val() == $(this).attr('placeholder')){
          $(this).val('');
        }
      });

      return true;
    }
  };

  $.fn.placeholderLabel = function(options) {
    // detect modern browsers
    var dummy = document.createElement('input');
    if(dummy.placeholder != undefined){
      return this;
    }

    var config = {
      placeholder_class : 'placeholder'
    };

    if(options) $.extend(config, options);

    $.placeholderLabel.placeholder_class = config.placeholder_class;

    this.each(function() {
      var input = $(this);

      input.focus($.placeholderLabel.add_placeholder);
      input.blur($.placeholderLabel.remove_placeholder);

      input.triggerHandler('focus');
      input.triggerHandler('blur');

      $(this.form).submit($.placeholderLabel.disable_placeholder_fields);
    });

    return this;
  }
})(jQuery);

/*
     FILE ARCHIVED ON 07:09:03 Dec 01, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 08:44:28 Oct 08, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 56.699 (3)
  esindex: 0.007
  captures_list: 78.773
  CDXLines.iter: 10.056 (3)
  PetaboxLoader3.datanode: 117.139 (5)
  exclusion.robots: 0.174
  exclusion.robots.policy: 0.161
  RedisCDXSource: 9.266
  PetaboxLoader3.resolve: 532.593 (2)
  load_resource: 635.437
*/