/*
 * Javascript pour le menu deroulant vertical sur MSIE 6
 *
 */
if (jQuery.browser.msie) {
	function hover(obj) {
		if(document.all) {
			UL = obj.getElementsByTagName('ul');
			if(UL.length > 0) {UL[0].style.display = 'block';}
		}
	}
	
	function hout(obj) {
		if(document.all) {
			UL = obj.getElementsByTagName('ul');
			if(UL.length > 0) {UL[0].style.display = 'none';}
		}
	}
	function setHover(){
		if (document.getElementById('menu_rubriques')) {
			LI = document.getElementById('menu_rubriques').getElementsByTagName('li');
			nLI = LI.length;
			for(i=0; i < nLI; i++){
				LI[i].onmouseover = function(){hover(this);}
				LI[i].onmouseout = function(){hout(this);}
			}
		}
	}

	jQuery(document).ready(function() {
		setHover()
	});
}

/*
     FILE ARCHIVED ON 15:07:39 Dec 01, 2016 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 08:44:27 Oct 08, 2018.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  LoadShardBlock: 108.007 (3)
  esindex: 0.012
  captures_list: 129.993
  CDXLines.iter: 14.901 (3)
  PetaboxLoader3.datanode: 118.865 (5)
  exclusion.robots: 0.272
  exclusion.robots.policy: 0.255
  RedisCDXSource: 2.64
  PetaboxLoader3.resolve: 100.04 (3)
  load_resource: 165.107
*/