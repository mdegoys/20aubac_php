<?php
// On constitue le titre de la page
$titrepage = 'Contribuez au contenu de 20aubac';

// Titre h1
$titre_h1 = 'Contribuez au contenu du site';

// Description de la page
$descpage = 'Apprenez comment contribuer au contenu de 20aubac, en tant qu\'élève ou professeur. 20aubac fonctionne selon deux principes : l\'échange de copies entres élèves et l\'ajout de corrigés par les professeurs qui sont ainsi rémunérés pour chaque accès payant au site.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/apropos-contribuez.html';

ob_start();
?>

<h2>Enrichir le site de nouveaux corrigés</h2>

<p>20aubac a besoin de vous pour enrichir son contenu ! L'objectif est de couvrir un maximum de sujets afin de proposer l'aide la plus complète possible. La seule exigence (qui reste de taille:)) est que les corrigés ou commentaires soient suffisament développés et sérieux dans leur contenu pour réellement aider les visiteurs du site.</p>

<h2>Contribuez en tant qu'élève ou professeur</h2>

<p>Les corrigés du site vous ont aidé ? Vous pouvez soumettre à votre tout vos propres copies d'élève ou commentaires personnels depuis un compte élève et partager ainsi vos travaux avec tous vos camarades actuels et futurs. Chaque copie d'élève est relue et éventuellement corrigée avant d'être publiée.</p>

<p>Si vous êtes professeur, vous pouvez ouvrir un compte en tant que tel et être reconnu ainsi sur vos contributions de corrigés. Vous avez également accès aux sujets les plus demandés - et donc qui vont naturellement bénéficier d'un fort trafic - pour répondre aux plus fortes demandes. Enfin, si vous possédez un blog ou site personnel où vous proposez des corrigés, nous pouvons les référencer pour vous - il suffit de <a href="apropos-mentions.html">nous contacter</a> !</p>

<p>Quoiqu'il arrive, vous restez à tout moment maître de vos contributions et pouvez modifier/supprimer vos contenus si vous le souhaitez.</p>

<p class="textalignright"><a href="compte-creer.html">Créez un compte</a> &bull; <a href="compte-acceder.html">Se connecter</a></p>


<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
