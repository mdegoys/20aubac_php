<?php
// Titre de la page
$titrepage = ucfirst($type).'s';
if ($selection=='annales') { $titrepage.= ' (sujets du bac)'; }
if ($matiere=='philo') { $titrepage .=' de philosophie'; } else { $titrepage .= ' de français'; }

if ($rubrique!='') {
    $titrepage .= ' : '.lcfirst($nomreel);
}

$titrepage .= ' sur 20aubac';

// Titre h1
if ($rubrique!='') {
	 $titre_h1 = $nomreel.' - '; if ($matiere=='francais')  { $titre_h1 .= 'commentaires composés'; } else if ($type=='dissertation') {  $titre_h1 .= ' dissertations corrigées'; } else { $titre_h1 .= ' commentaires corrigés'; }
	 if ($selection=='annales') { $titre_h1 .= ' (sujets du bac)'; }
} else {
	$titre_h1 = 'Liste des'; if ($matiere=='francais') { $titre_h1 .= ' commentaires composés'; } else { $titre_h1 .= ' '.$type.'s'; if ($type=='dissertation') { $titre_h1 .= ' corrigées'; } else { $titre_h1 .= ' corrigés'; }}
}

// Description de la page
if ($matiere=='philo') {
	$descpage = 'Retrouvez sur cette page';
	if ($rubrique=='') {
		$descpage .= 'tous les corrigés de philosophie consultables immédiatement, classés par rubrique (auteur ou notion).';
	}
	else {
		if ($selection=='annales') { $descpage .= ' tous les sujets du bac corrigés'; }
		else if ($selection=='') { $descpage .= ' tous les corrigés de philosophie'; }


		$descpage .= ', consultables immédiatement, concernant '.lcfirst($nomreel).'.';
	}
}
else if ($matiere=='francais') {
	$descpage = 'Retrouvez sur cette page';
	if ($rubrique=='') {
		$descpage .= 'tous les commentaires de français consultables immédiatement, classés par auteur.';
	}
	else {
		if ($selection=='annales') { $descpage .= ' tous les sujets du bac corrigés'; }
		else if ($selection=='') { $descpage .= ' tous les commentaires composés'; }

	    $descpage .= ', consultables immédiatement, concernant '.lcfirst($nomreel).'.';
	}
}

// URL canonique
if ($matiere=='philo') {
    $url_canonique = $url_base.'/philo/corriges-'.$type;
}
else if ($matiere=='francais') {
	$url_canonique = $url_base.'/francais/commentaires-composes';
}
if ($rubrique!='') {
    $url_canonique .= '-'.$rubrique;
}
$url_canonique .= '.html';

// Style associé à la page
$style_page[] = '
.liensressources {
	width: 100%;
	margin: 2px auto;
	display: flex;
	justify-content: space-between;
}

#groupesrubriques {
	vertical-align: top;
	display: flex;
	flex-wrap: wrap;
}

.rubriques {
	vertical-align:top;
	padding:6px;
	width:45%;
}';

ob_start();
 // Si une rubrique a été renseignée : on extrait les ressources correspondantes
if ($rubrique!='') { ?>

	 <br />
	<?php if ($matiere=='francais') { ?>
	Retrouvez ici notre sélection de <a href="commentaires-composes.html">commentaires composés</a>.
	<?php } else { ?>
	Retrouvez ici notre sélection de <a href="corriges-dissertation.html">dissertations</a> et <a href="corriges-commentaire.html">commentaires</a> corrigés.
	<?php } ?>

	<br />
	Si vous ne trouvez pas votre <?php if ($matiere=='philo') { ?>sujet<?php } else { ?>texte<?php } ?>, essayez notre moteur de recherche.

	<?php if ($nb_listesujets_annales!=0 AND $nb_listesujets>0) { ?>
		<p>&nbsp;</p>
		<p>Filtrer les <?php if ($matiere=='francais') { echo 'commentaires'; } else { ?>corrigés<?php } ?> :
		<?php if ($nb_listesujets_annales>0) { ?>
			<a href="<?php if ($matiere=='francais') { ?>commentaires-composes<?php } else { echo 'corriges-'.$type; } ?>-<?php echo $rubrique; ?>.html?s=annales" class="<?php if ($selection=='annales') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Sujets du bac (<?php echo $nb_listesujets_annales; ?>)</a>
		<?php } ?>
		<a href="<?php if ($matiere=='francais') { ?>commentaires-composes<?php } else { echo 'corriges-'.$type; } ?>-<?php echo $rubrique; ?>.html" class="<?php if ($selection=='') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Tous les <?php if ($matiere=='francais') { echo 'commentaires'; } else { ?>corrigés<?php } ?> (<?php echo $nb_listesujets; ?>)</a>
		</p>
	<?php
	} ?>


	<p>&nbsp;</p>

	<?php
	if ($nb_listesujets>0) {
		while ($val = $listesujets->fetch()) {
			$id = $val['id'];
			$type = $val['type'];
			$sujet = $val['sujet'];
			$motscles = $val['motscles'];
			$annale = $val['annale'];
			$ressources = unserialize($val['ressources']);
			$ressources_ext = unserialize($val['ressources_ext']);

			if ($ressources!='') {
				$ressources_tab[0]=explode('_', $ressources[0]);
      } else if ($ressources_ext != '') {
				$ressources_tab[0]=explode('_', $ressources_ext[0]);
      }
			?>

			<div class="liensressources">
				<a href="<?php echo $type; ?>-<?php echo $id; ?>-<?php echo $motscles.'-r'.$ressources_tab[0][1]; ?>.html" class="ressource carre<?php echo $matiere; ?>"> <?php echo $sujet; ?>
				<?php if ($selection=='annales') { ?>
					<br /><em><?php echo $annale; ?></em>
				<?php } ?></a>
			</div>

		<?php
		}
	}  else {
		echo '<p><em>Il n\'y a pas encore de ';	if ($matiere=='francais') { echo 'commentaire'; } else { echo 'corrigé'; }  echo ' pour '.$nomreel.'.</em></p>';
	}

// Si pas de rubrique renseignée : on les liste
} else { ?>

	<?php if ($matiere=='philo') { ?>
		<p>&nbsp;</p>
		<p>
			<a href="corriges-dissertation.html" class="<?php if ($type=='dissertation') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Dissertations</a>
			<a href="corriges-commentaire.html" class="<?php if ($type=='commentaire') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Commentaires</a>
		</p>
	<?php
	} ?>



	<div id="groupesrubriques">
	<?php
	for($nb =0; $nb < count(${'groupe_'.$type.'_'.$matiere}); $nb++)
	{ ?>

		<div class="rubriques">
		<h2><?php echo ${'groupe_'.$type."_".$matiere}[$nb]; ?></h2>
		<?php
		while ($val = $listerubriques[$nb]->fetch())
		{  $nomdecode = $val['nomdecode'];
		   $nomreel = $val['nomreel'];

		if ($matiere=='francais') { ?>
		     <a href="commentaires-composes<?php
	    } else {
			 echo '<a href="corriges-'.$type;
		}
		?>-<?php echo $nomdecode; ?>.html" class="ressource carre"><?php echo $nomreel; ?></a>
		<?php } ?>
		</div>
	<?php } ?>
	</div>
<?php
}
$contenu = ob_get_clean();
require 'gabarit.php';
?>
