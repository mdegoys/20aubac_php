<?php
// On constitue le titre de la page
$titrepage = 'Accueil membres';

// Titre h1
$titre_h1 = 'Bonjour '.$pseudomembre;

ob_start();
?>

<h2>Déposer un corrigé/commentaire</h2>

<p><a href="membres-ajouterressource.html">Déposer un corrigé</a><br />
<a href="membres-ressources.html">Consulter mes corrigés proposés/validés</a></p>

<?php if ($nivmembre==3) { ?>
	<h2>Zone Admin</h2>
	<p><a href="admin-index.html">Accéder à la zone admin</a></p>
<?php
}
$contenu = ob_get_clean();
require 'gabarit.php';
?>
