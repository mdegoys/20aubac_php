<?php 
// Titre de la page
$titrepage = '[Zone admin] Validation ressources';

// Titre h1
$titre_h1 = 'Ressources à modifier/valider';

ob_start();
?>

<h2>Ressources en attente de validation</h2>

<table class="tableau">
	<tr>
		 <th class="tableau textalignleft">id</th>
		 <th class="tableau textalignleft">Titre du sujet</th>
		 <th class="tableau textalignleft">Auteur</th>
		 <th class="tableau textaligncenter">Dernière maj</th>
		 <th class="tableau textaligncenter">Type</th>
		 <th class="tableau textaligncenter">Rubrique</th>
	</tr>
<?
$reponse = $bdd->query("SELECT * FROM `ressources` WHERE `etat`='2' AND `ressource`!='' ORDER BY `id` DESC");
$nb_resultat = $reponse->rowCount();
if ($nb_resultat>0) {
	while ($val = $reponse->fetch()) {
		$id=$val['id'];
		$sujet_id=$val['sujet_id'];
		$auteur=$val['auteur']; 
		$niveau_auteur=$val['niveau_auteur']; 
		$date_maj_ressource=date('d/m/Y',$val['date_maj']); 
	   
	   $reponse2 = $bdd->query("SELECT * FROM `sujets` WHERE id='".$sujet_id."'");
	   while ($val2 = $reponse2->fetch()) {
		$sujet=$val2['sujet'];
		$type=$val2['type'];
		$motscles=$val2['motscles'];
		$matiere_sujet=$val2['matiere']; 
		$rubrique=$val2['rubrique'];	
?>    
	<tr>
		 <td class="tableau textalignleft"><?php echo $id; ?></td>
		 <td class="tableau textalignleft"><a href="<?php echo $url_base; ?>/<?php echo $matiere_sujet; ?>/<?php echo $type; ?>-<?php echo $sujet_id; ?>-<?php echo $motscles; ?>-r<?php echo $id; ?>.html"><?php echo $sujet; ?></a></td>
		 <td class="tableau textalignleft"><?php echo $auteur.' ('.$niveau_auteur.')'; ?></td>
		 <td class="tableau textaligncenter"><?php echo $date_maj_ressource; ?></td>
		 <td class="tableau textaligncenter"><?php echo ucfirst($type); ?></td>
		 <td class="tableau textaligncenter"><?php echo ndc2nr($rubrique,$matiere_sujet); ?></td>
	</tr>
	<?php 
	}
	}
} 
else { ?>
	<tr>
		<td colspan="6" class="tableau textaligncenter"><p><em>Pas de brouillons en attente de validation</em></p></td>
	</tr>
<?php } ?>
</table>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>