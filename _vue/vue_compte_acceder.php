<?php
// Titre de la page
$titrepage = 'Accéder à son compte membre';

// titre h1
$titre_h1 = 'Accéder à son compte';

// Description de la page
$descpage = 'Page d\'accès à son compte membre sur 20aubac. Il vous suffit d\'y indiquer votre nom d\'utilisateur choisi à l\'inscription ainsi que le dernier mot de passe reçu par email.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/compte-acceder.html';

ob_start();
?>

<?php if (isset($message_confirmation)) { ?>
	<p class="resultat"><?php echo $message_confirmation; ?></p>
<?php } else if (isset($message_avertissement)) { ?>
	<p class="avertissement"><?php echo $message_avertissement; ?></p>
<?php } ?>

    <p class="avertissement">L'espace membre est fermée pour cause de réécriture du site.</p>
<!--<p>Veuillez saisir votre nom d'utilisateur ainsi que votre mot de passe (par défaut indiqué dans votre email d'inscription).<br />
  En cas d'oubli de mot de passe <a href="compte-recuperer.html">demandez à le recevoir à nouveau</a>. </p>-->

<form name="identification" action="compte-acceder.html<?php
	//Si on a enregistrée une url demandée pour un visiteur non logué, on la transmet pour redirigé le visiteur une fois celui-ci logué
	if ($url_demandee!='') { ?>?url_demandee=<?php echo $url_demandee; } ?>" method="post" class="form">

<p><label for="username" class="label"><strong>Nom d'utilisateur : </strong></label><input type="text" name="pseudo" id="username" size="25" required <?php echo 'value="'.htmlspecialchars($pseudo).'"'; ?> /></p>

<p><label for="password" class="label"><strong>Mot de passe : </strong></label><input type="password" name="mdp" id="password" size="25" required /></p>

<p class="label"><input type="submit" value="<?php
	//Si on a enregistrée une url demandée pour un visiteur non logué, on la transmet pour redirigé le visiteur une fois celui-ci logué
	if ($url_demandee!='') { ?>Se connecter<?php } else { echo 'Accéder au compte'; } ?>" /></p>
</form>

<p>Si vous n'avez pas de compte, <a href="inscription-eleve.html">créez-en un en deux minutes</a>.</p>
<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
