<?php
// Titre de la page
$titrepage = 'Mise en forme sur 20aubac';

// Titre h1
$titre_h1 = 'La mise en forme sur 20aubac';

// Description de la page
$descpage = 'Comment utiliser la mise en forme sur 20aubac pour vos rédactions.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/aide-bbcode.html';

ob_start();
?>

<p>La mise en forme se fait grâce à des balises (de type [balise d'ouverture] et [/balise de fermeture]) que l'on met dans le texte de départ (lors de l'écriture) et qui sont interprétées par le site (lors de la lecture, les balises sont alors invisibles).</p>

<p>Exemple:</p>

  <table class="tableau">
    <tr>
      <th class="tableau textaligncenter width50per">Le dépositaire du texte saisit:</td>
      <td class="tableau textaligncenter">&quot;[b]connais-toi toi même[/b]&quot;</td>
    </tr>
    <tr>
      <th class="tableau textaligncenter">Le visiteur lit alors:</td>
      <td class="tableau textaligncenter">&quot;<strong>connais-toi toi même</strong>&quot;</td>
    </tr>
</table>

<p>&nbsp;</p>
<p>Il y a ainsi 4 balises de mise en forme disponibles:</p>

 <table class="tableau">
 <tr>
      <td class="tableau textaligncenter width50per">[tp][/tp]</td>
      <td class="tableau textaligncenter"><span class="titre2">les titres des différents paragraphes</span></td>
    </tr>
    <tr>
      <td class="tableau textaligncenter">[b][/b]</td>
      <td class="tableau textaligncenter"><strong>mettre en gras</strong></td>
    </tr>
    <tr>
      <td class="tableau textaligncenter">[i][/i]</td>
      <td class="tableau textaligncenter"><em>mettre en italique</em></td>
    </tr>
    <tr>
      <td class="tableau textaligncenter">[u][/u]</td>
      <td class="tableau textaligncenter"><u>pour souligner</u> </td>
    </tr>
</table>

<p>&nbsp;</p>
<p>Deux remarques:</p>

<ol>
	<li>Il est important de refermer les balises, sinon tout le texte suivant la balise d'ouverture est mis en forme</li>
	<li>Pour respecter la charte du site, veuillez mettre :
        <ul>
		<li>les titres des parties entre [tp] et [/tp] (titre paragraphe)</li>
        <li>les titres des sous-parties entre [b] et [/b] (mis en gras)</li>
        <li>les citations entre [i] et [/i] (italiques)</li>
        <li>et les références d'oeuvres entre [u] et [/u] (souligner)</li>
		</ul>
	</li>
</ol>
<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
