<?php
// Style associé à la page
$style_page[] = '
.infossujet {
	display: flex;
}

.infosujet {
	width: 40%;
}


@media screen and (max-width: 480px) {
	.infossujet {
		flex-wrap: wrap;
	}

	.infosujet {
		width: 100%;
	}
}';
?>

<h2>Informations sur le sujet</h2>

<?php
// On détermine si l'on peut modifier les informations
if (isset($topic_id) AND $topic_id!=0 AND $page!='admin_modifiersujet') {
	$peutmodifier = 0;
} else {
	$peutmodifier = 1;
}
?>

<?php function selection_rubrique($type) {
global $sujet_id,$rubrique,$bdd,$matiere,$peutmodifier,${'groupe_'.$type.'_'.$matiere};	?>

<select name="rubrique<?php if ($sujet_id!=0) { echo '2'; } ?>" id="rubrique" onchange="selectchange()" <?php if ($peutmodifier==0 AND $rubrique!="") { ?> disabled="disabled" <?php } ?> required>
	<option label="disabled" disabled value="" <?php if ($rubrique=='') { echo 'selected'; } ?>>Choisissez...</option>
	<option label="disabled" disabled></option>
		  	 <?php

			 $groupe=${'groupe_'.$type.'_'.$matiere};
		 	 $nb_groupes = count($groupe);
			 for($nb = 0; $nb<$nb_groupes; $nb++)
			 {	?>
		<optgroup label="<?php echo $groupe[$nb]; ?>">
			 <?php
			 $reponse = $bdd->query('SELECT * FROM rubriques WHERE `groupe`=\''.$groupe[$nb].'\' AND `matiere`=\''.$matiere.'\'');
			 while ($val = $reponse->fetch()) {
					$nomdecode = $val['nomdecode'];
					$nomreel = $val['nomreel']; ?>
			<option value="<?php echo $nomdecode; ?>"<?php if ($nomdecode==$rubrique) { echo ' selected'; } ?>><?php echo $nomreel; ?></option>
			<?php } ?>
		</optgroup>
		<?php } if ($type=='commentaire'){ ?>
	<option label="disabled" disabled></option>
	<option value="autre" <?php if  ($rubrique=='autre') { echo 'selected'; } ?>>Autre auteur</option>
		<?php } ?>
</select>
<?php } ?>




 <?php if  ($type=='dissertation') { ?>

    <p><strong><label for="sujet">Le sujet de la dissertation</label></strong><br />
	       <em>(Ex: Etre libre, est-ce philosopher ?)</em><br />
			<input name="sujet<?php if ($sujet_id!=0) { echo '2'; } ?>" id="sujet" type="text" size="50" maxlength="146" value="<?php echo $sujet; ?>" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?> required /></p>

	<p><strong><label for="rubrique">Notion concernée</label></strong> <em>(ou celle s'en rapprochant le plus)</em><br />

		<?php selection_rubrique($type); ?></p>


     <?php } else if ($type=='commentaire') { ?>
    <p><strong><label for="textesource">Texte étudié</label></strong><br />
      Attention, le texte doit être entièrement retranscrit : vous pouvez le recopier ou le retrouver sur des sites tel que <a href="http://fr.wikisource.org" target="_blank">wikisource</a>.<br />
	  <textarea name="textesource<?php if ($sujet_id!=0) { echo '2'; } ?>" id="textesource" cols="75" rows="10" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?> required><?php echo $textesource; ?></textarea></p>

	<p><strong><label for="rubrique">Auteur du texte</label></strong><br />
	  <?php selection_rubrique($type); ?></p>


	<div id="rubrique_autre" <?php if ($sujet_id==0 OR $rubrique!='autre' OR $rubrique2!='autre') { ?>class="displaynone"<?php } ?>>
	<p>Si l'auteur n'est pas présent dans la liste ci-dessus, indiquez son nom ici<br>
	<input name="rubrique_autre<?php if ($sujet_id!=0) { echo '2'; } ?>" type="text" value="<?php echo $rubrique_autre; ?>" size="25" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?>></p>  </div>



	<div class="infossujet">
		<div class="infosujet"><strong><label for="oeuvre">Titre de l'oeuvre</label></strong> -facultatif<br />
		<em>(Ex : Critique de la raison pure)</em><br />
		<input name="oeuvre<?php if ($sujet_id!=0) { echo '2'; } ?>" id="oeuvre" type="text" size="25" value="<?php echo $oeuvre; ?>" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?>>
		</div>

		<div class="infosujet">
		<strong><label for="passage">Indications sur le passage</label></strong> -facultatif<br />
		Tout ce qui permettra de retrouver le passage : partie, chapitre <em>(ex : livre I, chapitre 5)</em><br />
		<input name="passage<?php if ($sujet_id!=0) { echo '2'; } ?>" id="passage" type="text" size="40" value="<?php echo $passage; ?>" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?>>
		</div>
	</div>
	<br />
	<div class="infossujet">
		<div class="infosujet">
		<strong><label for="themes">Thème(s) du texte</label></strong> -facultatif<br />
		Indiquez les thèmes abordés par l'auteur <em><br />
		(Ex :  Individu et communauté)</em><br />
		<input name="themes<?php if ($sujet_id!=0) { echo '2'; } ?>" id="themes" type="text" size="35" value="<?php echo $themes; ?>" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?>>
		</div>

		<div class="infosujet">
		<strong><label for="edition">Indications sur l'édition</label></strong> -facultatif<br />
		Indiquez ici les informations relatives à la page dans une certaine édition, à une traduction particulière<br />
		<em>(ex: pp 23-24 aux éditions Flammarion)</em><br />
		<input name="edition<?php if ($sujet_id!=0) { echo '2'; } ?>" id="edition" type="text" size="40" value="<?php echo $edition; ?>" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?>>
		</div>
	</div>
<?php } ?>

<?php if ($nivmembre==3)  {
    if (isset($ressource_id) AND $ressource_id!=0) {
?>
		<p><strong><label for="motscles">Mots-clés associés au titre</label></strong><br />
		<em>Pour le référencement : doit contenir les mots importants qui caractérisent le <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?>.</em><br />
		<input name="motscles" id="motscles" type="text" value="<?php echo $motscles; ?>" size="45" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?> /></p>
<?php
	}
?>
<p><strong><label for="annale">Annale ?</label></strong><br />
<em>Format type : Annale bac 2010, Séries Technologiques - France métropolitaine</em><br />
<input name="annale" id="annale" type="text" value="<?php echo $annale; ?>" size="45" <?php if ($peutmodifier==0) { ?> disabled="disabled" <?php } ?> /></p>
<?php
}
?>
