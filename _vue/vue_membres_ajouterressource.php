<?php 
// Titre de la page
if ($ressource_id!=0) { 
	$titrepage = 'Modifier un '; if ($matiere=='francais') { $titrepage .= 'commentaire'; } else { $titrepage .= 'corrigé'; } $titrepage .= ' (n° '.$ressource_id.')';
} else {
	$titrepage = 'Proposer un corrigé/commentaire';
}

// Titre h1
if ($ressource_id!=0) { 
    $titre_h1 =  'Modifier un '; if ($matiere=='francais'){ $titre_h1 .= 'commentaire'; } else { $titre_h1 .= 'corrigé'; } $titre_h1 .= ' (n° '.$ressource_id.')'; 
} else {
	$titre_h1 =  'Proposer un corrigé/commentaire';
}

ob_start();
?>

<?php if ($nivmembre>=2) { ?>
	<p>Proposez vos corrigés/commentaires sur 20aubac !</p>

	<p>Remarques :<br /> 
	- Portez attention à la qualité de votre corrigé : les mieux notés sont mis en avant et donc plus consultés.<br />
	- Vous devez obligatoirement être l'auteur du corrigé.
	</p>
<?php } elseif ($nivmembre==1) { ?>
	<p>Proposez vos propres corrigés/commentaires sur 20aubac !</p>

	<p>Remarques :<br />
	- Si votre corrigé est incomplet ou pas assez bien rédigé, il pourra vous être demandé de le revoir. A l'inverse, les meilleurs corrigés seront validés plus rapidement.<br />
	- Vous devez obligatoirement avoir personnellement rédigé le corrigé (ceux notamment récupérés sur d'autres sites seront systématiquement refusés).</p>
<?php } ?>

<?php
// On affiche les erreurs ou le message de confirmation selon le cas
if (!empty($erreurs)) { ?>
    <p class="avertissement">Les données n'ont pas pu être enregistrées, pour <?php if (count($erreurs)==1) { echo 'la raison suivante'; } else { ?>les raisons suivantes<?php } ?> :</p>
	<ul>
	<?php foreach ($erreurs as $valeur) { ?>
	    <li><?php echo $valeur; ?></li>
	<?php } ?>
	</ul>
<?php }
else {
	if (isset($message_confirmation)) { ?>
	    <p class="resultat"><?php echo $message_confirmation; ?></p>
<?php
	}
    if (isset($confirmation_admin)) { ?>
	    <p><?php echo $confirmation_admin; ?></p>
<?php }
} ?>


<?php if ($ressource_id!=0) { ?>
	<p><em>Pour enregistrer une modification, validez par le bouton en bas de page.
	<?php if ($etat!=3) { ?>
		<br />Pour soumettre le <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?>, prévisualisez-le puis cliquez sur &quot;Soumettre à validation&quot;.
	<?php } ?>
	</em>
	<br /><a href="<?php echo $matiere; ?>/<?php echo $type; ?>-<?php echo $sujet_id; ?>-<?php echo $motscles; ?>-r<?php echo $ressource_id; ?>.html">Prévisualiser<?php if (!isset($etat) OR $etat!=3) { ?>/soumettre<?php } if ($matiere=='francais'){ ?> le commentaire<?php } else { ?> le corrigé<?php } ?></a>
	</p>
<?php } else {  ?>
	<p><em>Une fois les champs obligatoires remplis, validez en bas de page. Vous pourrez alors prévisualiser le résultat.</em></p>
<?php }

 
//Si le sujet_id = 0 (n'existe pas), on laisse le choix dans le type de sujet
if ($sujet_id==0) { ?>
	<p>&nbsp;</p>
	<p>
	<strong>Type de corrigé :</strong>
	<a href="membres-ajouterressource.html?type=dissertation" class="<?php if ($type=='dissertation') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Dissertation philo</a> 
	<a href="membres-ajouterressource.html?type=commentaire&amp;matiere=philo" class="<?php if ($type=='commentaire' AND $matiere=='philo') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Commentaire philo</a> 
	<a href="membres-ajouterressource.html?type=commentaire&amp;matiere=francais" class="<?php if ($type=='commentaire' AND $matiere=='francais') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Commentaire français</a>
	</p>

<?php } ?>

<p>&nbsp;</p>
<form action="membres-ajouterressource.html<?php if ($ressource_id!=0) { ?>?ressource_id=<?php echo $ressource_id; }
												else if ($sujet_id!=0) { ?>?sujet_id=<?php echo $sujet_id; } 
												else { ?>?type=<?php echo $type; ?>&amp;matiere=<?php echo $matiere; } ?>" id="ressourceajout" name="ressourceajout" method="post">

<?php include('inc_vue_sujet_informations.php'); ?>

<p>&nbsp;</p>
<h2>Le <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?></h2>

<p><strong><label for="description">Description du <?php if ($matiere=='francais') { ?>commentaire<?php } else { ?>corrigé<?php } ?></label></strong><br />
<?php if ($matiere=='francais') { ?>
	Décrivez en quelques lignes votre commentaire: votre niveau, le plan adopté, s'il est constituée en partie de notes de cours et tout autre élément utile pour le décrire. Ceci permet d'indiquer aux autres membres à quoi ils doivent s'attendre. (<em>ex: Commentaire en deux parties : I. Une scène comique dans la tradition de la comedia del arte, II. Évolution des personnages: une scène de revirement. Un commentaire d'une élève de Terminale L.</em>)
<?php }  elseif ($nivmembre==1) { ?> 
	Présentez en quelques lignes votre corrigé : votre niveau, le plan adopté, s'il comporte des éléments de correction ou des remarques du professeur, la note obtenue et enfin s'il s'agit d'une copie complètement rédigée ou d'un plan très détaillé. Ceci permet d'indiquer aux autres membres à quoi ils doivent s'attendre.<br />
	<em>(ex: Plan en trois parties : I- Une Cité corrompue par le peuple souverain, II- La naissance du populus et du contrat social, III- La démocratie est-elle la forme politique o&ugrave; le populus exerce son pouvoir ?. Une copie entièrement restranscrite d'un élève de Terminale L. Note obtenue : 15/20.)</em>

<?php } elseif ($nivmembre>=2) { ?>        
	Présentez en quelques lignes votre corrigé : le plan adopté et s'il s'agit d'une copie complètement rédigée ou d'un plan très détaillé. Ceci permet d'indiquer aux élèves quoi ils doivent s'attendre.<br />
	<em>(ex: Le plan détaillé en trois parties : I- Une Cité corrompue par le peuple souverain, II- La naissance du populus et du contrat social, III- La démocratie est-elle la forme politique o&ugrave; le populus exerce son pouvoir ?. De nombreuses références pouvant être utilisées sont incluses.)</em>
<?php } ?>        
</p>     
    
<textarea name="description<?php if ($ressource_id!=0) { echo '2'; } ?>" id="description" cols="75" rows="6" required><?php echo $description; ?></textarea>	  
	  
<p><strong><label for="ressource_texte">Le <?php if ($matiere=='francais') { ?>commentaire<?php } else { ?>corrigé<?php } ?></label></strong><br />
Plus votre <?php if ($matiere=='francais') { ?>commentaire<?php } else { ?>corrigé<?php } ?> est complet et soigné, plus il aura de chance d'être accepté rapidement.
</p>


<p><em>Pour mettre en forme le texte, sélectionnez-en une partie, puis cliquez sur le bouton voulu.</em><br />   
<input type="button" value="b" id="boutonb" onclick="storeCaret('b','b')">
<input type="button" value="i" id="boutoni" onclick="storeCaret('i','i')">
<input type="button" value="u" id="boutonu" onclick="storeCaret('u','u')">
<input type="button" value="Titre d'un paragraphe" id="boutontp" onclick="storeCaret('tp','tp')">
(<a href="aide-bbcode.html" target="_blank">Comment ça fonctionne ?</a>)</p>
				
<textarea name="ressource_texte<?php if ($ressource_id!=0) { echo '2'; } ?>" id="ressource_texte" rows="25" cols="75" required><?php echo $ressource_texte; ?></textarea>
	
	
<?php if ($ressource_id!=0 AND $nivmembre==3 AND $etat>=2)  { ?>
	<p>&nbsp;</p>
	<h2>Modération</h2>			  

	<?php //on affiche la possibilité de choisir la note (impossible ensuite de la changer par ce formulaire) et choisir motif refus que dans le cas où le corrigé n'a pas déjà été publié
	if ($etat!=3) { ?>
		<p><strong><label for="note_admin">Note accordée : détermine si la ressource est acceptée ou refusée</label></strong><br />
		<em>Note = 1 ou 2 => Ressource refusée / Note 3 à 5 => Ressource acceptée.</em><br />
		<select name="note_admin" id="note_admin">
			<option value="" <?php if ($note_admin=='') { echo ' selected'; } ?>>Choisissez une note</option>
			<option value="1" <?php if ($note_admin==1) { echo ' selected'; } ?>>1 - Sans grand intérêt</option>
			<option value="2" <?php if ($note_admin==2) { echo ' selected'; } ?>>2 - Faible</option>
			<option value="3" <?php if ($note_admin==3) { echo ' selected'; } ?>>3 - Pas mal</option>
			<option value="4" <?php if ($note_admin==4) { echo ' selected'; } ?>>4 - Bon</option>
			<option value="5" <?php if ($note_admin==5) { echo ' selected'; } ?>>5 - Excellent</option>
		</select></p>

			
		<p><strong>Raison du refus </strong></p>

		<input name="refus" type="radio" value="vide" id="vide" /> <label for="vide">Formulaire quasi vide</label><br />
		<input name="refus" type="radio" value="fautes" id="fautes" /> <label for="fautes">Trop de fautes</label><br />
		<input name="refus" type="radio" value="copie" id="copie" /> <label for="copie">Copié d'un autre site</label><br />
		<input name="refus" type="radio" value="succint" id="succint" /> <label for="succint">Trop succint</label><br />
		<input name="refus" type="radio" value="textemanquant" id="textemanquant" /> <label for="textemanquant">Texte manquant (pour un commentaire)</label><br />
		<input name="refus" type="radio" value="autre" id="autre" checked /> <label for="autre">Autre</label><br />
		<textarea name="refus_autre" id="refus_autre" cols="40" rows="4"><?php if (isset($refus_autre)) { echo $refus_autre; } ?></textarea>
	<?php } else { ?>
		<p><strong>Note accordée : détermine si la ressource est acceptée ou refusée</strong><br />
		Note choisie : <?php echo $note_admin; ?>/5, note des membres : <?php if ($total_votes>3) { echo round(($total_notes-($note_admin*3))/($total_votes-3),2).'/5'; } else { echo 'Pas encore noté'; } ?></p>
	<?php 
	} 
} ?>		
    <p><input type="submit" name="submit" id="button" value="<?php if ($sujet_id==0) { echo 'Sauvegarder et prévisualiser le résultat'; } else { echo 'Valider les modifications';} ?>"></p>
</form>   
<?php 
$contenu = ob_get_clean();
require 'gabarit.php';
?>