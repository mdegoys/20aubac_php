<?php
// Style associé à la page
$style_page[] = '
#cadre_texteetudie {
    width: 80%;
	margin: 28px auto 4px auto;
	border : 1px solid #000;
	box-shadow: -4px 4px 4px #CCC;
	padding: 12px;
}

#texteetudie {
    max-height: calc(1.3em*15);
    overflow-y: auto;
    text-align: justify;
    padding: 2px 8px 2px 2px;
}';


if ($date_maj_ressource!=0) {
  echo 'Dernière mise à jour : '.date('d/m/Y',$date_maj_ressource); }
else if ($date_ajout_ressource!=0) {
  echo 'Date d\'ajout : '.date('d/m/Y',$date_ajout_ressource);
}
// fonction number_format utilisée pour afficher séparateur de milliers
echo ' &bull; '.number_format($nb_consultations,0,',',' ').' vue'; if ($nb_consultations>1) echo 's ';

if ($annale!='') {
  echo ' &bull; '.$annale;
}

// Cadre texte étudié
if ($textesource!='') {    ?>
	<div id="cadre_texteetudie">
		<em>Texte étudié :</em>
		<p id="texteetudie">
		<?php echo $textesource = nl2br($textesource); ?>
		</p>

		<?php
		if ($rubrique=='autre') {
		    echo '<p><u>'.$rubrique_autre;
		}
		else {
		    $nomreel = ndc2nr($rubrique,$matiere);
			echo '<p><u>'.$nomreel;
		}
		if ($oeuvre!='') {
		    echo ', '.$oeuvre.' ';
		}
		if ($passage!='') {
		    echo ' - '.$passage;
		}
		if ($edition!='') {
		    echo ' ('.$edition.')';
		}
		echo '</u></p>'; ?>
	</div>
<?php
 // FIN Cadre texte étudié
} else if ($page=='sujet_consulter') {
    echo '<br />';
}

// Lien modification sujet pour admin
if ($nivmembre==3) { ?>
    <p><a href="<?php echo $url_base; ?>/admin-modifiersujet.html?sujet_id=<?php echo $sujet_id; ?>">Admin : Modifier le sujet</a></p>
<?php
}
?>
