<?php
// Titre de la page
$titrepage = 'Dissertations et commentaires corrigés, aide en philosophie et français';

// Titre h1
$titre_h1 = 'Trouvez le meilleur corrigé en philo ou français :';

// Description de la page
$descpage = 'Dissertations et commentaires corrigés, en philosophie et français, de professeurs et d\'élèves pour préparer efficacement son bac de philo et son oral de français aux épreuves anticipées.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/';

// Style associé à la page
$style_page[] = '
#h1index {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 2rem;
	font-weight: bold;
	text-shadow: -1px 1px #CCC;
	background-image: url("'.$url_base.'/contenu/image/fond_trans.png");
	background-repeat: repeat;
	max-width: 550px;
	padding: 10px;
	margin: 280px 0 0 25%;
}

.h2index {
	font-size: 1.4rem;
	font-weight: bold;
	padding: 0;
	margin: 0;
	font-family: Verdana,Arial,Helvetica,sans-serif;
}

#imageindex {
	background-repeat: no-repeat;
	height: 413px;
	max-width: 960px;
	margin-left: auto;
	margin-right: auto;
	border : 1px solid #CCC;
	background-image: url("'.$url_base.'/contenu/image/index_lyceens.jpg");
}

form.rechercheindex {
	max-width:550px;
	background-color:#FFF;
	border:1px #000 solid;
	padding:2px;
	margin-left:25%;
	margin-top:10px;
	vertical-align:middle;
	text-align:left;
}

input.rechercheindex {
	border: 0px;
	background-color:#FFFFFF;
	font-size: 1.8rem;
	font-family:"verdana",sans-serif;
	color: #000;
	width: calc(100% - 30px);
	vertical-align: middle;
}

input.rechercheindex[type=submit] {
	background-image: url("'.$url_base.'/contenu/image/icone_recherche.png");
	background-color:#FFF;
	background-repeat:no-repeat;
	border: solid 0px #000000;
	width: 22px;
	height: 20px;
	padding:1px;
}

#cadresindex {
    display:flex;
	justify-content: space-between;
}

.cadreindex {
	width: 100%;
	vertical-align: top;
	border-width: 1px;
	border-style: solid;
	padding: 14px;
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	text-align: justify
}

#cadreindex_dossier {
	background-color: #DFDFD0;
	border: 1px solid #DFDFD0;
	padding: 14px;
}

.boutonindex {
	display: inline-block;
	text-align: center;
	padding: 6px;
	max-width: 250px;
	margin: 8px 0 0 auto;
	vertical-align: bottom;
	color: #FFF;
	text-decoration: none;
	opacity: 0.8;
}

.boutonindex:hover {
	color: #FFF;
	opacity: 1;
}

@media screen and (max-width: 480px) {
	#h1index {
		width: auto;
		padding: 10px;
		margin: 140px 0 0 2%;
	}

	#cadresindex {
		flex-wrap: wrap;
	}

	.cadreindex {
		width: 100%;
		margin-bottom: 5px;
	}

	#imageindex {
		max-width: 100%;
		height: 262px;
		background-image: url("'.$url_base.'/contenu/image/index_lyceens_petit.jpg");
	}

	form.rechercheindex {
		width: auto;
		margin-left: 2%;
	}

	input.rechercheindex {
		margin-bottom: 5px;
	}
}
';

ob_start();
?>
<section>
<div id="imageindex">
	<h1 id="h1index"><?php echo $titre_h1; ?></h1>
	<form method="get" name="recherche" action="rechercher.html" target="_top" class="rechercheindex">
		<input name="r" type="search" class="rechercheindex" placeholder="Recherchez parmi <?php echo $nb_ressources; ?> dissertations et commentaires" aria-label="Recherche corrigé" />
		<input type="submit" class="rechercheindex" value="" />
	</form>
</div>
</section>
<br />
<section>
<div id="cadresindex">
	<div class="cadreindex bordureorange">
		<h2 class="h2index orange"># Les derniers corrigés ajoutés ou mis à jour</h2>
        <p>
		<?php

			if($nbResultat_dernieresressources > 0){
			while ($val = $reponse_dernieresressources->fetch())
			{
				$sujet_id = $val['sujet_id'];
				$sujet = $val['sujet'];
				$sujet_motscles = $val['sujet_motscles'];
				$sujet_type = $val['sujet_type'];
				$sujet_matiere = $val['matiere'];
				$sujet = $val['sujet'];
				$ressources = unserialize($val['ressources']);
				$ressource_id = $val['ressource_id'];
				$niveau_auteur = $val['niveau_auteur'];
		?>
		<a href="<?php echo $sujet_matiere; ?>/<?php  echo $sujet_type; ?>-<?php  echo $sujet_id; ?>-<?php  echo $sujet_motscles.'-r'.$ressource_id; ?>.html" class="ressource carre"><?php echo $sujet; ?></a>
		<?php }} ?>
		</p>

		<a href="philo/corriges-dissertation.html" class="boutonindex fondorange">Accédez à tous<br /> les corrigés gratuits</a>
	</div>

</div>
</section>
<br />
<section>
<div id="cadreindex_dossier">
	<h2 class="h2index saumon">Notre objectif : référencer les meilleurs corrigés gratuits pour chaque sujet</h2>

	<p>20aubac vous propose des corrigés gratuits de philosophie (<a href="philo/corriges-dissertation.html">dissertations</a> ou <a href="philo/corriges-commentaire.html">commentaires</a>) et de français (<a href="francais/commentaires-composes.html">commentaires composés</a>), présents soit directement sur le site et qui ont été relus et validés manuellement, soit sur d'autres sites qui font référence : sites de professeurs ou d'éditeurs travaillant avec des professeurs.</p>

	<p>Vous pouvez vous-même <a href="apropos-contribuez.html">contribuer en envoyant vos propres corrigés</a>. (<a href="apropos-presentation.html">En savoir plus sur le site</a>)</p>
</div>
</section>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
