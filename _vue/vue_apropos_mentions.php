<?php
// Titre de la page
$titrepage = 'Mentions légales de 20aubac';

// Titre h1
$titre_h1 = 'A propos';

// Description de la page
$descpage = 'Mentions légales et traitement des données par 20aubac.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/apropos-mentions.html';

ob_start();
?>

<h2>Mentions légales</h2>

<p>20aubac est un site édité à titre personnel, domicilié au :<br />
89 rue Compans<br />
75019 Paris FRANCE<br />
Représentant légal :  Matthieu de Goys<br />
Téléphone : +33.(0) 6 28 33 54 06 <br />
Email : <img src="<?php echo $url_base; ?>/contenu/image/email_20aubac.png" alt="adresse email" height="14" width="129" /></p>

<p>20aubac est hébergé par Gandi SAS, au capital de 300.000&euro; domiciliée au&nbsp;:<br />
63-65 boulevard Massena <br />
75013 Paris FRANCE<br />
Téléphone : +33.(0) 1 70.37.76.61<br />
Email : <a href="mailto:direction@gandi.net">direction@gandi.net</a></p>

<h2>Traitement des données</h2>

<p>Si vous ne souhaitez pas subir les publicités/être tracé et pourtant pouvoir rémunérer les contenus qui vous plaisent ou vous sont utiles, nous vous conseillons l'utilisation du <a href="https://www.brave.com/"  target="_blank">navigateur Brave</a>.</p>

<h2>Infos techniques</h2>

<p>Le site a été entièrement développé avec des outils open source : Notepad++, FileZilla, GIMP. Il est testé en priorité sur Mozilla Firefox.</p>


<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
