<?php
// Titre de la page
if ($recherche!='') {
	if ($matiere=='francais'){ $titrepage = 'Commentaires de français trouvés : '.htmlspecialchars($recherche); }
	else if ($matiere=='philo')  { $titrepage = 'Corrigés de philo trouvés : '.htmlspecialchars($recherche); }
	else { $titrepage = 'Commentaires et corrigés trouvés : '.htmlspecialchars($recherche); }

	if ($page_resultat!='') { $titrepage .= ' (page '.$page_resultat.')'; }
}
else { $titrepage = 'Rechercher un corrigé ou commentaire'; }

// Titre h1
if ($recherche !='') {
	$titre_h1 = 'Résultats de la recherche : <em>'.$recherche.'</em> ('. $nb_resultats .' résultats)';
} else {
	$titre_h1 = 'Recherchez un'; if ($matiere=='francais') { $titre_h1 .= ' commentaire'; } else { $titre_h1 = ' corrigé'; }
}

// Description de la page
if ($recherche!='') {
	$descpage = 'Les ressources trouvées pour la recherche : '.htmlspecialchars($recherche);
	if ($page_resultat!='') { $descpage .= ' (page '.$page_resultat.')'; }
	$descpage .= '. Les corrigés et commentaires proposés sur 20aubac vous permettent de progresser plus rapidement et de préparer efficacement le bac.';
}

else { $descpage = 'Trouver un corrigé de philosophie ou un commentaire composé de français grâce à notre moteur de recherche dédié'; }

// URL canonique
$url_canonique = 'https://www.20aubac.fr/rechercher.html';
if ($matiere!='') {
	$url_canonique .= '?m='.$matiere;
}

// Style associé à la page
$style_page[] = '.page_selectionnee {
	border: 1px solid #000;
	margin: 0 4px;
	min-width: 12px;
	padding: 4px;
	text-align: center;
	font-weight: bold;
}

a.page_nonselectionnee {
	display: inline-block;
	border: 1px solid #333;
	margin: 0 4px;
	min-width:12px;
	padding:4px;
	text-align:center;
}

.lienrecherche {
	margin: 8px 0;
	display: flex;
	justify-content: space-between;
}';

ob_start();
?>

<?php
// Affiche les différentes pages de résultat de la recherche
function afficher_pages ($recherche,$matiere,$nb_resultats,$nb_aff,$page_resultat)  { ?>
    <div class="displayflex"> <?
    // calcul du nombre de pages
    $nbpages = ceil($nb_resultats / $nb_aff); // arrondi a l'entier superieur
    // on affiche les pages
    for($i = 1;$i <= $nbpages;$i ++){
        if ($page_resultat == $i) { ?>
            <div class="page_selectionnee"><?php echo $i; ?></div>
        <?php } else { ?>
            <a href="rechercher.html?r=<?php echo urlencode($recherche); ?>&amp;p=<?php echo $i; ?><?php if ($matiere!='') { ?>&amp;m=<?php echo $matiere; } ?>" class="page_nonselectionnee"><?php echo $i; ?></a>
<?php
            }
    }
?>
    </div>
<?php } ?>

<?php
// Cas a) une recherche a été lancée
if ($recherche !='') {

	// a) i) première possibilité : pas de résultats
	if (($matiere!='' AND ${'nb_resultats_'.$matiere}==0) OR $nb_resultats==0) { ?>

	<p class="avertissement">Aucun résultat
	<?php if ($matiere=='philo' AND $nb_resultats>0) { ?> en philosophie. Souhaitez vous voir les <a href="rechercher.html?r=<?php echo urlencode($recherche); ?>&amp;m=francais" class="avertissement"><?php echo $nb_resultats; ?> résultats en français</a> ?<br /> Sinon essayez d'autres termes et ajoutez votre sujet si besoin est.
	<?php } else if ($matiere=='francais' AND $nb_resultats>0) { ?> en français. Souhaitez vous voir les <a href="rechercher.html?r=<?php echo urlencode($recherche); ?>&amp;m=philo" class="avertissement"><?php echo $nb_resultats; ?> résultats en philosophie</a> ?<br /> Sinon essayez d'autres termes et ajoutez votre sujet si besoin est.
	<?php } else { ?> : essayez d'autres termes et ajoutez votre sujet si besoin est.<?php } ?></p>
	<p>&nbsp;</p>

	<?php
	 // a) ii) Sinon on affiche normalement
	} else {
	?>
		<p>&nbsp;</p>
	<?php
		// Si des résultats différents sont disponibles selon les matières : on affiche les filtres
		if ($nb_resultats_francais>0 AND $nb_resultats_philo>0) { ?>
		<p>Filtrer les résultats :
		<a href="rechercher.html?r=<?php echo urlencode($recherche); ?>&amp;m=philo" class="<?php if ($matiere=='philo') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Philosophie (<?php echo $nb_resultats_philo; ?>)</a>
		<a href="rechercher.html?r=<?php echo urlencode($recherche); ?>&amp;m=francais" class="<?php if ($matiere=='francais') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Français (<?php echo $nb_resultats_francais; ?>)</a>
		<?php if ($nb_resultats_francais>0 AND $nb_resultats_philo>0) { ?>
			<a href="rechercher.html?r=<?php echo urlencode($recherche); ?>" class="<?php if ($matiere=='') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Tous (<?php echo $nb_resultats; ?>)</a>
		<?php } ?>
		</p>
		<?php }

		// Pour l'affiche des pages, si une matière est sélectionné, on prend le nombre de résultats pour cette matière
		if ($matiere!='') { $nb_resultats_aff = ${'nb_resultats_'.$matiere}; } else { $nb_resultats_aff = $nb_resultats; }

		if ($nb_resultats_aff>10) {
			afficher_pages ($recherche,$matiere,$nb_resultats_aff,$nb_aff,$page_resultat);
		} else {
			echo '<p>&nbsp;</p>';
		}

		$resultat_mysql = rechercheSujets($mots,$matiere,$debut,$nb_aff);

		while ($val = $resultat_mysql->fetch()) {
			$sujet = $val['sujet'];
			$id = $val['id'];
			$type = $val['type'];
			$motscles = $val['motscles'];
			$matiere_sujet = $val['site'];
			$ressources = unserialize($val['ressources']);
			$ressources_ext = unserialize($val['ressources_ext']);
			$indice = $val['indice'];

			if ($ressources!='') {
        $ressources_tab[0]=explode('_', $ressources[0]);
      } else if ($ressources_ext != '') {
        $ressources_tab[0]=explode('_', $ressources_ext[0]);
      }
		?>

		<div class="lienrecherche">
			<a href="<?php echo $matiere_sujet; ?>/<?php echo $type; ?>-<?php echo $id; ?>-<?php echo $motscles.'-r'.$ressources_tab[0][1]; ?>.html" class="ressource carre<?php echo $matiere_sujet; ?>"> <?php echo $sujet; ?></a>
		</div>

		<?php
		}
			if ($nb_resultats_aff>10) {
			afficher_pages ($recherche,$matiere,$nb_resultats_aff,$nb_aff,$page_resultat);
		} else {
			echo '<p>&nbsp;</p>';
		}
	}

	?>
	<br />

<?php
// b) Si aucune recherche n'a été effectué, on affiche un message d'avertissement
} else { ?>

	<p class="avertissement">Veuillez saisir au moins un terme dans le champ de recherche ci-dessus.</p>

	<p>Saisissez les deux ou trois mots-clés  qui correspondent à ce que vous recherchez.<br />
	&Ccedil;a peut être aussi bien des notions que des auteurs.</p>

	<?php if ($matiere=='francais'){ ?>
		Exemple pour trouver un commentaire composé sur La comédie Humaine de Balzac :<br />
		<em>comédie humaine balzac</em>
	<?php } else { ?>
		Exemple pour trouver la dissertation :<br />
		<em>Obéir est-ce renoncer à sa liberté ?</em><br /><br />

		Les mots-clés associés  les plus pertinents sont :<br />
		«<em>obéir</em>» et «<em>liberté</em>»
	<?php } ?>

<?php
}
$contenu = ob_get_clean();
require 'gabarit.php';
?>
