<?php 
// Titre de la page
$titrepage = 'Modifier un sujet (n° '.$sujet_id.')';

// Titre h1
$titre_h1 = 'Modifier un sujet (n° '.$sujet_id.')';

ob_start();
?>

<?php
// On affiche les erreurs ou le message de confirmation selon le cas
if (!empty($erreurs)) { ?>
    <p class="avertissement">Les données n'ont pas pu être enregistrées, pour <?php if (count($erreurs)==1) { echo 'la raison suivante'; } else { ?>les raisons suivantes<?php } ?> :</p>
	<ul>
	<?php foreach ($erreurs as $valeur) { ?>
	    <li><?php echo $valeur; ?></li>
	<?php } ?>
	</ul>
<?php }
else {
	if (isset($message_confirmation)) { ?>
	    <p class="resultat"><?php echo $message_confirmation; ?></p>
<?php
	}
    if (isset($confirmation_admin)) { ?>
	    <p><?php echo $confirmation_admin; ?></p>
<?php }
} ?>

<p>
<em>Pour enregistrer une modification, validez par le bouton en bas de page.</em>
<br /><a href="<?php echo $matiere; ?>/<?php echo $type; ?>-<?php echo $sujet_id; ?>-<?php echo $motscles; ?>.html">Prévisualiser le sujet</a>
</p>

<p>&nbsp;</p>
<form action="admin-modifiersujet.html?sujet_id=<?php echo $sujet_id; ?>" id="modifiersujet" name="modifiersujet" method="post">

<?php  include('inc_vue_sujet_informations.php'); ?>
		
    <p><input type="submit" name="submit" id="button" value="Valider les modifications"></p>
</form>   
<?php 
$contenu = ob_get_clean();
require 'gabarit.php';
?>