<?php 
// Titre de la page
$titrepage = '[Zone admin] Accueil';

// Titre h1
$titre_h1 = 'Section Administration';

// Style associé à la page
$style_page[] = '
#categories {
	display: flex;
	flex-wrap: wrap;
}

.categorie {
	width:50%;
	text-align:center;
	margin:5px 0;
}';

ob_start();
?>

<div id="categories">
   <div class="categorie">
	   <img src="<?php echo $url_base; ?>/contenu/image/admin_controle.jpg" alt="admin_controle" width="200" height="160" /><br />
	   <a href="admin-validationressources.html">Validation ressources proposées</a> (<?php echo $row_count5; ?>)<br />
	   <a href="admin-validationurls.html">Validation urls proposées</a> (<?php echo $row_count6; ?>)<br />
	   <a href="admin-validationquestions.html">Validation questions posées</a> (<?php echo $row_count7; ?>)
   </div>
   
   <div class="categorie">
	   <img src="<?php echo $url_base; ?>/contenu/image/admin_statistiques.gif" alt="admin_statistiques" width="200" height="160" /><br />
	   <a href="admin-statistiques.html">Statistiques</a>
   </div>

   <div class="categorie">
	   <img src="<?php echo $url_base; ?>/contenu/image/admin_prison.gif" alt="admin_prison" width="200" height="140" /><br />
	   <a href="admin-validationprofs.html">Validation compte professeur</a> (<?php echo $row_count13; ?>)
   </div>
</div>
<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>