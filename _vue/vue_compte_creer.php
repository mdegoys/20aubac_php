<?php
// Titre de la page
$titrepage = 'Inscription membre sur 20aubac';

// titre h1
$titre_h1 = 'Inscription membre';

// description de la page
$descpage = 'page d\'inscription sur 20aubac.';

$url_canonique = 'https://www.20aubac.fr/compte-creer.html';

ob_start();
?>

<?php /*  if ($submit==true and empty($erreurs)) { ?>

  <p><div class="resultat">votre compte a bien été créé. nous vous en remercions !</div></p>

  <p><strong>un email vous a été envoyé contenant votre mot de passe</strong>. vous en aurez besoin pour pouvoir vous connecter sur le site.</p>

<p>si au bout d'une dizaine de minutes vous n'avez toujours reçu aucun email, vous pouvez demander à <a href="compte-recuperer.html">recevoir un nouveau mot de passe</a>. merci de rajouter l'adresse <em>contact@20aubac.fr</em> dans votre liste de contact pour être sûr de recevoir nos emails.</p>

	<p>une fois votre mot de passe reçu, vous pouvez directement :</p>
	<ul>
	    <li><a href="<?php echo $url_base.'/membres-ajouterressource.html'; ?>">ajouter un corrigé ou un commentaire pour accéder aux ressources du site gratuitement</a></li>
	</ul>

<?php } else { */ ?>
<?php
	if ($submit==true and !empty($erreurs)) { ?>
	    <p class="avertissement">l'inscription n'a pas eu lieu, pour <?php if (count($erreurs)==1) { echo 'la raison suivante'; } else { ?>les raisons suivantes<?php } ?> :</p>
		<ul>
	    <?php foreach ($erreurs as $valeur) { ?>
	        <li><?php echo $valeur; ?></li>
	    <?php } ?>
	    </ul>
<p>&nbsp;</p>
<?php
  }
?>

		<!--<p>vous pourrez, en tant que membre, soumettre vos propres corrigés.</p>-->
    <p class="avertissement">L'inscription est fermée pour cause de réécriture du site.</p>


	<form action="compte-creer.html" method="post" name="inscription" id="inscription" class="form">

	<p><strong><label for="pseudo" class="label">Nom d'utilisateur : </label></strong><input name="pseudo" id="pseudo" type="text" size="35" maxlength="20" <?php echo 'value="'.$pseudo.'"'; ?> required /></p>

	<p><strong><label for="email" class="label">Adresse email : </label></strong><input name="email" id="email" type="email" size="35" <?php echo 'value="'.$email.'"'; ?> required /></p>


	<p><input type="checkbox" name="cgu" id="cgu" value="1" <?php if ($cgu==1) { echo 'checked="checked"'; } ?> required /> <label for="cgu">J'accepte les <a href="aide-cgu.html" target="_blank">Conditions Générales d'Utilisation</a> du site.</label></p>

	<p class="label"><br/>
	<!--<input type="submit" name="submit" value="Valider mon inscription" /></p>-->
	</form>

	<p>Si vous possédez déjà un compte, vous pouvez directement <a href="compte-acceder.html">vous connecter sur cette page</a>.</p>

<?php
/* } */
$contenu = ob_get_clean();
require 'gabarit.php';
?>
