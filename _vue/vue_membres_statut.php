<?php
// Titre de la page
$titrepage = 'Justifier son statut sur 20aubac';

// Titre h1
$titre_h1 = 'Justification de vos diplômes/statut';

ob_start();
?>

<p>Afin de pouvoir accéder à votre espace membre professeur (et publier des corrigés), vous devez pouvoir justifier vos diplômes ou statut d'enseignant. Courriers administratifs à votre nom, diplôme, contrat, fiche de paie (desquelles vous pouvez bien sûr effacer les montants)... tout document officiel qui pourra vous relier à votre poste ou niveau de qualification est accepté.</p>

<p>Vous pouvez nous envoyer ces documents justificatifs par voie numérique à l'aide du formulaire ci-dessous.</p>

<?php
// On affiche les erreurs ou le message de confirmation selon le cas
if (isset($erreurs) AND !empty($erreurs)) { ?>
    <p class="avertissement">Le ou les documents n'ont pas été réceptionnés, pour <?php if (count($erreurs)==1) { echo 'la raison suivante'; } else { ?>les raisons suivantes<?php } ?> :</p>
	<ul>
	<?php foreach ($erreurs as $valeur) { ?>
	    <li><?php echo $valeur; ?></li>
	<?php } ?>
	</ul>
<?php }
else if (isset($message_confirmation)) { ?>
	<p class="resultat"><?php echo $message_confirmation; ?></p>
<?php } ?>

<?
// On regarde si fichiers déjà transmis (si le dossier existe)
$nb_fichier = 0;

if (is_dir($dossier_chemin) AND $dossier2 = opendir($dossier_chemin)) {
	$liste_fichiers ='<ul>';

	while (false!==($fichier = readdir($dossier2))) {
		if($fichier!= '.' && $fichier!='..') {
			$nb_fichier++;
			$liste_fichiers .= '<li>' . $fichier . '</li>';
		}
	} // On termine la boucle

	$liste_fichiers .= '</ul>';

	closedir($dossier2);
}


if ($nb_fichier>0) { ?>
	<p><br />
	<span class="avertissement">Vous nous avez transmis <?php echo $nb_fichier; ?> fichier<?php if ($nb_fichier>1) echo 's'; ?></span>. <?php if ($nb_fichier>1) { echo 'Ceux-ci seront examinés et validés'; } else  { ?>Celui-ci sera examiné et validé<?php } ?> sous 72h. Vous pouvez si nécessaire transmettre d'autres documents.

	 </p>
	<?php echo $liste_fichiers; ?>
<?php } ?>


<form action="membres-statut.html" id="validation" name="validation" method="post" enctype="multipart/form-data">
<h2>Nous envoyer des documents</h2>

<p>Cliquez sur &quot;Parcourir&quot; afin d'aller récupérer des documents sur votre ordinateur.<br />
Tous les types de document (pdf, jpg, word, zip) sont acceptés. Ils ne doivent simplement pas dépasser 5Mo.</p>

<p>
<input type="file" name="fichierA" size="58" value="<?php if(isset($fichierA)) echo $fichierA; ?>" />
</p>

<p>
<input type="file" name="fichierB" size="58" value="<?php if(isset($fichierB)) echo $fichierB; ?>" />
</p>
<br />

<p>
 Pour tout commentaire vis-à-vis de ces documents merci de les indiquer ci-dessous.</p>
<textarea name="commentaire" cols="75" rows="6" id="commentaire"><?php if (isset($erreurs) AND !empty($erreurs)) { echo $commentaire; } ?></textarea>
</p>

<p>
<input type="submit" name="submit" id="button" value="Envoyer">
</p>
</form>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
