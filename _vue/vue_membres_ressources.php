<?php
// Titre de la page
$titrepage = 'Mes corrigés sur 20aubac';

// Titre h1
$titre_h1 = 'Mes corrigés';

ob_start();
?>

<p>Pour modifier ou publier vos corrigés, cliquez sur le titre du sujet.<br />
Pour que votre corrigé soit en ligne, il faut qu'il ait été soumis à publication puis validé.</p>

<h2>Mes corrigés en brouillon/en attente de validation</h2>

<?
$reponse = $bdd->query("
SELECT a.id AS ressource_id, a.etat AS ressource_etat, s.id AS sujet_id, s.sujet AS sujet, s.motscles AS sujet_motscles, s.type AS sujet_type, s.matiere AS sujet_matiere
FROM `sujets` AS s
INNER JOIN `ressources` AS a
ON a.sujet_id = s.id
WHERE a.auteur = '" . $pseudomembre . "' AND etat = 1 OR a.auteur = '" . $pseudomembre . "' AND etat = 2
ORDER BY ressource_etat ASC, ressource_id DESC");

$nbResultat = $reponse->rowCount();
if($nbResultat > 0){  ?>

<table class="tableau">
	<tr>
		<th class="tableau textalignleft"><strong>N°</strong></th>
		<th class="tableau textalignleft"><strong>Titre du sujet</strong></th>
		<th class="tableau textaligncenter"><strong>Type</strong></th>
		<th class="tableau textaligncenter"><strong>Statut</strong></th>
	</tr>
	<?php
	while ($val = $reponse->fetch())
	{ 
		$sujet_id = $val['sujet_id'];
		$sujet = $val['sujet'];
		$sujet_motscles = $val['sujet_motscles'];
		$sujet_type = $val['sujet_type'];
		$sujet_matiere = $val['sujet_matiere'];
		$ressource_id = $val['ressource_id'];
		$ressource_etat = $val['ressource_etat'];

	?>
	<tr>
		<td class="tableau textalignleft"><?php echo $ressource_id; ?></td>
		<td class="tableau textalignleft"><a href="<?php echo $sujet_matiere; ?>/<?php echo $sujet_type; ?>-<?php echo $sujet_id; ?>-<?php echo $sujet_motscles; ?>-r<?php echo $ressource_id; ?>.html"><?php echo htmlspecialchars($sujet); ?></a></td>
		<td class="tableau textaligncenter"><?php echo ucfirst($sujet_type); ?></td>
		<td class="tableau textaligncenter"><?php if($ressource_etat ==1) echo 'Brouillon'; else if($ressource_etat==2) echo 'Validation en cours'; else if($ressource_etat==3) echo 'Validé'; ?></td>
	</tr>
<?php } ?>
 </table>
 
<?php } else { ?>
<p><em>Vous n'avez pas de corrigé en brouillon/en attente de validation. <a href="membres-ajouterressource.html">Soumettez-en un</a> dès à présent.</em></p>
<?php } ?>


<h2>Mes corrigés en ligne</h2>
<?
$reponse = $bdd->query("
SELECT a.id AS ressource_id, a.etat AS ressource_etat, s.id AS sujet_id, s.sujet AS sujet, s.motscles AS sujet_motscles, s.type AS sujet_type, s.matiere AS sujet_matiere
FROM `sujets` AS s
INNER JOIN `ressources` AS a
ON a.sujet_id = s.id
WHERE a.auteur = '" . $pseudomembre . "' AND etat = 3
ORDER BY ressource_etat ASC, ressource_id DESC");

$nbResultat = $reponse->rowCount();
if($nbResultat > 0){  ?>

<table class="tableau">
	<tr>
		<th class="tableau textalignleft"><strong>N°</strong></th>
		<th class="tableau textalignleft"><strong>Titre du sujet</strong></th>
		<th class="tableau textaligncenter"><strong>Type</strong></th>
		<th class="tableau textaligncenter"><strong>Statut</strong></th>
	</tr>
	<?php
	while ($val = $reponse->fetch())
	{ 
		$sujet_id = $val['sujet_id'];
		$sujet = $val['sujet'];
		$sujet_motscles = $val['sujet_motscles'];
		$sujet_type = $val['sujet_type'];
		$sujet_matiere = $val['sujet_matiere'];
		$ressource_id = $val['ressource_id'];
		$ressource_etat = $val['ressource_etat'];

	?>
	<tr>
		<td class="tableau textalignleft"><?php echo $ressource_id; ?></td>
      	<td class="tableau textalignleft"><a href="<?php echo $sujet_matiere; ?>/<?php echo $sujet_type; ?>-<?php echo $sujet_id; ?>-<?php echo $sujet_motscles; ?>-r<?php echo $ressource_id; ?>.html"><?php echo htmlspecialchars($sujet); ?></a></td>
		<td class="tableau textaligncenter"><?php echo ucfirst($sujet_type); ?></td>
		<td class="tableau textaligncenter"><?php if($ressource_etat ==1) echo 'Brouillon'; else if($ressource_etat==2) echo 'Validation en cours'; else if($ressource_etat==3) echo 'Validé'; ?></td>
	</tr>
	<?php } ?>
 </table>
 
<?php } else { ?>
<p><em>Vous n'avez pas encore de corrigé validé. <a href="membres-ajouterressource.html">Soumettez-en un</a> dès à présent.</em></p>

<?php
}
$contenu = ob_get_clean();
require 'gabarit.php';
?>