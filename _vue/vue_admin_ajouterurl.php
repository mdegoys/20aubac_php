<?php 
// On constitue le titre de la page
if ($ressource_id!=0) { 
	$titrepage = 'Modifier les infos d\'un '; if ($matiere=='francais'){ $titrepage .= 'commentaire'; } else { $titrepage .= 'corrigé'; } $titrepage .= ' d\'un autre site (n° '.$ressource_id.')';
} else {
	$titrepage = 'Proposer un corrigé ou commentaire d\'un autre site';
}
ob_start();
?>

<?php
// Titre h1
if ($ressource_id!=0) { 
    $titre_h1 = 'Modifier les infos d\'un '; if ($matiere=='francais'){ echo 'commentaire'; } else { echo 'corrigé'; } echo ' d\'un autre site (n° '.$ressource_id.')'; 
} else {
	$titre_h1 = 'Proposer un corrigé ou commentaire d\'un autre site';
} ?>


<?php
// On affiche les erreurs ou le message de confirmation selon le cas
if (!empty($erreurs)) { ?>
    <p class="avertissement">Les données n'ont pas pu être enregistrées, pour <?php if (count($erreurs)==1) { echo 'la raison suivante'; } else { ?>les raisons suivantes<?php } ?> :</p>
	<ul>
	<?php foreach ($erreurs as $valeur) { ?>
	    <li><?php echo $valeur; ?></li>
	<?php } ?>
	</ul>
<?php }
else {
	if (isset($message_confirmation)) { ?>
	    <p class="resultat"><?php echo $message_confirmation; ?></p>
<?php
	}
    if (isset($confirmation_admin)) { ?>
	    <p><?php echo $confirmation_admin; ?></p>
<?php }
} ?>


<?php if ($ressource_id!=0) { ?>
	<p><em>Pour enregistrer une modification, validez par le bouton en bas de page.</em><br />
	<?php if ((isset($etat2) AND $etat2==3) OR $etat==3) { ?>
	<a href="<?php echo $matiere; ?>/<?php echo $type; ?>-<?php echo $sujet_id; ?>-<?php echo $motscles; ?>.html">Prévisualiser le sujet avec<?php if ($matiere=='francais'){ ?> le commentaire<?php } else { ?> le corrigé<?php } ?> en lien</a>
	<?php } else { ?>
	<a href="<?php echo $url; ?>" target="_blank">Prévisualiser le lien proposé (vous pouvez le modifier si nécessaire)</a>
	<?php } ?>
	</p>
<?php } else {  ?>
	<p><em>Une fois les champs obligatoires remplis, validez en bas de page.</em></p>
<?php }

 
//Si le sujet_id==0 (n'existe pas), on laisse le choix dans le type de sujet
if ($sujet_id==0) { ?>
	<p>&nbsp;</p>
	<p>
	<strong>Type de corrigé :</strong>
	<a href="admin-ajouterurl.html?type=dissertation" class="<?php if ($type=='dissertation') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Dissertation philo</a> 
	<a href="admin-ajouterurl.html?type=commentaire&amp;matiere=philo" class="<?php if ($type=='commentaire' AND $matiere=='philo') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Commentaire philo</a>
	<a href="admin-ajouterurl.html?type=commentaire&amp;matiere=francais" class="<?php if ($type=='commentaire' AND $matiere=='francais') { echo 'bouton_selectionne fondgris'; } else { ?>bouton gris borduregris<?php } ?>">Commentaire francais</a>
	</p>

<?php } if  ($type!='') { ?>

	<p>&nbsp;</p>
	<form action="admin-ajouterurl.html<?php if ($ressource_id!=0) { ?>?ressource_id=<?php echo $ressource_id; }
													else if ($sujet_id!=0) { ?>?sujet_id=<?php echo $sujet_id; } 
													else { ?>?type=<?php echo $type; ?>&matiere=<?php echo $matiere; } ?>" id="ressourceajout" name="ressourceajout" method="post">

<?php } include('inc_vue_sujet_informations.php'); ?>


<p>&nbsp;</p>
<h2>Les infos du <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?></h2>

<p><strong><label for="url">Adresse (url) du <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?></label></strong></p>     
<input type="url" name="url<?php if ($ressource_id!=0) { echo '2'; } ?>" id="url" value="<?php echo $url; ?>" size="70" required>

<p><strong><label for="auteur">Nom de l'auteur</label></strong></p>     
<input type="text" name="auteur<?php if ($ressource_id!=0) { echo '2'; } ?>" value="<?php echo $auteur; ?>" id="auteur">	

<p><strong><label for="niveau_auteur">Niveau de l'auteur</label></strong></p>     
<select name="niveau_auteur<?php if ($ressource_id!=0) { echo '2'; } ?>" id="niveau_auteur">
    <option value="eleve"<?php if (isset($niveau_auteur) AND $niveau_auteur=='eleve') { echo ' selected'; } ?>>Elève</option>
	<option value="professeur"<?php if (isset($niveau_auteur) AND $niveau_auteur=='professeur') { echo ' selected'; } ?>>Professeur</option>
</select>


<?php if ($nivmembre==3 AND $ressource_id!=0 AND $etat>=2)  { ?>
	<p>&nbsp;</p>
	<h2>Modération</h2>
		
	<?php //on affiche la possibilité de choisir la note (impossible ensuite de la changer par ce formulaire) et choisir motif refus que dans le cas où le corrigé n'a pas déjà été publié
	if ($etat!=3) { ?>
		<p><strong><label for="note_admin">Note accordée : détermine si la ressource est acceptée ou refusée</label></strong><br />
		<em>Note = 1 ou 2 => Ressource refusée / Note 3 à 5 => Ressource acceptée.</em><br />
		<select name="note_admin" id="note_admin">
			<option value="" <?php if ($note_admin=='') { echo ' selected'; } ?>>Choisissez une note</option>
			<option value="1" <?php if ($note_admin==1) { echo ' selected'; } ?>>1 - Sans grand intérêt</option>
			<option value="2" <?php if ($note_admin==2) { echo ' selected'; } ?>>2 - Faible</option>
			<option value="3" <?php if ($note_admin==3) { echo ' selected'; } ?>>3 - Pas mal</option>
			<option value="4" <?php if ($note_admin==4) { echo ' selected'; } ?>>4 - Bon</option>
			<option value="5" <?php if ($note_admin==5) { echo ' selected'; } ?>>5 - Excellent</option>
		</select></p>

	<?php } else { ?>
		<p><strong>Note accordée : détermine si la ressource est acceptée ou refusée</strong><br />
		Note choisie : <?php echo $note_admin; ?>/5, note des membres : <?php if ($total_votes>3) { echo round(($total_notes-($note_admin*3))/($total_votes-3),2).'/5'; } else { echo 'Pas encore noté'; } ?></p>
	<?php 
	} 
} ?>

	  

<p><input type="submit" name="submit" id="button" value="<?php if ($sujet_id==0) { echo 'Sauvegarder et prévisualiser le résultat'; } else { echo 'Valider les modifications';} ?>"></p>
</form>   
<?php 
$contenu = ob_get_clean();
require 'gabarit.php';
?>