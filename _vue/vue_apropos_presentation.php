<?php
// Titre de la page
$titrepage = 'A propos de 20aubac';

// Titre h1
$titre_h1 = 'Présentation de 20aubac';

// Description de la page
$descpage = 'Présentation de 20aubac et de ses objectifs.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/apropos-presentation.html';

ob_start();
?>

<h2>Référencer le plus grand nombre de corrigés...</h2>

<p><img src="<?php echo $url_base; ?>/contenu/image/icone_collaboratif.jpg" width="76" height="52" alt="icone_collaboratif" class="floatleft" /></p>
<p class="presentation">20aubac se donne pour but de mettre à disposition le maximum de corrigés gratuits aux élèves pour réussir en philosophie et en français.</p>

<p class="presentation">Le système d'échange de corrigés permet pour cela aux élèves de partager leur propres copies. Les professeurs ont également la possibilité de rédiger et diffuser des corrigés via leur espace membre dédié.</p>

<p class="presentation">20aubac référence également les corrigés disponibles ailleurs sur le web, lorsqu'ils sont de même gratuits et complet. Il est ainsi facile de comparer pour chaque sujet les différents corrigés disponibles qui s'y rapportent. </p>

<h2>... tout en ne retenant que les corrigés de qualité</h2>

<p><img src="<?php echo $url_base; ?>/contenu/image/icone_moderation.jpg" width="76" height="69" alt="icone_moderation" class="floatleft" /></p>
<p class="presentation">Chaque soumission de copie d'élève est modérée pour s'assurer de la qualité du contenu soumis. Cela permet également d'éviter les reprises illicites de contenus publiés sur d'autres sites internet.</p>

<p class="presentation">De même les comptes professeurs sont soumis à validation pour s'assurer de leurs diplômes et qualifications.</p>

<p class="presentation">Enfin, les contenus référencés d'autres sites web ne proviennent que de sources sûres : site de professeurs, d'éditeurs travaillant avec des professeurs ou encore copies d'élèves relues et individuellement validées.</p>

<h2>Eviter la triche, en accompagnant les élèves et en y associant les professeurs</h2>

<p><img src="<?php echo $url_base; ?>/contenu/image/icone_antitriche.jpg" width="76" height="58" alt="icone_antitriche" class="floatleft" /></p>

<p class="presentation">20aubac n'est pas destiné à être un site de triche, permettant aux élèves de ne pas travailler en recopiant des devoirs tout faits. Les élèves chercheront en fait toujours naturellement de l'aide sur internet, mais en leur indiquant uniquement les bons devoirs cela peut au contraire les inspirer à s'intéresser aux sujets traités.</p>

<p class="presentation">20aubac associe aussi les professeurs à son projet, qui ont des comptes dédiés et ont accès aux mêmes corrigés que les élèves.</p>
<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
