<?php
// Titre + titre h1 + description de la page + URL canonique
if (isset($erreur) AND $erreur==404) {
    $titrepage = 'Page inexistante sur 20aubac';
	$descpage = 'Désolé, mais cette page est inexistante sur 20aubac';
	$url_canonique = 'https://www.20aubac.fr/erreur-404.html';
	$titre_h1 = 'Désolé, cette page n\'existe plus ou a été déplacée';
} else {
	$titrepage = 'Plan de 20aubac';
	$descpage = 'Page répertoriant tout le contenu de 20aubac';
	$url_canonique = 'https://www.20aubac.fr/plan-site.html';
	$titre_h1 = 'Plan du site';
}

ob_start();
?>

<?php if (isset($erreur)AND $erreur==404) { ?>

	<p>Cette page <?php if (isset($url_visitee) AND ($url_visitee!=$url_base.'/erreur-404.html')) { echo '('.$url_visitee.')'; } ?> n'est malheureusement pas/plus valide. Nous allons rechercher la cause et essayer de réparer cette erreur.<br />

	En attendant, voici pour retrouver facilement le contenu recherché les différentes rubriques de 20aubac :</p>

<?php } else { ?>

	<p>Retrouvez ici tout le contenu de 20aubac présenté en une seule page  :</p>

<?php } ?>

<div class="displayflex">
    <div class="width50per">
	<h2>Accueil et accès au contenu</h2>
	 <p><a href="<?php echo $url_base; ?>">Page d'accueil</a><br />
	 <a href="<?php echo $url_base; ?>/rechercher.html">Moteur de recherche</a><br />
	 <a href="<?php echo $url_base; ?>/plan-site.html">Plan du site (page actuelle)</a></p>

	<h2>Corrigés et commentaires</h2>
	<p><a href="<?php echo $url_base; ?>/philo/corriges-dissertation.html">Dissertations corrigées</a><br />
    <a href="<?php echo $url_base; ?>/philo/corriges-commentaire.html">Commentaires corrigés</a></p>
	<a href="<?php echo $url_base; ?>/francais/commentaires-composes.html">Commentaires composés</a></p>

    <h2>Compte membres</h2>
    <p><a href="<?php echo $url_base; ?>/compte-creer.html">Créer un compte</a><br />
	<a href="<?php echo $url_base; ?>/compte-acceder.html">Accéder à son compte</a><br />
    <a href="<?php echo $url_base; ?>/compte-recuperer.html">Récupérer son mot de passe</a></p>
	</div>

    <div>
	<h2>A propos de 20aubac</h2>
    <p><a href="<?php echo $url_base; ?>/apropos-presentation.html">Présentation du site</a><br />
    <a href="<?php echo $url_base; ?>/apropos-contribuez.html">Contribuez au contenu</a><br />
	<a href="<?php echo $url_base; ?>/apropos-mentions.html">Mentions légales</a><br />

	<h2>Pages d'aide</h2>
	<p><a href="<?php echo $url_base; ?>/aide-cgu.html">Conditions Générales d'Utilisation</a><br />
	<a href="<?php echo $url_base; ?>/aide-bbcode.html">Explication de la mise en forme/bbcode</a>
	</p>
	</div>
</div>

<p>N'hésitez-pas également à utiliser notre moteur de recherche pour accéder directement aux différents sujets répertoriés.</p>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
