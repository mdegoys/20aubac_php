<?php
// Titre de la page
$titrepage = '[Zone admin] Validation comptes professeurs';

// Titre h1
$titre_h1 = 'Validation comptes professeurs';

ob_start();
?>

<?php
if (isset($confirmation_admin)) { ?>
    <p><?php echo $confirmation_admin; ?></p>
<?php } ?>

<h2>Compte professeurs en attente de validation</h2>

<table class="tableau">
	<tr>
		<th class="tableau textalignleft">id</th>
		<th class="tableau textalignleft">Nom</th>
		<th class="tableau">Dossier</th>
	</tr>
</table>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
