<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
// S'il s'agit du sous-domaine de test on l'indique dans le titre pour bien le rendre visible
if ($domaine=='test') {  $titrepage='*VERSIONTEST* '.$titrepage; }
?>
<title><?php echo $titrepage; ?></title>
<?php if (isset($descpage)) { ?>
    <meta name="description" content="<?php echo $descpage; ?>" />
<?php } ?>
<?php if (isset($url_canonique)) { ?>
    <link rel="canonical" href="<?php echo $url_canonique; ?>" />
<?php } ?>
<link rel="shortcut icon" href="<?php echo $url_base; ?>/contenu/image/favicon.ico"  />
<?php // S'il s'agit du sous-domaine de test on empêche le référencement de la page par les moteurs de recherche pour éviter le duplicate content
if ($domaine=='test' OR ($page=='rechercher' AND $recherche!='')) { ?>
<meta name="robots" content="noindex">
<?php } ?>
<?php if ($page == 'accueil') { ?>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
   	"@type": "WebSite",
	  "url": "https://www.20aubac.fr/",
	  "potentialAction": {
      "@type": "SearchAction",
      "target": "https://www.20aubac.fr/rechercher.html?r={search_term_string}",
      "query-input": "required name=search_term_string"
    }
  }
</script>
<?php } ?>
<?php
$style_page['autre'] = '
#boutonb{width:50px;font-weight:700}#boutoni{width:50px;font-style:italic}#boutonu{width:50px;text-decoration:underline}#boutontp{width:150px}a.ressourcedetail_mini{display:inline-block;text-align:center;text-decoration:none;width:20px;color:#FFF;opacity:.8}a.ressourcedetail_mini:hover{opacity:1}.ratingblock{display:inline-block;vertical-align:-4px}.unit-rating{list-style:none;margin:0;padding:0;height:20px;position:relative;background:url('.$url_base.'/contenu/image/starrating_gris.gif) top left repeat-x}.unit-rating li{text-indent:-90000px;padding:0;margin:0;float:left}.unit-rating li a,.unit-rating li.current-rating{position:absolute;height:20px;display:block;text-indent:-9000px}.unit-rating li a{outline:0;width:20px;text-decoration:none;z-index:20;padding:0}.unit-rating li a:hover{background:url('.$url_base.'/contenu/image/starrating_gris.gif) left center;z-index:2;left:0}.unit-rating a.r1-unit{left:0}.unit-rating a.r1-unit:hover{width:20px}.unit-rating a.r2-unit{left:20px}.unit-rating a.r2-unit:hover{width:40px}.unit-rating a.r3-unit{left:40px}.unit-rating a.r3-unit:hover{width:60px}.unit-rating a.r4-unit{left:60px}.unit-rating a.r4-unit:hover{width:80px}.unit-rating a.r5-unit{left:80px}.unit-rating a.r5-unit:hover{width:100px}.unit-rating li.current-rating{background:url('.$url_base.'/contenu/image/starrating_gris.gif) left bottom;z-index:1}.voted{color:#000}.thanks{color:#36AA3D}.static{color:#5D3126}
';

$couleur = (isset($matiere)) ? $couleurs[$matiere] : $couleurs['standard'];
$style_page[] = ".titre2, h2, h2 a{
  color: {$couleur};
}

a.menuhaut_selectionne, a.menuhaut_selectionne:hover {
  background-color: {$couleur};
  color: #FFF;
}

a.menubas {
  border-bottom-color: {$couleur};
}

.bandeau_haut_wrapper {
  background-color: {$couleur};
}

.bandeau_bas_wrapper {
  background-color: {$couleur};
  box-shadow: 0 50vh 0 50vh {$couleur};
}
";
$style_page['stylesite'] = "@charset \"utf-8\";*{padding:0;margin:0}html{font-size:0.625em;font-size:calc(1em * .625);height:101%}body{font-size:1.3em;background-color:#FFF;font-weight:normal;font-family:Verdana, Arial, Helvetica, sans-serif}h1,h1 a{font-size:24px;font-family:'The Serif Light',Georgia,serif;color:#000;margin:0;line-height:1em;padding:16px 0 5px;text-decoration:none}.titre2{font-size:14px;font-family:Geneva, sans-serif;font-weight:bold;padding-top:18px;margin:0;display:inline-block}#bandeau_login_wrapper{background-color:#e5e5e5}#bandeau_login{display:flex;justify-content:space-between;max-width:960px;margin-right:auto;margin-left:auto;padding:3px;color:#444;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:9px;line-height:normal;text-align:right}#bandeau_haut{display:flex;justify-content:space-between;align-items:center;min-height:25px;padding:10px;max-width:960px;margin-right:auto;margin-left:auto;font-family:Verdana, Helvetica, Arial, sans-serif;font-size:10px;line-height:normal}#bandeau_bas{min-height:25px;padding:10px;vertical-align:middle;max-width:960px;margin-right:auto;margin-left:auto;display:flex}#bandeau_gris_haut{background-color:#e5e5e5;min-height:4px}#bandeau_gris_bas{background-color:#e5e5e5;min-height:4px;margin-top:8px}#menusite{margin:8px 0;display:flex;justify-content:space-between;align-items:center;max-width:960px;margin-right:auto;margin-left:auto}#menugauche{max-width:50%}#menudroite{display:flex;justify-content:space-between;margin-bottom:5px}#logo{flex:1;text-align:left}#logo_img{margin-right:10px;text-align:left;vertical-align:middle;border:0}#icone_membre{margin-left:5px;vertical-align:middle;border:0}#icone_connexion{vertical-align:top}#contenu_principal{max-width:960px;margin-left:auto;margin-right:auto;padding:0 1px}.marginbottom28{margin-bottom:28px}.marginbottom6{margin-bottom:6px}a.menuexpress{color:#444;text-decoration:underline;font-size:11px}a.menuexpress:hover{text-decoration:none}a.menuhaut,a.menuhaut_selectionne{font-family:Verdana, Arial, sans-serif;text-transform:uppercase;text-decoration:none;display:inline-block;text-align:center;width:auto;padding:8px 16px;font-size:16px}a.menuhaut:hover{background-color:#CCC;color:#FFF}a.menubas{font-size:12.5px;color:#FFF;text-decoration:none;border-bottom-width:1px;border-bottom-style:dotted;display:inline}a.menubas:hover{color:#FFF;text-decoration:none;border-bottom:1px dotted #FFF}.menubas{font-size:12.5px;color:#FFF}#fil_ariane{padding:3px 0;max-width:960px;margin-right:auto;margin-left:auto;color:#333;font-size:11px}.nobreak{overflow:hidden;text-overflow:ellipsis;white-space:nowrap}@media screen and (max-width: 480px){.nobreak{white-space:normal}}a.fil_ariane{color:#333;text-decoration:underline;font-size:11px}a.fil_ariane:hover{text-decoration:none}form.recherchehaut{width:340px;background-color:#FFF;border:1px #CCC solid;padding:1px;margin:0;display:inline-block;vertical-align:middle;text-align:left}input.recherchehaut{border-width:0;background-color:#FFF;font-family:Verdana,sans-serif;font-size:13px;color:#000;width:calc(100% - 30px);margin:0;padding:0;cursor:text;vertical-align:middle}input.recherchehaut:hover{border:0}input.recherchehaut[type=submit]{background-image:url('{$url_base}/contenu/image/icone_recherche.png');width:22px;height:20px;padding:1px;cursor:pointer}.colonnebas{margin:20px 20px 50px 0}.width25{width:25%}img.iconebas{margin-right:3px;opacity:0.8;width:28px;height:28px}img.iconebas:hover{opacity:1}@media screen and (max-width: 480px){#bandeau_haut{width:auto;flex-wrap:wrap}.element:nth-child(2){order:3}form.recherchehaut{width:100%;margin-top:5px}#menusite{width:auto}#menugauche{width:0}#menudroite{width:100%}a.menuhaut,a.menuhaut_selectionne{font-size:13px}#contenu_principal{width:auto;margin:0 5px}#bandeau_bas{width:auto}#fil_ariane{width:auto}}p{margin:1em 0}img{border-width:0}table{width:100%;border-spacing:0;border-collapse:collapse;margin-left:auto;margin-right:auto}input,select,textarea{max-width:100%;padding:1px;border:1px solid #666}input:hover,select:hover,textarea:hover{border:1px solid #000}input[type=button],input[type=reset],input[type=submit]{padding:3px;background-color:#FFF;cursor:pointer;color:#333}input[type=button]:hover,input[type=reset]:hover,input[type=submit]:hover{color:#000}h2,h2 a{font-family:Geneva, sans-serif;font-weight:bold;font-size:1.4rem;padding:18px 0 9px;margin:0;text-decoration:none}.resultat{font-family:Tahoma, Geneva, sans-serif;color:#33cc33;font-weight:bold;font-size:1.3rem}.avertissement{font-family:Tahoma, Geneva, sans-serif;color:#ff9933;font-weight:bold;font-size:1.3rem}a{font-size:1.3rem;color:#333;text-decoration:underline;direction:ltr;unicode-bidi:embed}a:hover{color:#333;text-decoration:none}a.ressource{display:block;background-repeat:no-repeat;background-position:0 5px, left;padding:0 0 0 12px;margin:0}a.ressource:hover{text-decoration:none}a.carre{background-image:url('{$url_base}/contenu/image/carre_gris.gif')}a.carrefrancais{background-image:url('{$url_base}/contenu/image/carre_rose.gif')}a.carrephilo{background-image:url('{$url_base}/contenu/image/carre_violet.gif')}a.bouton{padding:8px;font-size:1.3rem;font-weight:bold;text-decoration:none;opacity:0.8;border-width:1px;border-style:solid;display:inline-block}a.bouton:hover{opacity:1}a.bouton_selectionne{padding:8px;font-size:1.3rem;font-weight:bold;text-decoration:none;color:#FFF;display:inline-block}table.tableau{border:1px solid black;padding:5px;margin:10px 0;width:95%;margin-left:auto;margin-right:auto}td.tableau,th.tableau{border:1px solid black;padding:5px;margin:10px 0}th.tableau{background-color:#666;color:#FFF;font-weight:bold}form.form{margin:50px}label.label{display:inline-block;width:25%;vertical-align:top;padding-right:12px}@media screen and (max-width: 480px){label.label{padding-right:0;width:100%}}p.label{margin-left:25%;padding-left:12px}p.presentation{margin-left:100px;text-align:justify}ol{list-style-position:inside}ul{list-style:square;padding-left:8px;margin:8px}li{padding-left:8px}.blanc,a.blanc{color:#fff}.orange,a.orange{color:#ff9933}.bleu,a.bleu{color:#3366ff}.vert,a.vert{color:#33cc33}.gris,a.gris{color:#666}.saumon{color:#F33}.fondblanc{background-color:#fff}.fondorange{background-color:#ff9933}.fondgrisclair{background-color:#eee}.fondgris{background-color:#666}.fondvert{background-color:#33cc33}.fondbleu{background-color:#3366ff}.bordureorange{border-color:#ff9933}.bordurevert{border-color:#33cc33}.borduregrisclair{border-color:#eee}.borduregris{border-color:#666}.bordurebleu{border-color:#3366ff}.textalignleft{text-align:left}.textaligncenter{text-align:center}.textalignright{text-align:right}.textalignjustify{text-align:justify}.displaynone{display:none}.displayflex{display:flex}.displayinline{display:inline}.displayblock{display:block}.floatleft{float:left}.width40per{width:40%}.width50per{width:50%}";
?>

<style>
<?php
$style_page = array_reverse($style_page);

foreach ($style_page as $value){
    echo $value;
}
 ?>
</style>
</head>
<body>
<header>
	<div id="bandeau_login_wrapper">
    <!--
		<div id="bandeau_login">
			<div id="menuexpress">
			<?php if ($nivmembre==3) { ?>
				<a href="<?php echo $url_base; ?>/admin-index.html" class="menuexpress">Admin site</a> -
			<?php } else if  ($nivmembre>0) { ?>
				<a href="<?php echo $url_base; ?>/membres-ajouterressource.html" class="menuexpress">Publier un corrigé</a> -
				<a href="<?php echo $url_base; ?>/membres-accueil.html" class="menuexpress">Espace membre</a>
			<?php } ?>
			</div>
			<div id="login">
			<?php if ($nivmembre==0) { ?>
				<a href="<?php echo $url_base; ?>/compte-creer.html" class="menuexpress">S'enregistrer</a> -
				<a href="<?php echo $url_base; ?>/compte-acceder.html" class="menuexpress">Se connecter</a>
			<?php } else { ?>
				<a href="<?php echo $url_base; ?>/compte-accueil.html" class="menuexpress"><?php echo $pseudomembre; ?> : panneau utilisateur</a> - Se déconnecter
			<?php } ?>
			</div>
    </div>
    -->
	</div>
	<div class="bandeau_haut_wrapper">
		<div id="bandeau_haut">
			<div id="logo">
			<a href="<?php echo $url_base; ?>/"><img src="<?php echo $url_base; ?>/contenu/image/logo_20aubac.png" width="125" height="23" alt="Aide en philo et français" id="logo_img" /></a>
      </div>

			<?php if ($page!='accueil') { ?>
			<form method="get" name="recherche" action="<?php echo $url_base; ?>/rechercher.html" target="_top" class="recherchehaut element">
				<input name="r" type="search" class="recherchehaut" value="<?php if (isset($recherche)) { echo $recherche; } ?>"
				<?php if (isset($matiere) AND $matiere=='philo') { ?>placeholder="Recherchez votre sujet de philo"<?php }
				else if (isset($matiere) AND $matiere=='francais') { ?>placeholder="Recherchez votre sujet de français"<?php }
				else { ?>placeholder="Recherchez votre sujet de philo ou français"<?php } ?>
				aria-label="Recherche corrigé"
				/>
				<?php if (isset($matiere)) { ?><input name="m" type="hidden" value="<?php echo $matiere; ?>"  /><?php } ?>
				<input type="submit" class="recherchehaut" value="" />
			</form>
			<?php } ?>
		</div>
	</div>
	<div id="bandeau_gris_haut"></div>
	<div id="menusite">
		<div id="menugauche"></div>
		<div id="menudroite">
			<a href="<?php echo $url_base; ?>/philo/corriges-dissertation.html" class="menuhaut<?php if (isset($matiere) AND $matiere=='philo') { ?>_selectionne<?php } ?>">Philosophie</a>
			<a href="<?php echo $url_base; ?>/francais/commentaires-composes.html" class="menuhaut<?php if (isset($matiere) AND  $matiere=='francais') { ?>_selectionne<?php } ?>">Français</a>
		</div>
	</div>
</header>
<!--  contenu principal -->
<div id="contenu_principal" class="<?php if ($page=='index') { echo 'marginbottom6'; } else { echo 'marginbottom28'; } ?>">

<?php if ($page!='accueil') { ?>
	<h1><a href="<?php echo $url_canonique; ?>"><?php echo $titre_h1; ?></a></h1>
<?php } ?>
<?= $contenu ?>

</div>
<!-- FIN contenu principal -->
 <footer>
<div id="bandeau_gris_bas">
	<div id="fil_ariane">
		<?php if ($page!='index') { ?>
		    <a href="<?php echo $url_base; ?>/plan-site.html" class="fil_ariane">Plan du site</a> > <a href="<?php echo $url_base; ?>/index.html" class="fil_ariane">Accueil</a>
    <?php } ?>

	 	<?php if ($page=='ressources' OR $page=='ressource_consulter') { ?> >
			<?php if ($matiere=='philo') { ?>
			Corrigés : <a href="corriges-<?php echo $type; ?>.html" class="fil_ariane"><?php echo $type; ?>s</a>
			<?php } else { ?>
      <a href="commentaires-composes.html" class="fil_ariane">Commentaires composés</a>

			<?php } if ($rubrique!='' AND $rubrique!='autre') {
			$nomreel=ndc2nr($rubrique,$matiere);?> >
			<?php if ($type=='dissertation') { echo 'Notion :'; }
      elseif ($type=='commentaire') { echo 'Auteur :'; } ?> <a href="<?php if ($matiere=='philo') { echo 'corriges-'.$type; } else { echo 'commentaires-composes'; } ?>-<?php echo $rubrique ?>.html" class="fil_ariane"><?php echo $nomreel; ?></a> <?php } ?>

      <?php if ($page=='ressource_consulter' and isset($ressource_id) AND $ressource_id!=0) { ?>
          <span class="nobreak">> <a href="<?php echo $type; ?>-<?php echo $sujet_id; ?>-<?php echo $motscles; echo '-r'.$ressource_id; ?>.html" class="fil_ariane">
            <?php echo $sujet; ?> -
        <?php
        if ($type=='commentaire') { echo 'Commentaire'; }
				else if($type=='dissertation')  { echo 'Dissertation'; }
        echo ' de '.$auteur;
        if ($niveau_auteur=='eleve') { echo ' (élève)'; }
        else { echo ' (professeur)'; } ?></a></span>
			<?php } ?>
		<?php } elseif (strpos($page, 'membres_')!== FALSE) {  ?>
        > <a href="membres-accueil.html" class="fil_ariane">Accueil membres</a> (<a href="membres-accueil.html" class="fil_ariane">Se déconnecter</a>)
		<?php } elseif (strpos($page, 'compte_')!== FALSE) {  ?>
		    > <a href="compte-creer.html" class="fil_ariane">Création compte</a> / <a href="compte-acceder.html" class="fil_ariane">Accès</a> / <a href="compte-recuperer.html" class="fil_ariane">Mot de passe</a>
		<?php } elseif (strpos($page, 'apropos_')!== FALSE) { ?>
		    > A propos du site : <a href="apropos-presentation.html" class="fil_ariane">Présentation</a> / <a href="apropos-contribuez.html" class="fil_ariane">Contribuez</a> / <a href="apropos-mentions.html" class="fil_ariane">Mentions légales</a>
    <?php } elseif (strpos($page, 'aide_')!== FALSE) { ?>
		    > Pages d'aide : <a href="aide-cgu.html" class="fil_ariane">CGU</a> / <a href="aide-bbcode.html" class="fil_ariane">BBcode</a>
		<?php } ?>
	</div>
</div>
<div class="bandeau_bas_wrapper">
<div id="bandeau_bas">
		<div class="colonnebas width25">
		<div class="menubas"><strong>A propos</strong></div>
		<br />
			<a href="<?php echo $url_base; ?>/apropos-presentation.html" class="menubas">Présentation du site</a><br />
			<a href="<?php echo $url_base; ?>/apropos-contribuez.html" class="menubas">Contribuez</a><br />
      <a href="<?php echo $url_base; ?>/apropos-mentions.html" class="menubas">Mentions légales</a><br /><br />

      <a href="http://www.xiti.com/xiti.asp?s=605647" title="WebAnalytics" target="_top">
      <script src="<?php echo $url_base; ?>/contenu/js/xiti.js"></script>
      <noscript>
      Mesure d'audience ROI statistique webanalytics par <img width="39" height="25" src="https://logv2.xiti.com/hit.xiti?s=605647&p=<?php echo $page; ?>" alt="WebAnalytics" />
      </noscript></a>


		</div>
		<div class="colonnebas">
		<div class="menubas"><strong><?php if (isset($matiere) AND $matiere=='francais') { echo 'Commentaires'; } else { echo 'Corrigés'; } ?> les plus populaires</strong></div>
		<br />
		<?php
		if (isset($matiere)) { $top5ressources = getTop5Ressources($matiere); }
		else { $top5ressources = getTop5Ressources(); }

		while ($val = $top5ressources->fetch()) {
			$id = $val['id'];
			$sujet = $val['sujet'];
			$type = $val['type'];
			$motscles = $val['motscles'];
			$matiere = $val['site'];
			$ressources = unserialize($val['ressources']);
			$ressources_tab[0]=explode('_', $ressources[0]);

			echo '<a href="'.$url_base.'/'.$matiere.'/'.$type.'-'.$id.'-'.$motscles.'-r'.$ressources_tab[0][1];
			echo '.html" class="menubas">'.$sujet.'</a><br />';
		}
		?>
	</div>
</div>
</div>
</footer>
<?php if ($page=='membres_ajouterressource') { ?>
    <script src="<?php echo $url_base; ?>/contenu/js/redaction.js" async></script>
<?php } ?>
</body>
</html>
