<?php
// Titre de la page
$titrepage = 'Récupérer son compte sur 20aubac';

// titre h1
$titre_h1 = 'Récupérer l\'accès à son compte';

// Description de la page
$descpage = 'Récupérer son nom d\'utilisateur et un nouveau mot de passe en renseignant son adresse email sur 20aubac.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/compte-recuperer.html';

ob_start();

if (isset($erreurs) AND !empty($erreurs)) { ?>
<p class="avertissement">Votre compte n'a pu être retrouvé, pour <?php if (count($erreurs)==1) { echo 'la raison suivante'; } else { ?>les raisons suivantes<?php } ?> :</p>
	<ul>
	<?php foreach ($erreurs as $valeur) { ?>
		<li><?php echo $valeur; ?></li>
	<?php } ?>
	</ul>
<?php
}
?>

<h2>1. J'ai perdu mon nom d'utilisateur/mot de passe</h2>

<p>Indiquez ici l'adresse email que vous avez donné lors de votre inscription. Un email vous sera envoyé indiquant votre nom d'utilisateur et un nouveau mot de passe. Si vous avez des difficultés à recevoir nos emails, vérifiez dans votre dossier de spams ou <a href="apropos-mentions.html">contactez-nous</a>.</p>


 <form name="rappel_email" method="post" action="compte-recuperer.html" class="form">
 <label for="email" class="label"><strong>Votre adresse email : </strong></label> <input name="email" id="email" type="email" size="25" <?php echo 'value="'.$email.'"'; ?> required> <input type="submit" name="submit" value="Retrouver mon accès">
 </form>


<h2>2. J'ai perdu mon mot de passe et je ne reçois pas l'email de rappel/j'ai oublié mon adresse email</h2>

<p>Si vous ne retrouvez pas votre mot de passe et que vous avez également oublié votre adresse email, <a href="apropos-mentions.html">veuillez nous contacter</a> en nous donnant le maximum d'informations sur votre compte : nom d'utilisateur, date d'inscription...</p>
<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
