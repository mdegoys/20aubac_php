<?php
// Titre de la page
$titrepage = 'Conditions générales d\'utilisation';

// Titre h1
$titre_h1 = 'Les Conditions Générales d\'Utilisation';

// Description de la page
$descpage = 'Les conditions générales d\'utilisation du site. Veuillez en prendre connaissance avant de vous inscrire.';

// URL canonique
$url_canonique = 'https://www.20aubac.fr/aide-cgu.html';

ob_start();
?>

<h2>I) Définition et acceptation des Conditions Générales d'Utilisation </h2>

<p>Les Conditions Générales d'Utilisation présentes sur cette page ont pour pour objet de définir les relations entre le service de l'entreprise (en cours de création) 20aubac intitulée &quot;20aubac&quot; et présente à l'adresse Internet www.20aubac.fr et les utilisateurs de ce service.</p>

<p>L'utilisation par les visiteurs de ce service nécessite l'acceptation des Conditions Générales d'Utilisation.</p>

<p>L'entreprise &quot;20aubac&quot; se réserve le droit de modifier à tout moment les présentes Conditions Générales d'Utilisation.</p>


<h2>II) Description du service &quot;20aubac&quot;</h2>

<p>&quot;20aubac&quot; est un service fourni par l'entreprise &quot;20aubac&quot; et qui ont pour objet de fournir à ses utilisateurs des corrigés de philosophie, commnentaires composés de français rédigés par des élèves, des professeurs ou toute autre personne en ayant la compétence. </p>

<p>Ce service n'est disponible à ce jour que sur Internet. Il appartient donc aux utilisateurs de s'équiper du matériel nécessaire pour se connecter à Internet sans que cet équipement ne soit à la charge du service.</p>

<p>Il en est de même pour l'équipement et le paramètrage d'un logiciel de navigation, tel que Mozilla Firefox, capables de d'accéder aux services &quot;20aubac&quot;. Il est de la responsabilité de l'acheteur du document de vérifier que son navigateur internet est à jour et qu'il accepte les cookies.</p>

<h2>III) Engagements des utilisateurs</h2>

<p><strong>a) Engagements liés aux contenus rédigés : corrigés de philosophie et commentaires composés de français</strong></p>

<p>Les utilisateurs (enregistrés en tant qu'&quot;élève&quot; ou &quot;professeur&quot;) qui nous proposent des corrigés ou commentaires composés s'engagent à en être les auteurs. Les services &quot;20aubac&quot; ne pourront en aucun cas faire l'objet de poursuites en cas de litiges relatifs aux droits d'auteur sur un document publié sur les services &quot;20aubac&quot; dans le cas o&ugrave; l'utilisateur n'aurait pas respecté cette règle.</p>

<p>Les utilisateurs s'engagent à ne proposer aucun contenu répréhensible d'une manière générale (raciste, xénophobe, nuisant à l'image d'une personne physique ou morale...).</p>

<p>Les utilisateurs reconnaissent le droit aux services &quot;20aubac&quot; de refuser, de retirer tout corrigé, commentaire composé ou cours à leur seule discrétion et de refuser/supprimer ainsi tout bénéfices que les utilisateurs en retiraient.</p>

<p>Les utilisateurs reconnaissent le droit aux services &quot;20aubac&quot; de refuser leur inscription en tant que &quot;professeur&quot; ou de supprimer leur compte, &quot;élève&quot; ou &quot;professeur&quot; à leur seule discrétion.</p>

<p>Les utilisateurs reconnaissent le droit aux services &quot;20aubac&quot; de suspendre la fourniture du service à leur seule discrétion et sans que cela ne puisse engager leur responsabilité.</p>

<p>Dans le cas d'un cours ou corrigé accepté à la publication après soumission par l'utilisateur, celui-ci reconnait céder de manière exclusive ses droits patrimoniaux à l'entreprise &quot;20aubac&quot; pour la reproduction (à  titre commercial, promotionnel et publicitaire, directement ou indirectement  par tous procédés techniques, sur tous supports écrits, textiles, papier,  plastique, audiovisuel numérique ou multimédia, et sur tout réseau de  télécommunication privatif ou ouvert, national ou international - et notamment  Internet, les Intranets et Extranets) et l'adaptation  (le  droit d'adapter tout ou partie du cours ou corrigé, directement ou indirectement, sous  une forme modifiée, notamment par l'intégration d'éléments nouveaux) de son corrigé ou commentaire composé.</p>

<p>Cette cession est consentie pour le monde entier et pour la durée des droits de propriété intellectuelle.</p>

<p>Les utilisateurs reconnaissent que tous les corrigés ou commentaires composés présents sur les services &quot;20aubac&quot; sont protégés par les droits d'auteurs et que leur accès sur les services &quot;20aubac&quot; ne leur confère aucun droits patrimoniaux ou extra-patrimoniaux sur ces textes.</p>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
