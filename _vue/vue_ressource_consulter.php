<?php
// Titre de la page
$titrepage = $sujet .'-';
if ($type=='commentaire' AND $matiere=='philo') { $titrepage .= ' Commentaire corrigé'; }
else if($type=='commentaire' AND $matiere=='francais')  { $titrepage .= ' Commentaire composé'; }
else if($type=='dissertation')  { $titrepage .= ' Dissertation corrigée'; }
else { $titrepage .= ' 20aubac'; }

// Titre h1
$titre_h1 = $sujet;

// Description de la page
if ($type=='commentaire' AND $matiere=='philo') { $descpage = 'Commentaire corrigé du texte : '.htmlspecialchars($sujet).', proposé par '.$auteur; }
else if($type=='commentaire' AND $matiere=='francais')  { $descpage = ' Commentaire composé sur le texte : '.htmlspecialchars($sujet).', proposé par '.$auteur; }
else if ($type=='dissertation')  { $descpage = 'Dissertation corrigée sur le sujet : '.htmlspecialchars($sujet).', proposée par '.$auteur; }

if ($niveau_auteur=='eleve') { $descpage .= ' (élève).'; } else { $descpage .= ' (professeur).'; } $descpage .= ' Sa description : '.htmlspecialchars($description);

// URL canonique
$url_canonique = 'https://www.20aubac.fr/'.$matiere.'/'.$type.'-'.$sujet_id.'-'.$motscles.'-r'.$ressource_id.'.html';

// Style associé à la page
$style_page[] = '
#infos_ressource {
	display: flex;
	width: 100%;
	background-color: #EEE;
	padding: 6px;
}

a.calltoaction_mini {
  min-width: 160px;
	display: inline-block;
	text-decoration: none;
	padding: 8px;
	border-width: 1px;
  border-style: solid;
	text-align: center;
	margin-left: 5px;
	opacity: 0.8;
}

a.calltoaction_mini:hover {
	opacity: 1;
}

.cadreressource {
  width: 90%;
	border-left-width: 5px;
	border-left-style: solid;
	padding: 6px 8px 2px 12px;
	margin: 0 auto;
	display: flex;
	justify-content: space-between;
	align-items: center;
}

h2.sujet_consulter {
  margin: 12px 0 15px 3%;
}

.sujetssimilaires {
	width: 90%;
	padding: 0 8px;
	margin: 2px auto;
	display: flex;
	justify-content: space-between;
}';

ob_start();

include ('inc_vue_sujet_presentation.php'); ?>

<br /><br />
<?php if ($description != '') { ?>
  <div id="infos_ressource">
    <div class="width50per">
      <?php
      // On affiche la note/propose de noter uniquement s'il s'agit d'une ressource publiée ($etat=3)
      if ($etat==3) {
        if ($peutvoter==1) { ?>
          <strong>Notez ce <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?> : </strong>
        <?php } else { ?>
          Note du <?php if ($matiere=='francais'){ ?>commentaire<?php } else { ?>corrigé<?php } ?> :
        <?php } echo rating_bar($ressource_id,$peutvoter); ?><br />

      <?php } ?>
      Proposé par : <?php echo $auteur;

      if (strpos($niveau_auteur, 'eleve')!== FALSE)
        { echo ' (Elève)'; }

      else if ($niveau_auteur=='professeur')
        { echo ' (Professeur)'; } ?>
    </div>
    <div class="width50per textalignright">
    <?php if ($nivmembre==3 and $etat==1) { ?>Brouillon<br />

    <?php } else if ($auteur==$pseudomembre and $etat==2 OR $nivmembre==3 and $etat==2) { ?>
    <div class="avertissement"><strong><?php if ($matiere=='francais') { ?>Commentaire<?php } else { ?>Corrigé<?php } ?> en attente de validation</strong></div></a>

    <?php } else if ($auteur==$pseudomembre and $etat==3 OR $nivmembre==3 and $etat==3) { ?>
    <div class="resultat"><strong><?php if ($matiere=='francais') { ?>Commentaire<?php } else { ?>Corrigé<?php } ?> en ligne</strong></div></a>

    <?php } if ($auteur==$pseudomembre AND $etat == 1 OR $nivmembre==3 AND $etat == 1) { ?>
    <a href="<?php echo $type; ?>-<?php echo $sujet_id; ?>-<?php echo $motscles; ?>-r<?php echo $ressource_id; ?>.html?soumettre=1"><strong>Soumettre ce <?php if ($matiere=='francais') { ?>commentaire<?php } else { ?>corrigé<?php } ?> à validation</strong></a><br /><?php  } ?>

    <?php  if ($auteur==$pseudomembre AND $etat == 1 OR $auteur==$pseudomembre AND $etat == 2 OR $nivmembre==3) { ?>
    <a href="<?php $url_base; ?>/membres-ajouterressource.html?ressource_id=<?php echo $ressource_id; ?>">Modifier ce <?php if ($matiere=='francais') { ?>commentaire<?php } else { ?>corrigé<?php } ?></a><br />
    <?php } ?>
    </div>
  </div>

  <p>&nbsp;</p>
  <div class="textalignjustify"><em>Description :<br />
  <?php echo bbcode(nl2br($description)); ?></em></div>

  <?php if ($ressource != '') { ?>
  <p>&nbsp;</p>
  <div class="textalignjustify"><?php echo bbcode(nl2br($ressource)); ?></div>
  <?php } ?>

<?php } ?>

<?php if ($url != '') { ?>

<div class="cadreressource fondgrisclair <?php if ($niveau_auteur=='professeur') { echo ' bordureorange'; } else { echo ' bordurevert'; } ?>">
  <div>
    <?php if ($matiere=='philo') { if ($niveau_auteur=='professeur') { ?>Corrigé professeur<?php } else { ?>Copie élève<?php }} else { ?>Commentaire <?php if ($niveau_auteur=='professeur') { echo 'professeur'; } else { echo 'élève'; }}?> (<?php echo $auteur; ?>) du site <?php echo getRootDomain($url); ?>. Note : <?php echo rating_bar($ressource_id,0); ?>.
  </div>

  <a href="<?php echo $url; ?>" class="calltoaction_mini blanc <?php if ($niveau_auteur=='professeur') { echo ' fondorange bordureorange'; } else { echo ' fondvert bordurevert';  } ?>" target="_blank"><strong>Consultez ce corrigé</strong></a>
</div>
<?php } ?>

<?php
if ($ressources != '') { $nb_ressources = count($ressources); } else { $nb_ressources = 0; $ressources = []; }
if ($ressources_ext != '') { $nb_ressources_ext = count($ressources_ext); } else { $nb_ressources_ext = 0;  $ressources_ext = []; }
if ($nb_ressources + $nb_ressources_ext > 1) { ?>
	<h2 class="sujet_consulter">Autres ressources disponibles sur ce sujet</h2>
<?php
	$ressources = array_filter($ressources);
  $ressources_ext = array_filter($ressources_ext);
  $ressources_all = array_values(array_merge($ressources, $ressources_ext));
		foreach ($ressources_all as $cle=>$valeur) {
		$ressources_tab[$cle]=explode("_", $ressources_all[$cle]);
		}

	for($i = 0; $i < $nb_ressources + $nb_ressources_ext; $i++) {

    $reponse = $bdd->query("SELECT * FROM `ressources` WHERE id = '".$ressources_tab[$i][1]."'");
    while ($val = $reponse->fetch()) {
      $ressource_autre_id = $val['id'];
      $ressource_autre_nb_mots = $val['nb_mots'];
      $ressource_autre_auteur = $val['auteur'];
      $ressource_autre_niveau_auteur = $val['niveau_auteur'];
      $ressource_autre_total_notes = $val['total_notes'];
      $ressource_autre_total_votes = $val['total_votes'];
      if ($ressource_autre_id != $ressource_id) {
	?>

      <div class="sujetssimilaires">
        <a href="<?php echo $type.'-'.$sujet_id.'-'.$motscles.'-r'.$ressource_autre_id.'.html'; ?>" class="ressource carre<?php echo $matiere; ?>">
          <?php if ($matiere=='philo') {  if ($ressource_autre_niveau_auteur=='professeur') { ?>Corrigé professeur<?php } else { ?>Copie élève<?php }} else { ?>Commentaire <?php if ($ressource_autre_niveau_auteur=='professeur') { echo 'professeur'; } else { echo 'élève'; }}?> de <?php echo $ressource_autre_auteur; ?>. Note : <?php echo round($ressource_autre_total_notes/$ressource_autre_total_votes, 2).'/5'; ?>.
        </a>
      </div>
	<?php
      }
    }
	}
}	// fin du parcours des autres ressources disponibles pour ce sujet


// On affiche les ressources similaires en fonctions des mots-clés du sujet
$tab_motscles = explode('-', $motscles);

// On supprime les mots-clés doublons (fonction array_unique enlève les doublons en gardant les clefs)
$tab_motscles = array_unique ($tab_motscles);

// On supprime les mots-clés de 1 ou 2 lettres
foreach($tab_motscles as $cle=>$valeur) {
	if (strlen($valeur)<3) {
	unset($tab_motscles[$cle]);  }
}

// On recupère les résultats
$limit = 6;
$resultat_mysql = rechercheSujets ($motscles,$matiere,0,$limit,'restreint');

$i=0;

while ($i<5 AND $val = $resultat_mysql->fetch()) {
$sujet_sim = $val['sujet'];
$sujet_sim_id = $val['id'];
$sujet_sim_type = $val['type'];
$sujet_sim_motscles = $val['motscles'];
$sujet_sim_ressources = unserialize($val['ressources']);
$sujet_sim_ressources_ext = unserialize($val['ressources_ext']);

  if ($sujet_sim_ressources != '') {
		$sujet_sim_ressources_tab[0]=explode('_', $sujet_sim_ressources[0]);
	}
  if ($sujet_sim_ressources_ext != '') {
		$sujet_sim_ressources_tab[0]=explode('_', $sujet_sim_ressources_ext[0]);
	}

$sujet_sim_topic_id = $val['topic_id'];

	if ($sujet_sim_id!=$sujet_id) {
	if ($i==0) { ?>
	<h2 class="sujet_consulter">Sujets similaires disponibles</h2>
	<?php } $i++; ?>

	<div class="sujetssimilaires">
		<a href="<?php echo $sujet_sim_type; ?>-<?php echo $sujet_sim_id; ?>-<?php echo $sujet_sim_motscles.'-r'.$sujet_sim_ressources_tab[0][1]; ?>.html" class="ressource carre<?php echo $matiere; ?>"> <?php echo $sujet_sim; ?></a>

	</div>
<?php
	}
}
?>

<?php
$contenu = ob_get_clean();
require 'gabarit.php';
?>
