<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_membres.php');

$email = trim(strtolower(request_var('email', '', true)));

if ($email!=''){
	$erreurs = array();

	// Vérification validité adresse email
	if (!filter_var($email, FILTER_VALIDATE_EMAIL))	{
		$erreurs['validite_email'] = 'Votre adresse email n\'est pas valide';
	}

	// Vérification présence de caractères interdits pour spams
	$find = array('/bcc\:/i','/Content\-Type\:/i','/cc\:/i','/to\:/i', '/Mime\-Type\:/i');
	$email = preg_replace($find, '*!!*!!*', $email);

	if (strstr($email, '*!!*!!*')) 	{
		$erreurs['caracteres_email'] = 'Votre adresse email renseignée contient des caractères interdits';
	}

	if (empty($erreurs)) {
		$bdd = getBdd($domaine,'site');

		$reponse = $bdd->query("SELECT * FROM `membres` WHERE `email`='".$email."'");
		$row_count = $reponse->rowCount();
		if ($row_count==0) {
			$erreurs['email_introuvable'] = 'Votre adresse email est introuvable. Veuillez la vérifier et si le problème persiste contactez-nous.';
		}
		else {
			// Si on retrouve bien l'email, on 	extrait le nom d'utilisateur et on génère un nouveau mot de passe...
			while ($val = $reponse->fetch()) {
				$pseudo = $val['pseudo'];
			}

			$mdp = genmdp();
			$mdp_hash = phpbb_hash($mdp);

			// ...que l'on enregistre dans la base de donnée
			$bdd->exec("UPDATE `membres` SET `mdp` = '".$mdp_hash."' WHERE `email`='".$email."'");

			$message = 'Bonjour,'."\n";
			$message .= 'Vous avez demandé à recevoir vos informations de connexion sur 20aubac.'."\n\n";

			$message .= 'Votre nom d\'utilisateur: '.$pseudo."\n";
			$message .= 'Votre nouveau mot de passe: '.$mdp."\n\n";

			$message .= 'Si vous rencontrez des difficultés pour vous connecter, merci de nous contacter.'."\n\n";

			$message .= 'A bientôt sur '.$url_base."\n\n";

			envoi_email($email,'Vos informations sur 20aubac',$message);

			// On redirige directement vers la page de connexion avec message confirmant l'envoi d'un email
			header('Location:compte-acceder.html?e=3');	exit;
		}
	}
}

// Affichage
require '_vue/vue_compte_recuperer.php';
