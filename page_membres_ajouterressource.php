<?php
$nivmembrerequis = 1;
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_ressources.php');

$bdd = getBdd($domaine,'site');

$ressource_id = request_var('ressource_id', 0);
$sujet_id = request_var('sujet_id', 0);

// On teste les droits de membre d'accéder à la ressource déjà existante + on récupère les infos non modifiables sur la ressource/le sujet
if ($ressource_id!=0) {
	$reponse = $bdd->query("SELECT * FROM `ressources` WHERE id = '".$ressource_id."'");
	while ($val = $reponse->fetch()) {
	$auteur = $val['auteur'];
	$niveau_auteur = $val['niveau_auteur'];
	$etat = $val['etat'];
	$sujet_id = $val['sujet_id'];
	$total_notes = $val['total_notes'];
	$total_votes = $val['total_votes'];

		$reponse2 = $bdd->query("SELECT * FROM `sujets` WHERE id = '".$sujet_id."'");
		while ($val2 = $reponse2->fetch()) {
			$sujet = $val2['sujet'];
			$ressources = unserialize($val2['ressources']);
			$type = $val2['type'];
			$topic_id = $val2['topic_id'];
			$matiere = $val2['matiere'];
		}
	}
	if ($pseudomembre!=$auteur AND $nivmembre!=3) { header('Location: membres-ajouterressource.html'); }
}
else if ($sujet_id!=0) {
	$ressource_id = 0;
	$etat = 1;

	$reponse = $bdd->query("SELECT * FROM `sujets` WHERE id = '".$sujet_id."'");
	while ($val = $reponse->fetch()) {
		$ressources=unserialize($val['ressources']);
		$type = $val['type'];
		$topic_id = $val['topic_id'];
		$matiere = $val['matiere'];
	}
}
else {
	$type = request_var('type', 'dissertation', true);
	$matiere= request_var('matiere', 'philo', true);
	$etat = 1;
	$topic_id = 0;
}

$submit = (isset($_POST['submit'])) ? true : false;

// On récupère les champs soumis via les formulaires
if ($ressource_id==0) {
	$sujet = request_var('sujet', '', true);
	$textesource = request_var('textesource', '', true);
	$oeuvre = request_var('oeuvre', '', true);
	$passage = request_var('passage', '', true);
	$themes = request_var('themes', '', true);
	$edition = request_var('edition', '', true);
	$rubrique_autre = request_var('rubrique_autre', '', true);
	$rubrique = request_var('rubrique', '', true); if ($rubrique!='autre') { $rubrique_autre =''; }
	$description = request_var('description', '', true);
	$ressource_texte = request_var('ressource_texte', '', true);

	$motscles = '';
	$annale = '';
}

else {
	$sujet2 = request_var('sujet2', '', true);
	$textesource2 = request_var('textesource2', '', true);
	$oeuvre2 = request_var('oeuvre2', '', true);
	$passage2 = request_var('passage2', '', true);
	$themes2 = request_var('themes2', '', true);
	$edition2 = request_var('edition2', '', true);
	$rubrique_autre2 = request_var('rubrique_autre2', '', true);
	$rubrique2 = request_var('rubrique2', '', true); if ($rubrique2!='autre') { $rubrique_autre2 =''; }
	$motscles = request_var('motscles', '', true);
	$annale = request_var('annale', '', true);
	$description2 = request_var('description2', '', true);
	$ressource_texte2 = request_var('ressource_texte2', '', true);

	if ($nivmembre==3 AND  $etat>=2) {
		$note_admin = request_var('note_admin', '', true);
		$refus = request_var('refus', 'autre', true);
		$refus_autre = request_var('refus_autre', '', true);
	}
}

if ($submit==true) {
	// En cas de submit, on teste la validité des champs soumis
	$erreurs = array();

	 // Si les champs sujets sont désactivés (disabled) dans le cas d'un sujet déjà validé, on a pas besoin de les vérifier
	if ($topic_id!=0) {
		if	(
				($ressource_id==0 AND ($description=='' OR $ressource_texte==''))
			OR ($ressource_id!=0 AND ($description2=='' OR $ressource_texte2==''))
			) {
		   $erreurs['champs_vides'] = 'Vous devez remplir tous les champs non déjà pré-remplis';
		}
	}
	elseif ( // Sinon on distingue les 6 cas : type (dissertation/commentaire) sujet existant (oui/non), ressource existante (oui/non)
		($type=='dissertation' AND $sujet_id==0 AND ($sujet=='' OR $description=='' OR $ressource_texte=='' OR !isset($rubrique) OR $rubrique==''))
	OR ($type=='dissertation' AND $sujet_id!=0 AND $ressource_id==0 AND ($sujet2=='' OR $description=='' OR $ressource_texte=='' OR !isset($rubrique2) OR $rubrique2==''))
	OR ($type=='dissertation' AND $sujet_id!=0 AND $ressource_id!=0 AND ($sujet2=='' OR $description2=='' OR $ressource_texte2=='' OR !isset($rubrique2) OR $rubrique2==''))
  	OR ($type=='commentaire' AND $sujet_id==0 AND ($description=='' OR $ressource_texte=='' OR $textesource=='' OR !isset($rubrique) OR $rubrique=='' OR ($rubrique=='autre' AND $rubrique_autre=='')))
	OR ($type=='commentaire' AND $sujet_id!=0 AND $ressource_id==0 AND($description=='' OR $ressource_texte=='' OR $textesource2=='' OR !isset($rubrique2) OR $rubrique2=='' OR ($rubrique2=='autre' AND $rubrique_autre2=='')))
	OR ($type=='commentaire' AND $sujet_id!=0 AND $ressource_id!=0 AND($description2=='' OR $ressource_texte2=='' OR $textesource2=='' OR !isset($rubrique2) OR $rubrique2=='' OR ($rubrique2=='autre' AND $rubrique_autre2=='')))
	) {
	    $erreurs['champs_vides'] = 'Vous devez remplir tous les champs';
	}

  	// On teste la longueur du titre (maximum défini pour phpBB = 146) - si les champs sujets sont désactivés (disabled) dans le cas d'un sujet déjà validé, on a pas besoin de les vérifier
	if ($topic_id==0) {
	    if (
		($type=='dissertation' AND $sujet_id==0 AND strlen($sujet)>146)
		OR ($type=='dissertation' AND $sujet_id!=0 AND strlen($sujet2)>146)
		) {
		    $erreurs['titre_troplong'] = 'Le titre de votre sujet est trop long, veuillez le réduire';
		}
	    elseif (
			($type=='commentaire' AND $sujet_id==0 AND (strlen($rubrique)+strlen($rubrique_autre)+strlen($oeuvre)+strlen($passage)+strlen($themes))>138)
			OR ($type=='commentaire' AND $sujet_id!=0 AND (strlen($rubrique2)+strlen($rubrique_autre2)+strlen($oeuvre2)+strlen($passage2)+strlen($themes2))>138)
			) {
		    $erreurs['titre_troplong'] = 'Le titre de votre sujet est trop long, veuillez réduire un ou plusieurs des éléments suivants : nom de l\'auteur, de l\'oeuvre, passage et thèmes de l\'extrait';
		}
	}

	// Si tout est bon on distingue les 3 cas :	1/ création  de sujet+ressource, 2/ création d'1 ressource, 3/ modification d'1 ressource
	if (empty($erreurs)) {

		//Premier cas : création ressource + sujet
		if ($sujet_id==0) {
			$date_ajout = time();

			//Dans le cas d'un commentaire, on constitue le titre du sujet
			if ($type=='commentaire') {
				// Si thèmes non indiqués, on prend les premiers mots du texte étudié
				if ($themes=='')  { $themes='«'.resume_xmots($textesource,5).'...»'; }
			$sujet = creation_sujet($rubrique,$rubrique_autre,$oeuvre,$passage,$themes,$textesource); }

			// S'il ne s'agit pas d'un commentaire on définit les variables associées comme vide (pour éviter erreurs de variables indéfinies)
			else {  $textesource=''; $oeuvre=''; $passage=''; $themes=''; $edition=''; }

			$motscles = motscles($sujet);
			$nb_mots = str_word_count(minusculesSansAccents($ressource_texte));
			$extrait = suppr_bbcode(mb_substr($ressource_texte,0,1950,'utf-8'));


			// on détermine le niveau de l'auteur (admin = professeur)
			if ($nivmembre==1) { $niveau_auteur = 'eleve'; } else if ($nivmembre>=2) { $niveau_auteur = 'professeur'; }

			// on créé en premier le sujet...
			$req = $bdd->prepare("INSERT INTO sujets (sujet,matiere,motscles,annale,type,date_ajout,textesource,oeuvre,passage,themes,edition,rubrique,rubrique_autre)
			VALUES (:sujet,:matiere,:annale,:motscles,:type,:date_ajout,:textesource,:oeuvre,:passage,:themes,:edition,:rubrique,:rubrique_autre)");
			$req->execute(array(
				'sujet' => $sujet,
				'matiere' => $matiere,
				'motscles' => $motscles,
				'annale' => $annale,
				'type' => $type,
				'date_ajout' => $date_ajout,
				'textesource' => $textesource,
				'oeuvre' => $oeuvre,
				'passage' => $passage,
				'themes' => $themes,
				'edition' => $edition,
				'rubrique' => $rubrique,
				'rubrique_autre' => $rubrique_autre
				));

			// On récupère le sujet_id pour l'indiquer dans la nouvelle entrée ressource ci-dessous...
			$sujet_id = $bdd->lastInsertId();


			// ... puis la ressource ...
			$req = $bdd->prepare("INSERT INTO `ressources` (sujet_id,ressource,extrait,description,nb_mots,date_ajout,auteur,niveau_auteur,etat)
			VALUES (:sujet_id,:ressource,:extrait,:description,:nb_mots,:date_ajout,:auteur,:niveau_auteur,:etat)");
			$req->execute(array(
				'sujet_id' => $sujet_id,
				'ressource' => $ressource_texte,
				'extrait' => $extrait,
				'description' => $description,
				'nb_mots' => $nb_mots,
				'date_ajout' => $date_ajout,
				'auteur' => $pseudomembre,
				'niveau_auteur' => $niveau_auteur,
				'etat' => 1
				));

			// ... puis une fois la ressource créée, on redirige vers celle-ci.
			$ressource_id = $bdd->lastInsertId();

			//echo "<script type=\"text/javascript\">document.location.href=\"consulter-".$ressource_id."-".$motscles.".html\"</script>";
			$message_confirmation = 'Vos données ont bien été enregistrées.';

		// Deuxième cas : création d'1 ressource (mais sujet déjà existant)
		} else if ($sujet_id!=0 AND $ressource_id==0) {

			$date_ajout = time();
			$date_maj = time();

			$nb_mots = str_word_count(minusculesSansAccents($ressource_texte));
			$extrait = suppr_bbcode(mb_substr($ressource_texte,0,1950,'utf-8'));


			// on détermine le niveau de l'auteur (admin = professeur)
			if ($nivmembre==1) { $niveau_auteur = 'eleve'; } else if ($nivmembre>=2) { $niveau_auteur = 'professeur'; }

			// on ne modifie données du sujet que si celui-ci n'a pas déjà été publié ($topic_id==0)
			if ($topic_id==0) {

				//Dans le cas d'un commentaire, on constitue le titre du sujet
				if ($type=='commentaire') {
				$sujet2 = creation_sujet($rubrique2,$rubrique_autre2,$oeuvre2,$passage2,$themes2,$textesource2);}

				if ($motscles=='') { $motscles = motscles($sujet2); }

				if ($type=='dissertation') {
				$req = $bdd->prepare("UPDATE `sujets` SET sujet = :sujet,  rubrique = :rubrique, motscles = :motscles, annale =:annale, date_maj = :date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
				$req->execute(array(
					'sujet' => $sujet2,
					'rubrique' => $rubrique2,
					'motscles' => $motscles,
					'annale' => $annale,
					'date_maj' => $date_maj
					));

				} else if ($type=='commentaire') {
				$req = $bdd->prepare("UPDATE `sujets` SET sujet =:sujet, rubrique =:rubrique, rubrique_autre =:rubrique_autre, textesource =:textesource, oeuvre =:oeuvre, passage =:passage, themes =:themes, edition =:edition, motscles =:motscles, annale =:annale, date_maj =:date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
				$req->execute(array(
					'sujet' => $sujet2,
					'rubrique' => $rubrique2,
					'rubrique_autre' => $rubrique_autre2,
					'textesource' => $textesource2,
					'oeuvre' => $oeuvre2,
					'passage' => $passage2,
					'themes' => $themes2,
					'edition' => $edition2,
					'motscles' => $motscles,
					'annale' => $annale,
					'date_maj' => $date_maj
					));
				}
			}

			// On met à jour également le champ rubrique à la condition expresse que celui-ci n'ait pas déjà été renseigné (on devrait l'avoir récupéré dès lors dans le formulaire)
			if (isset($rubrique2) AND $rubrique2!='') { $bdd->exec("UPDATE `sujets` SET `rubrique` = IF (rubrique = '', '".$rubrique2."', rubrique) WHERE `sujets`.`id` ='".$sujet_id."'"); }

			$req = $bdd->prepare("INSERT INTO `ressources` (sujet_id,ressource,extrait,description,nb_mots,date_ajout,auteur,niveau_auteur,etat)
			VALUES (:sujet_id,:ressource,:extrait,:description,:nb_mots,:date_ajout,:auteur,:niveau_auteur,:etat)");
			$req->execute(array(
				'sujet_id' => $sujet_id,
				'ressource' => $ressource_texte,
				'extrait' => $extrait,
				'description' => $description,
				'nb_mots' => $nb_mots,
				'date_ajout' => $date_ajout,
				'auteur' => $pseudomembre,
				'niveau_auteur' => $niveau_auteur,
				'etat' => 1
				));

			// Une fois la ressource créé, on redirige vers celle-ci.
			$ressource_id = $bdd->lastInsertId();

			//echo "<script type=\"text/javascript\">document.location.href=\"consulter-".$ressource_id."-".$motscles.".html\"</script>";
			$message_confirmation = 'Vos données ont bien été enregistrées.';

		// 3. Troisième cas : modification d'1 sujet/ressource déjà existants (1) et éventuellement acceptation/refus (2) => qui amène à création d'un topic si acceptation (et email à l'auteur quoiqu'il arrive)
		} elseif ($sujet_id!=0 AND $ressource_id!=0) {

			// 3.1 On modifie la base de donnée avec les infos renseignées (quelque soit le cas, même si pas d'acceptation/refus => cas traités en 2)
			$date_maj = time();
			$nb_mots = str_word_count(minusculesSansAccents($ressource_texte2));
			$extrait = suppr_bbcode(mb_substr($ressource_texte2,0,1950,'utf-8'));

			// On met à jour la ressource
			$req = $bdd->prepare("UPDATE `ressources` SET ressource = :ressource, extrait = :extrait,  description = :description, nb_mots =:nb_mots,  date_maj =:date_maj WHERE `ressources`.`id` ='".$ressource_id."'");
			$req->execute(array(
				'ressource' => $ressource_texte2,
				'extrait' => $extrait,
				'description' => $description2,
				'nb_mots' => $nb_mots,
				'date_maj' => $date_maj
			));

			// On ne modifie données du sujet que si celui-ci n'a pas déjà été publié
			if ($topic_id==0) {

				//Dans le cas d'un commentaire, on constitue le titre du sujet
				if ($type=='commentaire') {
				$sujet2 = creation_sujet($rubrique2,$rubrique_autre2,$oeuvre2,$passage2,$themes2,$textesource2);}

				if ($motscles=='') { $motscles = motscles($sujet2); }

				if ($type=='dissertation') {
					$req = $bdd->prepare("UPDATE `sujets` SET sujet = :sujet,  rubrique = :rubrique, motscles = :motscles, annale = :annale, date_maj = :date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
					$req->execute(array(
						'sujet' => $sujet2,
						'rubrique' => $rubrique2,
						'motscles' => $motscles,
						'annale' => $annale,
						'date_maj' => $date_maj
					));

					} else if ($type=='commentaire') {
						$req = $bdd->prepare("UPDATE `sujets`  SET sujet = :sujet,  rubrique = :rubrique, rubrique_autre =:rubrique_autre, textesource =:textesource, oeuvre = :oeuvre, passage = :passage, themes = :themes, edition = :edition, motscles = :motscles, annale = :annale, date_maj = :date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
						$req->execute(array(
							'sujet' => $sujet2,
							'rubrique' => $rubrique2,
							'rubrique_autre' => $rubrique_autre2,
							'textesource' => $textesource2,
							'oeuvre' => $oeuvre2,
							'passage' => $passage2,
							'themes' => $themes2,
							'edition' => $edition2,
							'motscles' => $motscles,
							'annale' => $annale,
							'date_maj' => $date_maj
					));
				}
			}
			$message_confirmation = 'La modification a bien été enregistrée.';

			// On détermine l'état de la ressource, par défaut elle reste la même...
			$etat2 = $etat;

			// ... et 3.2 si note admin => on détermine le nouvel état (etat2) et cas d'acceptation/refus en fonction de la note
			// Si acceptation : on envoie email + créé topic + crédite compte - Si refus : on envoie juste email
			if ($nivmembre==3 AND $note_admin!='') {
				if ($note_admin<=2) {
					$etat2 = 1;
				}
				else if ($note_admin>2) {
					$etat2 = 3;
				}

				$total_notes = $note_admin*3;
				$total_votes = 3;

				// Quelque que soit le cas (acceptation ou refus) on modifie les données de la ressource liés à la modération/notation/etat/gratuité/extrait/date_validation

				// Textes de refus
				if ($refus=='vide') { $refus2 = 'Vous avez soumis un formulaire presque vide.'; }
				else if ($refus=='fautes') { $refus2 = 'Votre corrigé contient trop de fautes ou d\'erreurs; merci de les corriger. Vous pouvez le corriger et le re-soumettre ensuite dans votre espace membre.'; }
				else if ($refus=='copie') { $refus2 = 'Ce corrigé semble avoir été copié d\'un autre site. Vous devez détenir les droits d\'auteur sur le corrigé proposé.'; }
				else if ($refus=='succint') { $refus2 = 'Votre corrigé est trop succint; merci de le compléter. Vous pouvez le modifier et le re-soumettre ensuite dans votre espace membre.'; }
				else if ($refus=='textemanquant') { $refus2 = 'Il manque le texte source, indispensable pour un commentaire. Merci de vite l\'ajouter (soit en le recopiant, soit en le retrouvant sur un site tel que wikisource, http://fr.wikisource.org, par exemple).'; }
				else if ($refus=='autre') { $refus2 = $refus_autre; }

				$req = $bdd->prepare("UPDATE `ressources` SET ressource = :ressource, extrait = :extrait,  description = :description, nb_mots =:nb_mots, note_admin =:note_admin, total_notes = :total_notes, total_votes = :total_votes, refus = :refus, refus_autre =:refus_autre, date_maj =:date_maj, etat =:etat WHERE `ressources`.`id` ='".$ressource_id."'");
				$req->execute(array(
					'ressource' => $ressource_texte2,
					'extrait' => $extrait,
					'description' => $description2,
					'nb_mots' => $nb_mots,
					'note_admin' => $note_admin,
					'total_notes' => $total_notes,
					'total_votes' => $total_votes,
					'refus' => $refus,
					'refus_autre' => $refus2,
					'date_maj' => $date_maj,
					'etat' => $etat2
				));

				// On récupère niveau et adresse email de l'auteur

				$reponse = $bdd->query("SELECT * FROM membres WHERE pseudo='".$auteur."'");
				while ($val = $reponse->fetch()) {
					$email_auteur = $val['email'];
					$rentabiliweb = $val['rentabiliweb'];
					if (strpos($val['niveaumembre'], 'eleve')!==FALSE) { $niveaumembre_auteur = 1; } else { $niveaumembre_auteur = 2; }
				}

				// 3.2 a) On accepte la ressource
				if ($etat2==3 AND $etat==2)  {

				    // On met à jour la table sujets avec la nouvelle ressource ajoutée
					// On rajoute la nouvelle id en début de tableau pour ressource professeur, en fin pour ressource élève
					if ($niveau_auteur=='professeur' AND is_array($ressources)) { array_unshift($ressources,'professeur_'.$ressource_id); }
					else { $ressources[] = $niveau_auteur."_".$ressource_id; }

					/* if ($ressources!='') { */
						$req = $bdd->prepare("UPDATE `sujets` SET ressources = :ressources WHERE `sujets`.`id` ='".$sujet_id."'");
						$req->execute(array("ressources" => serialize($ressources)));
					/* } */

					// On met à jour la table membre avec les crédits additionnels s'il s'agit d'un élève
					if ($niveaumembre_auteur==1) {
					   $bdd->exec("UPDATE `membres` SET `credits` =  credits+5 WHERE `membres`.`pseudo`='".$auteur."'");
					}

					// On crée le topic correspondant et on met à jour la table sujets
					if ($topic_id==0) {

						$date_valid = $date_maj;

						$bdd->exec("UPDATE `sujets` SET `date_valid`='".$date_valid."' WHERE `id`='".$sujet_id."'");
					}

					// Et enfin, on envoie un email de confirmation à l'auteur
					if ($matiere=='francais') {
						$sujet_email = 'Commentaire accepté sur 20aubac'; }
					else {
						$sujet_email = 'Corrigé accepté sur 20aubac';
					}

					$message = 'Bonjour '.$auteur.','."\n\n";

					$message .= 'Vous avez proposé un';
					if ($matiere=='francais') { $message .= ' commentaire'; } else { $message .= ' corrigé'; }
					$message .= ' sur le sujet suivant:'."\n";
					if ($sujet2!='') {
						$message .= '"'.stripslashes($sujet2).'"'."\n\n";
					}
					else {
						$message .= '"'.stripslashes($sujet).'"'."\n\n";
					}

					$message .= 'Celui-ci vient d\'être validé !'."\n\n";

					$message .= 'Merci à vous et à bientôt sur 20aubac.'."\n";
					$message .= $url_base;

					envoi_email($email_auteur,$sujet_email,$message);
				}

				// 3.2 b) Cas du refus de la ressource : on envoie juste un email
				else if ($etat2==1 AND $etat==2) {

					if ($matiere=='francais') {
						$sujet_email  = 'Commentaire refusé sur 20aubac';
					}
					else {
						$sujet_email  = 'Corrigé refusé sur 20aubac';
					}

					$message = 'Bonjour '.$auteur.','."\n\n";

					$message .= 'Vous avez proposé un';
					if ($matiere=='francais') { $message .= ' commentaire'; } else { $message .= ' corrigé'; }
					$message .= ' sur le sujet suivant'.":\n";
					if ($sujet2!='') {
					    $message .= '"'.stripslashes($sujet2).'"'."\n\n";
					}
					else {
					    $message .= '"'.stripslashes($sujet).'"'."\n\n";
					}

					$message .= 'Celui-ci vient d\'être malheureusement refusé.'."\n\n";

					$message .= 'La raison (principale) donnée est la suivante:'."\n";
					$message .= stripslashes($refus2)."\n\n";

					if ($matiere=='francais') { $message .= 'Votre commentaire n\'a cependant pas disparu: il est classé dans vos brouillons.'."\n"; }
					else { $message  .= 'Votre corrigé n\'a cependant pas disparu: il est classé dans vos brouillons.'."\n"; }

					$message .= 'Vous pourrez le resoumettre après l\'avoir modifié (il sera supprimé au bout de 30 jours sans modification autrement).'."\n\n";

					$message .= 'Merci de votre compréhension.'."\n";
					$message .= $url_base;

					envoi_email($email_auteur,$sujet_email,$message);
				}


				// On confirme l'envoi d'un email
				if (isset($confirmation_admin)) {
					$confirmation_admin .= 'Etats (etat1 => etat2) :'.$etat.' => '.$etat2.' - ';
				} else {
					$confirmation_admin = 'Etats (etat1 => etat2) :'.$etat.' => '.$etat2.' - ';
				}
				$confirmation_admin .= 'Email envoyé à '.$email_auteur;

			} // Fin cas acceptation ou refus

		} // Fin vérification des 3 cas
	} // Fin cas absence d'erreurs
} // Fin submit

if ($sujet_id!=0) {
	$reponse = $bdd->query("SELECT * FROM sujets WHERE id = '".$sujet_id."'");
	while ($val = $reponse->fetch()) {
		$sujet=$val['sujet'];
		$motscles=$val['motscles'];
		$rubrique=$val['rubrique'];
		$rubrique_autre=$val['rubrique_autre'];
		$annale=$val['annale'];
		$textesource=$val['textesource'];
		$oeuvre=$val['oeuvre'];
		$passage=$val['passage'];
		$themes=$val['themes'];
		$edition=$val['edition'];
	}
}

if ($ressource_id!=0) {
	$reponse = $bdd->query("SELECT * FROM `ressources` WHERE id = '".$ressource_id."'");
	while ($val = $reponse->fetch()) {
		$ressource_texte=$val['ressource'];
		$description=$val['description'];
		$refus=$val['refus'];
		$note_admin=$val['note_admin'];
	}
}

// Affichage
require '_vue/vue_membres_ajouterressource.php';
