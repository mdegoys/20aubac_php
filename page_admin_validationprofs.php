<?php
$nivmembrerequis = 3;
require '_controleur/controleur_session.php';
require '_modele/modele.php';

$bdd = getBdd($domaine,'site');

$user_id = request_var('user_id', 0);
$action = request_var('action', '');

// Quelque soit le cas (refus ou validation) on supprime le dossier associé et on récupère son dossier + email
if ($user_id!=0) {

	$chemin = CHEMIN_SCRIPT.'/contenu/doc_membre/'.$dossier;
	$dir = opendir($chemin);
	while($file = readdir($dir)) {
	    @unlink($chemin . '/' . $file);
	}
	closedir($dir);
	rmdir($chemin);

	// Si valid : on change groupe utilisateur sur phpbb et on envoie un email au membre
	if ($action=='valider') {

		$message = 'Bonjour,'."\n\n";

		$message .= 'Votre compte professeur sur 20aubac a été validé !'."\n\n";

		$message .= 'Vous pouvez dès à présent :'."\n";
		$message .= '- Proposer des corrigés'."\n";
		$message .= '- Accéder à la liste des corrigés les plus demandés'."\n";

		$message .= 'Nous restons bien-sûr à votre disposition pour toute question.'."\n\n";

		$message .= $url_base."\n\n";

		envoi_email($user_email,'Compte professeur validé sur 20aubac',$message);

		$confirmation_admin = 'Compte accepté - email envoyé à '.$user_email.' + dossier supprimé - groupe phpBB mis à jour';
	}

	// Si refus : om envoie simplement un email pour prévenir le membre
	else if ($action=='refuser') {

		$message = 'Bonjour,'."\n\n";

		$message .= 'Votre compte professeur sur 20aubac n\'a pas pu être validé en l\'état.'."\n\n";

		$message .= 'Vous devez nous envoyer un ou d\'autres documents jutificatif (courrier administratif, diplôme, contrat, fiche de paie...).'."\n";
		$message .= 'Vous pouvez soumettre ces documents en vous connectant à votre espace membre ou en répondant directement à cet email (n\'hésitez-pas à indiquer également des questions ou commentaires éventuels).'."\n\n";

		$message .= $url_base;'\n\n';

		envoi_email($user_email,'Compte professeur non validé sur 20aubac',$message);

		$confirmation_admin = 'Compte refusé - email envoyé à '.$user_email.' + dossier supprimé';
	}
}

// Affichage
require '_vue/vue_admin_validationprofs.php';
