<?php
/*
Principe de la page :
Modifier un sujet existant déjà publié, et modifier également le topic qui est lié
*/

$nivmembrerequis = 3;
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_ressources.php');

$bdd = getBdd($domaine,'site');

$sujet_id = request_var('sujet_id', 0);

// On récupère les infos non modifiables sur le sujet
if ($sujet_id!=0) {
	$reponse = $bdd->query("SELECT * FROM `sujets` WHERE id = '".$sujet_id."'");
	while ($val = $reponse->fetch()) {
		$ressources = unserialize($val['ressources']);
		$ressources_ext = unserialize($val['ressources_ext']);
		$type = $val['type'];
		$topic_id = $val['topic_id'];
		$matiere = $val['matiere'];
	}
}

$submit = (isset($_POST['submit'])) ? true : false;

// On récupère les champs soumis via les formulaires
$sujet2 = request_var('sujet2', '', true);
$textesource2 = request_var('textesource2', '', true);
$oeuvre2 = request_var('oeuvre2', '', true);
$passage2 = request_var('passage2', '', true);
$themes2 = request_var('themes2', '', true);
$edition2 = request_var('edition2', '', true);
$rubrique_autre2 = request_var('rubrique_autre2', '', true);
$rubrique2 = request_var('rubrique2', '', true);
if ($rubrique2!='autre') { $rubrique_autre2 = ''; }
$motscles = request_var('motscles', '', true);
$annale = request_var('annale', '', true);

if ($submit==true) {
	// En cas de submit, on teste la validité des champs soumis
	$erreurs = array();

	// On vérifie que les champs obligatoires soient bien remplis, en distinquant les 2 types (dissertation/commentaire)
	if (
	($type=='dissertation' AND $sujet_id!=0 AND ($sujet2=='' OR $rubrique2==''))
  	OR ($type=='commentaire' AND $sujet_id!=0 AND ($textesource2=='' OR $rubrique2=='' OR ($rubrique2=='autre' AND $rubrique_autre2=='')))
	)
	{ $erreurs['champs_vides'] = 'Vous devez remplir tous les champs'; }

	// On teste la longueur du titre (maximum défini pour phpbb = 146)
	if (
	($type=="dissertation" AND $sujet_id==0 AND strlen($sujet)>146)
	OR ($type=="dissertation" AND $sujet_id!=0 AND strlen($sujet2)>146)
	) {
		$erreurs['titre_troplong'] = 'Le titre de votre sujet est trop long, veuillez le réduire';
	}
	elseif (
		($type=='commentaire' AND $sujet_id==0 AND (strlen($rubrique)+strlen($rubrique_autre)+strlen($oeuvre)+strlen($passage)+strlen($themes))>138)
		OR ($type=='commentaire' AND $sujet_id!=0 AND (strlen($rubrique2)+strlen($rubrique_autre2)+strlen($oeuvre2)+strlen($passage2)+strlen($themes2))>138)
		) {
		$erreurs['titre_troplong'] = 'Le titre de votre sujet est trop long, veuillez réduire un ou plusieurs des éléments suivants : nom de l\'auteur, de l\'oeuvre, passage et thèmes de l\'extrait';
	}

	// Si tout est bon on modifie le sujet et le topic associé
	if (empty($erreurs)) {

		// 3.1 On modifie la base de donnée avec les infos renseignées
		$date_maj = time();

		//Dans le cas d'un commentaire, on constitue le titre du sujet
		if ($type=='commentaire') {
			$sujet2 = creation_sujet($rubrique2,$rubrique_autre2,$oeuvre2,$passage2,$themes2,$textesource2);
		}

		if ($motscles=='') {
			$motscles = motscles($sujet2);
		}

		if ($type=='dissertation') {
			$req = $bdd->prepare("UPDATE `sujets` SET sujet=:sujet, rubrique=:rubrique, annale=:annale, motscles=:motscles, date_maj=:date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
			$req->execute(array(
				'sujet' => $sujet2,
				'rubrique' => $rubrique2,
				'annale'=> $annale,
				'motscles' => $motscles,
				'date_maj' => $date_maj
			));

			} else if ($type=='commentaire') {
				$req = $bdd->prepare("UPDATE `sujets`  SET sujet = :sujet,  rubrique = :rubrique, rubrique_autre =:rubrique_autre, annale=:annale, textesource =:textesource, oeuvre = :oeuvre, passage = :passage, themes = :themes, edition = :edition, motscles = :motscles, date_maj = :date_maj WHERE `sujets`.`id` ='".$sujet_id."'");
				$req->execute(array(
					'sujet' => $sujet2,
					'rubrique' => $rubrique2,
					'rubrique_autre' => $rubrique_autre2,
					'annale'=> $annale,
					'textesource' => $textesource2,
					'oeuvre' => $oeuvre2,
					'passage' => $passage2,
					'themes' => $themes2,
					'edition' => $edition2,
					'motscles' => $motscles,
					'date_maj' => $date_maj
			));
		}

		$message_confirmation = 'La modification a bien été enregistrée.';

	} // FIN du cas $erreur vide (pas d'erreur)
} // FIN du submit


// On récupère les données du sujet
$reponse = $bdd->query("SELECT * FROM sujets WHERE id = '".$sujet_id."'");
while ($val = $reponse->fetch()) {
	$sujet = $val['sujet'];
	$motscles = $val['motscles'];
	$rubrique = $val['rubrique'];
	$rubrique_autre = $val['rubrique_autre'];
	$annale = $val['annale'];
	$textesource = $val['textesource'];
	$oeuvre = $val['oeuvre'];
	$passage = $val['passage'];
	$themes = $val['themes'];
	$edition = $val['edition'];
}

// Affichage
require '_vue/vue_admin_modifiersujet.php';
