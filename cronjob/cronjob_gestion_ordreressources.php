<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Classement des ressources liés aux sujets</title>
</head>
<body>
<?php
include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');
include (CHEMIN_SCRIPT . '/_modele/modele_ressources.php');

$envoi_email = request_var('envoi_email', 0);

$bdd = getBdd($domaine,'site');

$i = 0;

// 1. On trie les ressources liées à chaque sujet selon le scoring suivant : prof = 1000/eleve =0 + Note sur 5*100 + nb_mots/10 000

$reponse = $bdd->query("SELECT * FROM `sujets` WHERE `ressources` !=''");
while ($val = $reponse->fetch()) {
	$sujet_id = $val['id'];
    $sujet = $val['sujet'];
	$type = $val['type'];
	$ressources = unserialize($val['ressources']);

	$ressources_nv_temp = array();
	$ressources_nv = array();

	//1ère étape : on récupère l'id de chaque ressource répertoriée et les infos nécessaires associées pour le scoring...
	foreach ($ressources as $cle=>$valeur) {
		$ressources_tab[$cle] = explode("_", $ressources[$cle]);
		$reponse2 = $bdd->query("SELECT * FROM `ressources` WHERE `id` ='".$ressources_tab[$cle][1]."'");
		while ($val2 = $reponse2->fetch()) {
			$total_notes = $val2['total_notes'];
			$total_votes = $val2['total_votes'];
			$niveau_auteur = $val2['niveau_auteur'];
			$total_moy = $total_notes/$total_votes;
			$nb_mots = $val2['nb_mots'];

			// ...que l'on insère dans un nouveau tableau
			if ($niveau_auteur=='professeur') {
				$ressources_nv_temp['professeur_'.$ressources_tab[$cle][1]] = 1000+($total_moy*100)+($nb_mots/10000);
			}

			if ($niveau_auteur=='eleve') {
				$ressources_nv_temp['eleve_'.$ressources_tab[$cle][1]] = 0+($total_moy*100)+($nb_mots/10000);
			}
		}
	}

	//2ème étape : on trie ce nouveau tableau pour avoir ressources par ordre de scoring décroissant (fonction arsort())
	arsort($ressources_nv_temp);

	//3ème étape : on re-créé un nouveau tableau (numéroté cette fois-ci) avec clé en valeur et ordre conservé
	foreach($ressources_nv_temp as $cle=>$valeur) {
		$ressources_nv[] = $cle;
	}

	$i++;

	if ($envoi_email==0) {
	echo $i." - ".$sujet_id." - ".$sujet."Changement ordre ressources :".print_r($ressources)." => ".print_r($ressources_nv)." <br />"; }

	//Enfin on le met à jour dans la base de données
	$req = $bdd->prepare("UPDATE `sujets` SET `ressources` = :ressources WHERE id='".$sujet_id."'");
	$req->execute(array('ressources' => serialize($ressources_nv)));
}




// Email recap envoyé à admin
if ($envoi_email==1) {

	$message = 'Bonjour,'."\n";
	$message .= '- '.$i.' sujets ont eu leur ressources remis en ordre'."\n\n";

	$message.= 'Pour en savoir plus : https://www.20aubac.fr/cronjob/cronjob_gestion_ordreressources.php'."\n\n";

	envoi_email('contact@20aubac.fr','Mise à jour de l\'ordre des ressources',$message);
}
?></body>
</html>
