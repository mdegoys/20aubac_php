<?php
include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');
include (CHEMIN_SCRIPT . '/_modele/modele_ressources.php');

$envoi_email = request_var('envoi_email', 0);

$bdd = getBdd($domaine,'site');

$result_notes = '';
$i = 0; 
$result_longueur = '';
$j = 0;

$reponse = $bdd ->query("SELECT * FROM `ressources` WHERE `etat`='3' AND `url`=''");
while ($val = $reponse ->fetch()) { 
	$ressource_id = $val['id'];
	$sujet_id = $val['sujet_id'];
    $note_admin = $val['note_admin'];
	$total_notes = $val['total_notes'];
	$total_votes = $val['total_votes'];
	$extrait = $val['extrait'];
	$ressource = $val['ressource'];

	// 1. Verification des notes trop basses (moins de 3 de moyenne)
	if ($total_notes/$total_votes<3) {
		$i++; 
		$result_notes .= $i.'. ressource_id '.$ressource_id.' (sujet_id : '.$sujet_id.') = > '.$total_notes.'/'.$total_votes.' = '.number_format($total_notes/$total_votes,2).'(note admin = '.$note_admin.'.)'."\n";  
	}

	// 2. Vérification de la longueur des ressources, notamment par rapport à l'extrait sélectionné (Si extrait > 50% de la longueur totale => soucis)
	$nb_mots_ressource = str_word_count(minusculesSansAccents($ressource));
	$nb_mots_extrait = str_word_count(minusculesSansAccents($extrait));

	if ($nb_mots_extrait/$nb_mots_ressource>0.5) {
		$j++; 
		$result_longueur .= $j.'. ressource_id = '.$ressource_id.' (sujet_id : '.$sujet_id.') = > '.$nb_mots_extrait.'/'.$nb_mots_ressource.' = '.number_format($nb_mots_extrait/$nb_mots_ressource*100,2).'% (note ressource = '.number_format($total_notes/$total_votes,2).'.)'."\n";	
	}	
}

echo nl2br($result_notes);
echo nl2br($result_longueur);

 
if ($i>0 AND $envoi_email==1) {
    // Email de résultat envoyé à admin
 
    $message = 'Bonjour,'."\n";
	$message .= 'Certaines des ressources sur 20aubac ont des soucis de qualité : '."\n\n"; 	  
	  
	$message .= $result_notes."\n\n";
	  
	$message .= $result_longueur."\n\n";
	  
	$message .= 'Pour en savoir plus : https://www.20aubac.fr/cronjob/cronjob_verif_qualite.php'."\n\n";
	  	  	  
	$message .= $url_base."\n\n";
	  
	envoi_email('contact@20aubac.fr','Soucis de qualité constatés - 20aubac',$message);
}
 ?>