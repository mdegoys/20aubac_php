<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Vérification sur les rubriques</title>
</head>
<body>
<?php
// Vérification sur les rubriques : 
//1) a) retrouvées dans la table `sujets` avec des ressources consultables mais pas dans la table `rubriques` => à rajouter dans la table rubrique, 
//	 b) retrouvées dans la table `sujets` mais sans ressources consultables => à mettre en "autre",
//2) listées dans table `rubriques` sans ressources correspondantes -> à supprimer


include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');

$envoi_email = request_var('envoi_email', 0);

$bdd = getBdd($domaine,'site');

// 1. Vérification valeurs `rubrique` dans la table `sujets` avec des ressources consultables mais pas dans la table `rubriques` => à rajouter dans la table rubrique,
$i = 0;	
$reponse = $bdd->query("SELECT DISTINCT rubrique, type, matiere FROM `sujets` WHERE `rubrique` != '' AND `rubrique` != 'autre' ORDER BY `matiere` DESC, type ASC");
while ($val = $reponse->fetch()) {	
	$rubrique=$val['rubrique'];
	$type = $val['type'];
	$matiere = $val['site'];
	
	echo $rubrique.' - '.$matiere.' - '.$type; 

	// On vérifie que l'on retrouve la rubrique concernée dans la table `rubriques`
	$reponse2 = $bdd ->query("SELECT * FROM `rubriques` WHERE `nomdecode` = '".$rubrique."' AND `type` = '".$type."' AND `matiere` = '".$matiere."'");
	$nb = $reponse2 ->rowCount();

	if ($nb==0) { 
		echo ' => rubrique non trouvée !';
		$i++;
	} 
	else if ($nb>1) { 
		echo ' => résultats doublons !';
		$i++;
	}
	else { echo ' ok'; } 
	echo '<br />'; 
}

// On répare message pour email admin
$result_rub1 = 'Vérif sur les rubriques 1.a) :'."\n";
$result_rub1 .= $i.' sujet(s) dont la rubrique soit pas trouvée soit en doublon => à rajouter dans la table rubriques (ou à supprimer doublon)'."\n\n"; 

echo nl2br($result_rub1);
	
	
// 2. On regarde les rubriques 'autre' sans rubrique_autre ou inversement les rubrique_autre renseignés sans que rubrique soit en 'autre'
$j =0;
$reponse = $bdd->query("SELECT id, sujet, rubrique, rubrique_autre, type, site FROM `sujets` WHERE `rubrique` = 'autre' AND `rubrique_autre` = '' OR `rubrique` != 'autre' AND `rubrique_autre` != ''");
$j = $reponse ->rowCount();	

$result_rub2 = 'Vérif sur les rubriques 1.b) :'."\n";
$result_rub2 .= $j.' rubriques "autre" sans rubrique_autre ou inversement rubrique_autre renseignés sans que rubrique soit en "autre"'."\n\n"; 
	
echo nl2br($result_rub2);

// 3. On compte le nombre de ressources existantes et consultables pour chaque `nomdecode` de la table `rubriques`
$k = 0; 
$l = 0;
$reponse3 = $bdd->query("SELECT * FROM `rubriques`");
while ($val3 = $reponse3->fetch()) { 
	$nomdecode = $val3['nomdecode']; 
	$matiere = $val3['matiere']; 

	// nb de ressources consultables dans chaque rubrique
	$topic_existant = '';
	$l++;
	${'count_'.$l} = 0;
	${'count_annales_'.$l} = 0;
		
	$reponse4 = $bdd->query("SELECT * FROM `sujets` WHERE `rubrique` = '".$nomdecode."' AND `matiere`='".$matiere."'");
	while ($val4 = $reponse4 ->fetch()) { 			
			
	    if ($val4['topic_id']==0) {
		    $topic_existant = 'non';
	    }
	    if ($val4['ressources']!='') {
		   $ressources = unserialize($val4['ressources']);
		   ${'count_'.$l} += count($ressources); 
		}
		if ($val4['ressources_ext']!='') {
		   $ressources_ext = unserialize($val4['ressources_ext']);
		   ${'count_'.$l} += count($ressources_ext);		   
		}
	}
	
	// nb de sujets d'annales avec ressources consultables dans chaque rubrique
	$reponse5 = $bdd->query("
	SELECT * FROM `sujets` 
	WHERE `rubrique` = '".$nomdecode."' AND `matiere`='".$matiere."' AND `annale`!='' AND `ressources`!=''
	OR `rubrique` = '".$nomdecode."' AND `matiere`='".$matiere."' AND `annale`!='' AND `ressources_ext`!=''
	");
	${'count_annales_'.$l} = $reponse5->rowCount();

	$reponse6 = $bdd->exec("
	UPDATE `rubriques` 
	SET `nb_ressources` = '".${"count_".$l}."', `nb_ressources_annales` = '".${"count_annales_".$l}."'
	WHERE `nomdecode`='".$nomdecode."' AND `matiere`='".$matiere."'"); 
	if (${"count_".$l} == 0 AND $topic_existant == 'non') { 
		echo $nomdecode. ' => pas de ressources ('.${'count_'.$l}.') ni topic d\'aide ouvert (topic existant ? '.$topic_existant.') !<br />'; 
		$k++;
	}
}

$result_rub3 = 'Vérif sur les rubriques 2 :'."\n";
$result_rub3 .= $k.' rubriques(s) sans ressources consultables correspondantes ni topic d\'aide ouvert => à supprimer de la table rubriques'."\n\n";
		 
echo nl2br($result_rub3);
	 
	 
// Email d'erreur éventuelle envoyé à admin 
if (($i+$j+$k>0) AND $envoi_email==1) {
   	
	$message = 'Bonjour,'."\n";
	$message .= 'Voici le résultat sur la vérification des rubriques.'."\n\n"; 	  
	  
	$message .= $result_rub1;	  
	$message .= $result_rub2;	  
	$message .= $result_rub3;
	  
	$message .= 'Pour en savoir plus : '."\n";
	$message .= 'https://www.20aubac.fr/cronjob/cronjob_gestion_rubriques.php'."\n\n";
	 	  	  
	$message .= $url_base."\n\n";

	envoi_email('contact@20aubac.fr','Vérification des rubriques sur 20aubac',$message);
}
 ?>
</body>
</html>