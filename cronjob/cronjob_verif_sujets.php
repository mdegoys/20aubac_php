<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Vérifications sur : sujets doublons, casse sujet et point d'interrogations</title>
</head>
<body>
<?php
// Cronjob vérification données sujets doublons + casse + point interrogation - on vérifie chaque point, on compile les résultats et on envoie un email
include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');

$envoi_email = request_var('envoi_email', 0);

$bdd = getBdd($domaine,'site');

?>

<p><strong>Vérif des sujets doublons :</strong></p>

<p>
<?php
// Vérification des sujets doublons (cad qu'il faut qu'ils soient uniques)
$result_doublons = '';

$reponse = $bdd->query("SELECT COUNT(motscles) AS nbr_doublon, id, sujet, motscles, site, ressources
FROM sujets
WHERE topic_id !=''
GROUP BY motscles
HAVING COUNT(motscles) > 1");

$nbr_results = $reponse->rowCount();
if ($nb_pb_doublons>0) {
	while ($val = $reponse->fetch()) {
		$motscles = $val['motscles']; 	
		echo '<p>'.$motscles.'<br />';
		
		
		$reponse2 = $bdd->query("SELECT * FROM sujets WHERE `motscles` = '".$motscles."'");
		while ($val2 = $reponse2->fetch()) {
			$id = $val2['id'];
			$sujet = $val2['sujet'];
			$type = $val2['type'];
			$matiere = $val2['site'];
			$nb_hits = $val2['nb_hits'];
			$ressources = unserialize($val2['ressources']);
			$ressources_tab[0] = explode('_', $ressources[0]);			
	  
	 if ($ressources!='') { ?>
	 <a href="<?php echo $url_base; ?>/<?php echo $matiere; ?>/<?php echo $matiere; ?>-<?php echo $motscles; ?>-r<?php echo $ressources_tab[0]; ?>.html"  target="_blank"><?php } echo $id; ?>. <?php echo $sujet .'(hits :'.$nb_hits.')'; if ($ressources!='') {?></a><?php } else { ?>==> A supprimer manuellement<?php } ?><br />
	<?php }
	   $result_doublons .= ' +'.$motscles."\n";   
	}	
} else { 
    $result_doublons = 'Pas de doublon trouvé'."\n\n"; 
} 
echo nl2br($result_doublons);	
?>
</p>

<p><strong>Vérif de la casse des sujets :</strong></p>

<p>
<?php // Vérification de la casse des sujets (cad qu'il faut une majuscule en première lettre et pas d'espace en début/fin)
$result_casse = '';
$nb_pb_casse = 0;

$reponse2 = $bdd->query("SELECT * FROM `sujets` WHERE `type` ='dissertation' AND topic_id !=''");
while ($val2 = $reponse2->fetch()) {
    $id = $val2['id'];
	$sujet = $val2['sujet']; 
	
	if (ucfirst(trim($sujet))!=$sujet) { 
	    $nb_pb_casse++; 
		$result_casse .= $id.' - '.$sujet.' => '.ucfirst(trim($sujet))."\n"; 
	}	
} 
if ($nb_pb_casse==0) { 
	$result_casse = 'Pas de souci de casse trouvé'."\n\n"; 
}
echo nl2br($result_casse);	
?>
</p>

<p><strong>Vérif des points d'interrogation :</strong></p>
<?php // Vérification de points d'interrogation (cad qu'il faut un espace avant)
$result_interrogation=""; ?>

<?php
$reponse3 = $bdd->query("SELECT * FROM `sujets` WHERE `sujet` LIKE '%?%' AND `sujet` NOT LIKE '% ?%' AND topic_id !=''");

$nb_pb_interrogation = $reponse3->rowCount();
if ($nb_pb_interrogation>0) {
echo '<p>'.$nb_pb_interrogation.' soucis de points d\'interrogation</p>'; 

	$result_interrogation='';

	while ($val3 = $reponse3->fetch()) {
		$id = $val3['id'];
		$sujet = $val3['sujet']; 
		
			$result_interrogation.=$id.' - '.$sujet.' => '.str_replace('?',' ?',$sujet)."\n"; 
		}		
} else { 
    $result_interrogation = 'Pas de souci de point d\'interrogation trouvé'."\n\n"; 
}

echo nl2br($result_interrogation);


if (($nb_pb_doublons+$nb_pb_casse+$nb_pb_interrogation>0) AND $envoi_email==1) {	
	// Email de résultat envoyé à admin
 
   	$message = 'Bonjour,'."\n";
	$message .= 'Des soucis ont été constatés lors de la vérification des données sur les sujets.'."\n\n"; 	  
	  
	$message .= 'Vérif des sujets doublons :'."\n";
	$message .= $result_doublons;
	  
	$message .= 'Vérif de la casse des sujets :'."\n";
	$message .= $result_casse;
	  
	$message .= 'Vérif des points d\'interrogation :'."\n";
	$message .= $result_interrogation;
	
	$message .= 'Pour en savoir plus : https://www.20aubac.fr/cronjob/cronjob_verif_sujets.php'."\n\n";
	  	  
	$message .= $url_base."\n\n";
	  
    envoi_email('contact@20aubac.fr','Soucis constatés sur les sujets - 20aubac',$message);
}	
?>
</body>
</html>