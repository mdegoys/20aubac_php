<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Brouillons inactifs</title>
</head>
<body>
<?php
// Avertissements/Suppression des brouillons non encore soumis (tous les 7 jours)
include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');

$envoi_email = request_var('envoi_email', 0);

$bdd = getBdd($domaine,'site');

function email_avertissement ($sujet_email, $message_corps, $email_destinataire)
{		
	global $auteur, $url_base;
		
	$message = 'Bonjour,'."\n";
	
	$message .= $message_corps;  
	  
	$message .= 'Pour rappel votre nom d\'utilisateur sur le site est :'."\n";
	$message .= $auteur."\n\n";
	
	$message .= 'Merci et à bientôt,'."\n";
	$message .= $url_base."\n\n";
	
	  
	if ($envoi_email==1) {	
	    envoi_email($email_destinataire,$sujet_email,$message);
	}	
}

$i = 0;
$i0 = 0;
$i14 = 0;  
$i21 = 0;  
$i28 = 0;  


// Limite pour soumettre son brouillon = 28 jours
$date_actuelle = time();

// Au bout de 14 jours : 1er avertissement
$date_maj_lim14 = $date_actuelle - 14*24*3600;

// Au bout de 21 jours : avertissement
$date_maj_lim21 = $date_actuelle - 21*24*3600;

// Au bout de 28 jours : suppression
$date_maj_lim28 = $date_actuelle - 28*24*3600;

 
// Verification brouillons/ressources non soumis

$reponse = $bdd->query("SELECT * FROM `ressources` WHERE  `etat`='1' AND `url`=''");
$nb_brouillonsnonsoumis = $reponse->rowCount();

if ($nb_brouillonsnonsoumis) {
	echo '<p>Brouillons non encore soumis :</p>'; 

	while ($val = $reponse->fetch()) {
		$id = $val['id'];
		$sujet_id = $val['sujet_id'];
		$date_ajout = $val['date_ajout'];
		$date_maj = $val['date_maj'];
		$auteur = $val['auteur'];
		$niveau_auteur = $val['niveau_auteur'];
			
		$reponse2 = $bdd->query("SELECT sujet, topic_id FROM `sujets` WHERE  `id`='".$sujet_id."'");
		while ($val2 = $reponse2->fetch()) {
			$sujet = $val2['sujet'];
			$topic_id = $val2['topic_id'];
						
		$reponse3 = $bdd->query("SELECT email, mdp, niveaumembre FROM `membres` WHERE  `pseudo`='".$auteur."'");
		while ($val3 = $reponse3->fetch()) {
			$email_auteur = $val3['email'];
			$mdp_auteur = $val3['mdp'];
			$niveau_auteur = $val3['niveaumembre'];
		}
		}
			
		// On cale la date de mise-à-jour sur la date de création si celle-ci est inexistante
		if ($date_maj==0) { $date_maj = $date_ajout;}

		$i++; 
		echo $i.'. '.$sujet.', rédigé par '.$auteur.' ('.$niveau_auteur.')- Dernière maj : '.date('d/m/Y',$date_maj).' - ('.$id.')';
	 
		// Si on dépasse la date limite on supprime la ressource, voire le sujet correspondant (topic_id="") et on avertit le membre
		if ($date_maj_lim28>$date_maj) {  
			$i28++;  
			echo ' ('.$date_maj_lim28.'  >  '.$date_maj.') ==> suppression à '.$email_auteur.' <br />';
		 
			$bdd->exec("DELETE FROM `ressources` WHERE `id` = '".$id."'");


			//On supprime le sujet correspondant si celui-ci a été créé avec la ressource (= pas de topic_id)
			if ($topic_id==0) { $bdd->exec("DELETE FROM `sujets` WHERE `id` = '".$sujet_id."'"); }


			// Enfin on envoie email informant de la suppression
			$sujet_email = 'Commentaire supprimé de 20aubac';	  
		 
		    $message_corps = 'Le corrigé suivant à été supprimé de 20aubac :'."\n";			
		    $message_corps .= '"'.stripslashes($sujet).'"'."\n\n";			
			
		    $message_corps .= 'Les corrigés non soumis à validation ou modifiés sont supprimés après un mois d\'inactivité.'."\n";		  
		    $message_corps .= 'Vous pouvez bien sûr soumettre de nouveaux corrigés depuis votre compte'."\n\n";			
		    
			email_avertissement ($sujet_email, $message_corps, $email_auteur);
		}
		  
		// Sinon on envoie un juste email d'avertissement	  
		else if ($date_maj_lim21>$date_maj) { 
		
			echo ' ==> avertissement2 à '.$email_auteur.'<br />';
			$i21++; 
	 
			$sujet_email = 'Commentaire supprimé dans 7 jours de 20aubac'; 
			 
			$message_corps = 'Sans modification de votre part, le commentaire suivant sera supprimé dans une semaine : '."\n"; 
			$message_corps .= '"'.stripslashes($sujet).'"'."\n\n";
			
			$message_corps .= 'Les corrigés non soumis à validation ou modifiés sont supprimés après un  mois d\'inactivité. Pour éviter cela veuillez vous connecter et modifier et/ou soumettre votre corrigé.'."\n\n";
		   											  
			email_avertissement ($sujet_email, $message_corps, $email_auteur);
		}
	  	  
		else if ($date_maj_lim14>$date_maj) { 
			echo ' ==> avertissement1 à '.$email_auteur.'<br />';
			$i14++; 
		 
			$sujet_email = 'Corrigé supprimé dans 14 jours de 20aubac';
			 
			$message_corps = 'Sans modification de votre part, le corrigé suivant sera supprimé dans deux semaines : '."\n";
			$message_corps .= '"'.stripslashes($sujet).'"'."\n\n";						
						
			$message_corps .= 'Les corrigés non soumis à validation ou modifiés sont supprimés après un  mois d\'inactivité. Pour éviter cela veuillez vous connecter et modifier et/ou soumettre votre corrigé.'."\n\n";
			   
			email_avertissement ($sujet_email, $message_corps, $email_auteur);
		} 
		else { 
		    $i0++;
			echo ' ==> Pas d\'avertissement avant le '.date('d/m/Y',$date_maj+14*24*3600).'<br />';
		}  
	}

	// Email recap envoyé à admin
	if ($envoi_email==1) {
		
		$message = 'Bonjour,'."\n";
		$message .= 'Voici le bilan des brouillons actuellement en ligne :'."\n\n";
		  
		$message .= '- '.$i0.' brouillons ont été mis à jour depuis moins de 14 jours'."\n";
		$message .= '- '.$i14.' a/ont eu un 1er avertissement'."\n";
		$message .= '- '.$i21.' a/ont eu un 2ème avertissement'."\n";
		$message .= '- '.$i28.' a/ont été supprimé(s)'."\n\n";
		  
		$message .= 'Pour en savoir plus : https://www.20aubac.fr/cronjob/cronjob_gestion_brouillons.php'."\n\n";
		  
		envoi_email('contact@20aubac.fr','Suppression des brouillons inactifs - 20aubac',$message);
	}
} else {
	echo 'Pas de brouillons non encore soumis';	
}	
?></body>
</html>