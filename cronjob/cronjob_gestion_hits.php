<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Remise à 0 des hits sur les sujets - hits - et sur les ressources - consultations (tous les mois)</title>
</head>
<body>
<?php
// Remise à 0 des hits sur les sujets - hits - et sur les ressources - consultation (tous les mois) 

include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');

$envoi_email = request_var('envoi_email', 0);

$bdd = getBdd($domaine,'site');

// Avant la remise à 0, on comptabilise somme des hits/consultations
$reponse = $bdd->query("SELECT SUM(nb_hits_30j) AS somme_nb_hits_30j FROM `sujets`");
while ($val = $reponse->fetch()) { $somme_nb_hits_30j = $val['somme_nb_hits_30j']; }

$reponse2 = $bdd->query("SELECT SUM(nb_consultations_30j) AS somme_nb_consultations_30j FROM `ressources`");
while ($val = $reponse2->fetch()) { $somme_nb_consultations_30j = $val['somme_nb_consultations_30j']; }

// On effectue la remise à 0
$reponse = $bdd->exec("UPDATE `sujets` SET `nb_hits_30j`='0'"); 
$reponse2 = $bdd->exec("UPDATE `ressources` SET `nb_consultations_30j`='0'");  

// Après la remise à 0, on vérifie qu'elle est bien effective
$reponse = $bdd->query("SELECT SUM(nb_hits_30j) AS somme_nb_hits_30j FROM `sujets`");
while ($val = $reponse->fetch()) { $somme_nb_hits_30j_raz = $val['somme_nb_hits_30j']; }
echo "Mise à jour stats hits sur les sujets : passés de ".$somme_nb_hits_30j." => à ".$somme_nb_hits_30j_raz."<p>"; 

$reponse2 = $bdd->query("SELECT SUM(nb_consultations_30j) AS somme_nb_consultations_30j FROM `ressources`");
while ($val = $reponse2->fetch()) { $somme_nb_consultations_30j_raz = $val['somme_nb_consultations_30j']; }
echo "Mise à jour stats de consultations sur les ressources : passées de ".$somme_nb_consultations_30j." => à ".$somme_nb_consultations_30j_raz."<p>";

    
// Email de confirmation envoyé à admin
if ($envoi_email==1) {
	
	$message = 'Bonjour,'."\n";
	$message .= 'Le nombre de hits sujets et de consultations ressources ont été remises  à 0 :'."\n"; 
	$message .= 'Hits sont passés de '.$somme_nb_hits_30j.' => à '.$somme_nb_hits_30j_raz."\n";	
	$message .= 'Consultations sont passées de '.$somme_nb_consultations_30j.' => à '.$somme_nb_consultations_30j_raz."\n\n";	 	  
	 	  
	$message .= $url_base."\n\n";
	  
	envoi_email('contact@20aubac.fr','Mise à jour des hits/consultations sur 20aubac',$message);
}	  
 ?>
</body>
</html>