<?php
function getBdd($domaine = 'www',$application = 'site') {
	// Connexion bdd site
	if ($domaine=='www' AND $application=='site') {
	    $bdd = new PDO('mysql:host=localhost;dbname=20aubacfr;charset=utf8', '20aubacfr', '14789512');
	}
	elseif ($domaine=='test' AND $application=='site') {
	    $bdd = new PDO('mysql:host=localhost;dbname=test20aubacfr;charset=utf8', '20aubacfr', '14789512');
	}
	$bdd ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	return $bdd;
}

// Envoie un email
function envoi_email($destinataire,$sujet,$message,$expediteur = '20aubac <contact@20aubac.fr>') {

	$entete  = "From: ".$expediteur."\n";
	$entete .= "Reply-To: ".$expediteur."\n";
	//$entete .= "Bcc: ".$expediteur."\n";
	$entete .= "X-Mailer: PHP/" . phpversion();
	/* $entete = "Content-Type: text/plain; charset=\"utf-8\""."\n";
	$entete .= "Content-Transfer-Encoding: 8bit"."\n";	 */

	mail($destinataire,"=?UTF-8?B?".base64_encode($sujet)."?=",$message,$entete);
}

// Gabarit : récupération des 5 ressources les plus consultées
function getTop5Ressources($matiere = '') {
	global $domaine;

	$bdd = getBdd($domaine,'site');
	$query = 'SELECT id, sujet, type, motscles, site, ressources
			FROM `sujets`
			WHERE `ressources` !=\'\'';
	if ($matiere!='') {
		$query .= ' AND `site`=\''.$matiere.'\'';
	}
	$query .= ' ORDER BY `nb_hits_30j` DESC LIMIT 5';
	$top5ressources = $bdd->query($query);
	return $top5ressources;
}
