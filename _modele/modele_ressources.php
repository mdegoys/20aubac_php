<?php
// Retourne le nom d'affichage d'une rubrique si nom de code trouvé, retourne FALSE sinon
function ndc2nr($nomdecode,$matiere) {
	global $domaine;

	$bdd = getBdd($domaine,'site');

	$reponse = $bdd->query("SELECT * FROM `rubriques` WHERE `nomdecode`='".$nomdecode."' AND `site`='".$matiere."'");
	$nb_resultat = $reponse->rowCount();

	if ($nb_resultat==1){
		while ($val = $reponse->fetch()){
			$nomreel = $val['nomreel'];
		}
	}
	else {
		$nomreel = FALSE;
	}
	return $nomreel;
}


function bbcode($ressource) {
    $ressource = preg_replace('/\[b\](.+?)\[\/b\]/si', '<strong>$1</strong>', $ressource);
    $ressource = preg_replace('/\[i\](.+?)\[\/i\]/si', '<em>$1</em>', $ressource);
    $ressource = preg_replace('/\[u\](.+?)\[\/u\]/si', '<span style="text-decoration:underline;">$1</span>', $ressource);
    $ressource = preg_replace('/\[tp\](.+?)\[\/tp\]/si', '<h2>$1</h2>', $ressource);

    return $ressource;
}

function suppr_bbcode($ressource) {
	$ressource = str_replace('[b]','', $ressource);
	$ressource = str_replace('[/b]','', $ressource);
	$ressource = str_replace('[i]','', $ressource);
	$ressource = str_replace('[/i]','', $ressource);
	$ressource = str_replace('[u]','', $ressource);
	$ressource = str_replace('[/u]','', $ressource);
	$ressource = str_replace('[tp]','', $ressource);
	$ressource = str_replace('[/tp]','', $ressource);

    return $ressource;
}


// Affiche la barre de notation
function rating_bar($id,$peutvoter) {

	global $bdd, $url_base;

	// On fixe ces 2 valeurs de nombre d'étoiles et et de taille de chacune des étoiles
	$units = 5;
	$rating_unitwidth = 20;

	// On récupère les nombres votes et total des notes
	$query = $bdd->query("SELECT total_votes, total_notes FROM `ressources` WHERE id='".$id."' ");

	while ($val = $query->fetch()) {
	    $total_votes = $val['total_votes'];
	    $total_notes = $val['total_notes'];
	}

	$tense=($total_votes==1||$total_votes==0) ? 'vote' : 'votes'; //plural form votes/vote

	// On génère la barre de notation
	$rating_width = @number_format($total_notes/$total_votes,2)*$rating_unitwidth;
	$rating1 = @number_format($total_notes/$total_votes,1);
	$rating2 = @number_format($total_notes/$total_votes,2);

	if ($peutvoter==0) {

		$static_rater = array();
		$static_rater[] .= "\n".'<div class="ratingblock">';
		$static_rater[] .= '<div id="unit_long'.$id.'">';
		$static_rater[] .= '<ul id="unit_ul'.$id.'" class="unit-rating" style="width:100px;">';
		$static_rater[] .= '<li class="current-rating" style="width:'.$rating_width.'px;">Note actuelle '.$rating2.'/'.$units.'</li>';
		$static_rater[] .= '</ul>';
		$static_rater[] .= '</div>';
		$static_rater[] .= '</div>'."\n\n";

		return join("\n", $static_rater);
	} else {

		$rater = '<div class="ratingblock">';

		$rater .= '<div id="unit_long'.$id.'">';
		$rater .= '<ul id="unit_ul'.$id.'" class="unit-rating" style="width:100px;">';
		$rater .= '<li class="current-rating" style="width:'.$rating_width.'px;">Note actuelle '.$rating2.'/'.$units.'</li>';

		for ($ncount = 1; $ncount <= $units; $ncount++) { // loop from 1 to the number of units
			$rater .= '<li><a href="'.$url_base.'/ressource-noter.html?v='.$peutvoter.'&amp;j='.$ncount.'&amp;q='.$id.'&amp;c='.$units.'" title="'.$ncount.' sur '.$units.'" class="r'.$ncount.'-unit rater" rel="nofollow">'.$ncount.'</a></li>';
		}

		$ncount = 0; // resets the count

		$rater .= '</ul>';
		$rater .= '</div>';
		$rater .= '</div>';
		return $rater;
	}
}

// Retourne les x premiers mots d'une chaine
function resume_xmots($MaChaine,$xmots)
{
   $ChaineTab=explode(' ',$MaChaine);
   $NbMots = min($xmots,count($ChaineTab));
   $NouvelleChaine ='';
   for($i=0;$i<$NbMots;$i++)
   {
      $NouvelleChaine.=' '.''.$ChaineTab[$i].'';
   }
   return $NouvelleChaine;
}

function minusculesSansAccents($chaine){
$chaine = mb_strtolower($chaine, 'UTF-8');
$chaine = str_replace(
        array(
            'à', 'â', 'ä', 'á', 'ã', 'å',
            'î', 'ï', 'ì', 'í',
            'ô', 'ö', 'ò', 'ó', 'õ', 'ø',
            'ù', 'û', 'ü', 'ú',
            'é', 'è', 'ê', 'ë',
            'ç', 'ÿ', 'ñ',
        ),
        array(
            'a', 'a', 'a', 'a', 'a', 'a',
            'i', 'i', 'i', 'i',
            'o', 'o', 'o', 'o', 'o', 'o',
            'u', 'u', 'u', 'u',
            'e', 'e', 'e', 'e',
            'c', 'y', 'n',
        ),
        $chaine
    );

return($chaine);}

function motscles($chaine){

    $motscles = minusculesSansAccents($chaine);
	$i = 0;

	$motscles = str_replace('œ','oe', $motscles);

	// Replacement par des espaces de tous les caractères qui ne soient pas des chiffres, lettre, tiret ou des espaces
	$motscles = preg_replace('#[^A-Za-z0-9- ]#',' ', $motscles);

	// Si commence par le/Le, la/La, les/Les on enlève
	$motscles = preg_replace('#^(l|L)(e|a|es) #', '', $motscles);

	// Si commence par Un, une on enlève
	$motscles = preg_replace('#^(un|une) #', '', $motscles);

	// On supprime toutes les lettres uniques ou les articles
	$motscles = str_replace(' d ',' ', $motscles);
	$motscles = str_replace(' l ',' ', $motscles);
	$motscles = str_replace(' m ',' ', $motscles);
	$motscles = str_replace(' n ',' ', $motscles);
	$motscles = str_replace(' s ',' ', $motscles);
	$motscles = str_replace(' t ',' ', $motscles);

	$motscles = str_replace(' les ',' ', $motscles);
	$motscles = str_replace(' le ',' ', $motscles);
    $motscles = str_replace(' la ',' ', $motscles);
    $motscles = str_replace(' et ',' ', $motscles);

	$motscles = str_replace(' des ',' ', $motscles);
	$motscles = str_replace(' de ',' ', $motscles);
	$motscles = str_replace(' du ',' ', $motscles);

	$motscles = str_replace(' une ',' ', $motscles);
	$motscles = str_replace(' un ',' ', $motscles);


	// Eventuellement si la chaine est longue (à partir de 100 caractères), on supprime les mots de connexion logique
	if (strlen($motscles) > 100) {
	$motscles = str_replace(' a ','', $motscles);
	$motscles = str_replace(' ou ','', $motscles);
	$motscles = str_replace(' ne ','', $motscles);
	$motscles = str_replace(' en ','', $motscles);

	$motscles = str_replace('qu est-ce qu','', $motscles);
	$motscles = str_replace('qu est-ce ','', $motscles);

	$motscles = str_replace('quelles ','', $motscles);
	$motscles = str_replace('quelle ','', $motscles);
	$motscles = str_replace('quels ','', $motscles);
	$motscles = str_replace('quel ','', $motscles);

	$motscles = str_replace('puis-je ','', $motscles);
	$motscles = str_replace('peut-on ','', $motscles);
	$motscles = str_replace('faut-il ','', $motscles);
	$motscles = str_replace('sommes-nous ','', $motscles);
	$motscles = str_replace('suis-je ','', $motscles);

	$motscles = str_replace('pensez-vous ','', $motscles);
	$motscles = str_replace('estimez-vous ','', $motscles);

	$motscles = str_replace(' je ','', $motscles);
	$motscles = str_replace(' moi ','', $motscles);
	$motscles = str_replace(' ils ','', $motscles);
	$motscles = str_replace(' il ','', $motscles);
	$motscles = str_replace(' elle ','', $motscles);
    $motscles = str_replace(' vous ','', $motscles);
	$motscles = str_replace(' nous ','', $motscles);
	$motscles = str_replace(' eux ','', $motscles);

	$motscles = str_replace(' quand ','', $motscles);
	$motscles = str_replace(' qui ','', $motscles);
	$motscles = str_replace(' quoi ','', $motscles);
	$motscles = str_replace(' que ','', $motscles);

	$motscles = str_replace(' souvent ','', $motscles);
	$motscles = str_replace(' jamais ','', $motscles);
	$motscles = str_replace(' toujours ','', $motscles);
	$motscles = str_replace(' avec ','', $motscles);
	$motscles = str_replace(' comme ','', $motscles);
	$motscles = str_replace(' pour ','', $motscles);

	$motscles = str_replace(' cette ','', $motscles);
	$motscles = str_replace(' cet ','', $motscles);
	$motscles = str_replace(' ce ','', $motscles);
	$motscles = str_replace(' on ','', $motscles);
	$motscles = str_replace(' sa ','', $motscles);
	$motscles = str_replace(' son ','', $motscles);
	$motscles = str_replace(' ses ','', $motscles);
	$motscles = str_replace(' se ','', $motscles);
	$motscles = str_replace(' me ','', $motscles);
	$motscles = str_replace(' ma ','', $motscles);
	$motscles = str_replace(' mon ','', $motscles);
	$motscles = str_replace(' mes ','', $motscles);
	$motscles = str_replace(' leurs ','', $motscles);
	$motscles = str_replace(' leur ','', $motscles);
	$motscles = str_replace(' c est ','', $motscles);
	$motscles = str_replace(' est ','', $motscles);
	$motscles = str_replace(' sommes-nous ','', $motscles);
	$motscles = str_replace(' est-on ','', $motscles);
	$motscles = str_replace(' est-elle ','', $motscles);
	$motscles = str_replace(' est-il ','', $motscles);
	$motscles = str_replace(' sont-ils ','', $motscles);
	$motscles = str_replace(' sont-elles ','', $motscles);
	$motscles = str_replace(' faut-il ','', $motscles);
	$motscles = str_replace(' qu-est-ce ','', $motscles);
    $motscles = str_replace(' est-ce ','', $motscles);
    $motscles = str_replace(' parait-elle ','', $motscles);
	$motscles = str_replace(' parait-il ','', $motscles);

    $motscles = str_replace(' t-elle ','', $motscles);
    $motscles = str_replace(' t-il ','', $motscles);

	$motscles = str_replace(' puis-je ','', $motscles);
	$motscles = str_replace(' peut-on ','', $motscles);
	$motscles = str_replace(' peut-il ','', $motscles);
    $motscles = str_replace(' peut-elle ','', $motscles);
	$motscles = str_replace(' pouvons-nous ','', $motscles);
	$motscles = str_replace(' pourrais-je ','', $motscles);

	$motscles = str_replace(' pourrions-nous ','', $motscles);

    $motscles = str_replace(' qu ','',$motscles);}


	// On transforme enfin les espaces en tirets, on les rend unique si plusieurs sont à la suite et éventuellement on supprime tirets si en début ou en fin de chaîne
	$motscles = str_replace(' ','-', $motscles);
	$motscles = preg_replace('#(-){2,}#', '-', $motscles);

	if ($motscles{0}=='-') { $motscles = substr($motscles, 1);}
	if  ($motscles{strlen($motscles)-1}=='-') { $motscles = substr($motscles, 0, -1);}


return ($motscles);}


// Retourne le résultat mysql de la recherche
function rechercheSujets ($mots_recherche,$matiere = '',$debut_limit = '',$nb_limit = '', $mode = 'etendu')  {

	global $bdd,$domaine;

	//on prépare la requête SQL
	$sql = "SELECT id, sujet, type, motscles, site, ressources, ressources_ext, topic_id, MATCH (sujet) AGAINST (:recherche) AS indice FROM `sujets` WHERE";

	$sql .= " MATCH (sujet) AGAINST (:recherche)";
	if ($matiere!='') {
	    $sql .= " AND `site`='".$matiere."'";
	}

	// Si on est en mode étendu (par défaut, on recherche sur toute les sujets, sinon on recherche aue sur les sujets ayant des ressources)
	if ($mode=='etendu') {
		$sql .= " AND `topic_id`!='0'";
	}
	else if ($mode=='restreint') {
		$sql .= " AND `ressources`!='' OR `ressources_ext`!=''";
		if ($matiere!='') {
			$sql .= " AND `site`='".$matiere."'";
		}
	}

	$sql .= " ORDER BY indice DESC, ressources DESC";

	if ($nb_limit!='') {
		$sql .= " LIMIT ".$debut_limit.",".$nb_limit; }
	else {
		$sql .= " LIMIT 0 , 150";
	}

		/* if ($domaine=="test") { echo "<p>".$sql."</p>";} */

		// on execute la requête SQL préparée
	$result = $bdd->prepare($sql);
	$result->execute(array('recherche' => $mots_recherche));

    return($result);
}


function creation_sujet($rubrique,$rubrique_autre,$oeuvre,$passage,$themes,$textesource) {

	global $bdd;

	if ($rubrique=='autre') { $sujet = $rubrique_autre; }
	else {

	// Récupération du nom réel de l'auteur pour le titre
	$reponse = $bdd->query('SELECT * FROM rubriques WHERE nomdecode=\''.$rubrique.'\'');
	while ($val = $reponse->fetch()) {  $rubrique_reel = $val['nomreel'];}

	$sujet = $rubrique_reel; }

	if ($oeuvre!='') {
	$sujet .= ', ';
	$sujet .= $oeuvre; }

	if ($passage!='') {
	$sujet .= ' - ';
	$sujet .= $passage; }

	if ($themes!='') {
	$sujet .= ' : ';
	$sujet .= $themes; }

	return($sujet);
}


// Pour récupérer le nom de domaine à partir d'une URL
function getRootDomain ($url) {
	$buff = preg_replace('/^[\w]{2,6}:\/\/([\w\d\.\-]+).*$/','$1',$url);
	preg_match('#^[\w.]*\.(\w+\.[a-z]{2,6})[\w/._-]*$#',$buff,$match);
	if (isset($match[1]) AND strlen($match[1])>2){
		$buff = $match[1];
	}
	return $buff;
}
