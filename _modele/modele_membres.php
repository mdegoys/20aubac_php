<?php
// On génère un mot de passe à l'inscription
function genmdp() {
	$key = '';

	//nombre de caractères total
	$taille = 12;

	//chaîne utilisée, on peut rajouter des lettres
	$chiffre = '123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ';
	srand(time());

	for ($i=0;$i<$taille;$i++) {
		//on mélange le tout
		$key .= substr($chiffre,(rand()%(strlen($chiffre))),1);
	}
	
	return($key);
}

// Pour les professeurs qui doivent valider leur compte : on détermine le dossier en fonction de leur pseudo
function getDossier($pseudo) {
	$dossier_pseudo = strtolower($pseudo);
	$dossier_pseudo = strtr($dossier_pseudo, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
	$dossier_pseudo = preg_replace('/([^.a-z0-9]+)/i', '_', $dossier_pseudo);
	return $dossier_pseudo;
}	