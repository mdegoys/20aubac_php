<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';
include ('_modele/modele_ressources.php');

$matiere = request_var('matiere', '');
if ($matiere=='philo') { $type = request_var('type', 'dissertation'); }
else if ($matiere=='francais') { $type = request_var('type', 'commentaire'); }

$rubrique = utf8_normalize_nfc(request_var('rubrique', ''));
$selection = utf8_normalize_nfc(request_var('s', ''));

// Récupération des sujets pour une rubrique/type de sujet/matière donnés
function getListeSujets($matiere,$type,$rubrique,$selection='') {
	global $domaine;

	$query="
	SELECT s.id, s.type, s.sujet, s.motscles, s.annale, s.ressources, s.ressources_ext, s.topic_id
	FROM sujets as s
	INNER JOIN `ressources` AS r
	ON r.sujet_id = s.id
	WHERE";

	if ($selection=='annales') {
		$query.=" s.type='".$type."' AND s.rubrique='".$rubrique."' AND s.`site`='".$matiere."' AND s.`ressources`!='' AND s.`annale`!=''
		OR s.type='".$type."' AND s.rubrique='".$rubrique."' AND s.`site`='".$matiere."' AND s.`ressources_ext`!='' AND s.`annale`!=''";
	}
	else {
		$query.=" s.type='".$type."' AND s.rubrique='".$rubrique."' AND s.`site`='".$matiere."' AND s.`ressources`!=''
		OR s.type='".$type."' AND s.rubrique='".$rubrique."' AND s.`site`='".$matiere."' AND s.`ressources_ext`!=''";
	}
	$query.=" GROUP BY r.sujet_id ORDER BY sujet";

	$bdd = getBdd($domaine,'site');
	$listesujets = $bdd->query($query);
	return $listesujets;
}

// Récupération des rubriques pour un groupe/matière donnés
function getListeRubriques($matiere,$groupe){
	global $domaine;

	$bdd = getBdd($domaine,'site');
	$listerubriques = $bdd->query("
		SELECT nomdecode, nomreel
		FROM rubriques
		WHERE groupe='".$groupe."' AND `site`='".$matiere."' AND `nb_ressources`!='0'
		ORDER BY naissance");
	return $listerubriques;
}


// On vérifie que la rubrique existe bien et on en profite pour récupérer son nom d'affichage
if ($rubrique!='') {
    $nomreel = ndc2nr($rubrique,$matiere);

	if ($nomreel==FALSE) {
		if ($matiere=='philo') { header ('Location:'.$url_base.'/philo/corriges-'.$type.'.html'); exit;}
	    elseif ($matiere=='francais') { header ('Location:'.$url_base.'/francais/commentaires-composes.html'); exit;}
	}
}

// Récupération des sujets pour une rubrique donnée
if ($rubrique!='') {
	$nb_listesujets_annales = getListeSujets($matiere,$type,$rubrique,'annales')->rowCount();
	$nb_listesujets = getListeSujets($matiere,$type,$rubrique)->rowCount();


	if ($selection!='') {
		$listesujets = getListeSujets($matiere,$type,$rubrique,$selection);
	}
	else {
		$listesujets = getListeSujets($matiere,$type,$rubrique);
	}
}

// Récupération des rubriques pour une matière et un groupe donnés
else {
	for($nb =0; $nb < count(${'groupe_'.$type.'_'.$matiere}); $nb++) {
        $listerubriques[$nb] = getListeRubriques($matiere,${'groupe_'.$type.'_'.$matiere}[$nb]);
	}
}

require '_vue/vue_ressources.php';
