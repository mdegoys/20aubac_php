<?php
// Contrôle des billets svp : on regarde le niveau du membre éventuel (ou on le force à 0) et on le redirige si besoin
$request_uri = request_var('REQUEST_URI','');


// On autorise temporairement l'accès aux superglobales pour récupérer les valeurs des sessions
$request->enable_super_globals();
session_start();

// On regarde également si session existante de visite d'un sujet ("visite") ou de consultations d'une ressource ("consultation")
foreach ($_SESSION as $key => $value) {
	if (strpos($key, 'visite_')!==FALSE) { ${$key} = 1; }
	if (strpos($key, 'consultation_')!==FALSE) { ${$key} = 1; }
}

// FIN - On autorise temporairement l'accès aux superglobales pour récupérer les valeurs des sessions
$request->disable_super_globals();

// Vérification de l'autorisation : si le membre n'est pas connecté on lui attribue la variable $nivmembre=0
/* $user->session_begin(); */
/* $auth->acl($user->data); */
/* $user->setup(); */

if (!isset($user->data['username']) OR ($user->data['username'] == 'Anonymous')) {
  $nivmembre = 0;
	$pseudomembre = '';
	$validmembre = 0;
}

// Autrement on lui attribue la variable $nivmembre et les droits phpbb correspondants
else {
	if ($user->data['group_id']==10) { $nivmembre = 1; }
	else if ($user->data['group_id']==9) { $nivmembre = 2; $validmembre = 0; }
	else if ($user->data['group_id']==8) { $nivmembre = 2; $validmembre = 1; }
	else if ($user->data['group_id']==5) { $nivmembre = 3; }
	$pseudomembre = $user->data['username'];
}


// On vérifie les droits de l'utilisateur par rapport aux droits requis sur la page
if (!isset($nivmembrerequis)) { $nivmembrerequis = 0; }

if ($nivmembre==2 AND $validmembre==0 AND $nivmembrerequis>0 AND $page!='membres_statut') {
	header('Location: membres-statut.html');
}

// Si droits insuffisants on redirige vers page de login avec en paramètre la page demandée
if (($nivmembrerequis==3 AND $nivmembre<3) OR ($nivmembrerequis==2 AND $nivmembre<2) OR ($nivmembrerequis==1 AND $nivmembre<1))
{
	if ($request_uri!='') {
	    header("Location: compte-acceder.html?e=1&nivmembre=".$nivmembre."&url_demandee=".$request_uri."");}
	else {
	    header("Location: compte-acceder.html?e=1&nivmembre=".$nivmembre);
	}
}
