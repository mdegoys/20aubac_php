<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';

$bdd = getBdd($domaine,'site');

// nb de ressources consultables total
$nb_ressources = 0;
$reponse = $bdd->query("SELECT * FROM `sujets` WHERE `ressources`!='' OR `ressources_ext`!=''");

while ($val = $reponse->fetch()) {
    $ressources = unserialize($val['ressources']);

	if (!empty($ressources)) {
	    $nb_ressources = $nb_ressources+count($ressources);
	}

	$ressources_ext = unserialize($val['ressources_ext']);
	if (!empty($ressources_ext)) {
	    $nb_ressources = $nb_ressources+count($ressources_ext);
	}
}

// 5 dernières ressources validées
$reponse_dernieresressources = $bdd->query("
SELECT r.id AS ressource_id, r.sujet_id AS sujet_id, r.etat AS ressource_etat, r.niveau_auteur AS niveau_auteur, s.sujet AS sujet, s.motscles AS sujet_motscles, s.type AS sujet_type, s.site AS matiere, s.ressources AS ressources
FROM `sujets` AS s
INNER JOIN `ressources` AS r
ON r.sujet_id = s.id
WHERE r.etat='3'
ORDER BY r.date_maj DESC LIMIT 0,5");

$nbResultat_dernieresressources = $reponse_dernieresressources->rowCount();

// Décompte avant le jour du bac
// $dateactuelle = time();
// $datebac_philo = mktime(8, 0, 0, 6, 15, 2017); // timestamp du 15 juin 2017 à 8h
// $datebac_francais = mktime(14, 0, 0, 6, 15, 2017); // timestamp du 15 juin 2017 à 14h

// $tempsrestant = round((${"datebac_".$matiere}-$dateactuelle)/(60*60*24)); // temps restant en jour

// Affichage
require '_vue/vue_accueil.php';
