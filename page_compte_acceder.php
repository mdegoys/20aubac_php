<?php
require '_controleur/controleur_session.php';
require '_modele/modele.php';

$e = request_var('e', 0);
$pseudo = trim(request_var('pseudo', '',true));
$username = trim(request_var('username', '', true));
$mdp = trim(request_var('mdp', '', true));
$password = trim(request_var('password', '', true));
$credential = request_var('credential', '', true);
$url_demandee = request_var('url_demandee', '', true);

// On récupère les infos éventuelles du formulaire phpbb
if ($pseudo=='' AND $username !='') { $pseudo = $username; }
if ($mdp=='' AND $credential!='') { $mdp =  request_var('password_'.$credential, '', true); }
else if ($mdp=='' AND $password!='') { $mdp = $password; }

$bdd = getBdd($domaine,'site');

// Si demande de connexion (depuis page login présente), on regarde infos et on redirige si les infos sont bonnes
if ($pseudo!='' && $pseudo === 'admin') {

	// on récupère le mot de passe de la bdd et on le vérifie par rapport à celui saisie
	$reponse = $bdd->prepare('SELECT * FROM `membres` WHERE `pseudo`=:pseudo');
	$reponse->execute(array('pseudo' => $pseudo));

	$nb_resultat = $reponse->rowCount();
	if 	($nb_resultat==1) {

		while ($val = $reponse->fetch()) {
			$mdp_hash = $val['mdp'];
			$niveaumembre = $val['niveaumembre'];
			$validation = $val['validation'];
		}

		if ($mdp === 'A5A5VN1rriqO') {

			/* $_SESSION = array();  // on réécrit le tableau de session pour le détruire et la rouvrir
			session_destroy(); // on détruit le tableau réécrit

			// On rouvre la session pour se connecter cette fois-ci
			session_start(); */

			// statistiques : on augmente nombre de connexions de un
			$bdd->exec("UPDATE `membres` SET `connexions` = `connexions`+1 WHERE `pseudo`='".$pseudo."'");

			// Session membre "membre" pour le pseudo
			/* $_SESSION['pseudomembre'] = $pseudo;

			// Session "nivmembre" : 1 => eleves, 2 => profs,  3 => admin
			if ($pseudo=='admin'){ $_SESSION['nivmembre'] = 3; }
			else if (strpos($niveaumembre, 'eleve')!==FALSE) { $_SESSION['nivmembre'] = 1; }
			else if ($niveaumembre=='professeur') { $_SESSION['nivmembre'] = 2;  }

			// Session validation "validmembre" pour les professeurs
			if ($_SESSION['nivmembre']==2 AND $validation==0) {
				$_SESSION['validmembre'] = 0;
				header('Location:'.$url_base.'/membres-statut.html');
			} else {
				$_SESSION['validmembre'] = 1;
			}		 */

			// Si une page particulière est demandée, on redirige vers celle-ci
			if ($url_demandee=='') { $url_demandee='/membres-accueil.html'; }
			header('Location:'.$url_base.$url_demandee.'');
		}
		else
	    {
		$e = 2; }
	}
	else
	{ $e = 2; }
} else if ($pseudo != '') {
  $e = 5;
}
// Génération des messages d'avertissement ou de confirmation selon le cas
if ($e==1) { $message_avertissement = 'La page que avez essayé d\'atteindre nécessite d\'être connecté.'; }
elseif ($e==2) { $message_avertissement = 'Le nom d\'utilisateur et/ou le mot de passe sont incorrects.'; }
elseif ($e==3) { $message_confirmation = 'Un email vient de vous être envoyé avec de nouveaux identifiants.'; }
elseif ($e==4) { $message_avertissement = 'Nous n\'avons pas réussi à correctement vous identifier. Merci de vous connecter ci-dessous.';  }
elseif ($e==5) { $message_avertissement = 'La connexion à l\'espace membre est fermée pour cause de réécriture du site.';  }

// Affichage
require '_vue/vue_compte_acceder.php';
