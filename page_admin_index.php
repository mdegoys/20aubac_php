<?php
$nivmembrerequis = 3;
require '_controleur/controleur_session.php';
require '_modele/modele.php';

$bdd = getBdd($domaine,'site');

// on récupère nombre de brouillons/profs en attente de validation
// nb de ressources en attente de validation
$reponse5 = $bdd->query("SELECT * FROM `ressources` WHERE etat='2' AND `ressource`!=''");
$row_count5 = $reponse5->rowCount();

// nb de ressources externes/url en attente de validation
$reponse6 = $bdd->query("SELECT * FROM `ressources` WHERE etat='2' AND `url`!=''");
$row_count6 = $reponse6->rowCount();

// nb de sujets en attente de validation
$reponse7 = $bdd->query("SELECT * FROM `sujets` WHERE `topic_id`='' AND `topic_question`!=''");
$row_count7 = $reponse7->rowCount();

// Affichage
require '_vue/vue_admin_index.php';
