<?php
$nivmembrerequis = 2;
require '_controleur/controleur_session.php';
require '_modele/modele.php';		 

$submit = (isset($_POST['submit'])) ? true : false;

$commentaire = request_var('commentaire', '', true);

// On autorise temporairement l'accès aux superglobales pour récupérer les valeurs des fichiers envoyés
$request->enable_super_globals();

if (isset($_FILES['fichierA']['name'])) { 
    $fichierA = $_FILES['fichierA']['name'];
	$fichierA_tmp = $_FILES['fichierA']['tmp_name'];
	$taille_fichierA = filesize($fichierA_tmp);
}
if (isset($_FILES['fichierB']['name'])) { 
    $fichierB = $_FILES['fichierB']['name'];
	$fichierB_tmp = $_FILES['fichierB']['tmp_name'];
	$taille_fichierB = filesize($fichierB_tmp);
}

// FIN - On autorise temporairement l'accès aux superglobales pour récupérer les valeurs des fichiers envoyés
$request->disable_super_globals();


// Taille maximale des fichiers : 5 Mbits
$taille_maxi = 1024000*5;

// on détermine nom du dossier en fonction du pseudo
$dossier_pseudo = getDossier($pseudomembre);
$dossier_chemin = CHEMIN_SCRIPT.'/contenu/doc_membre/' .$dossier_pseudo; 	

if ($submit==true) {
	
	$erreurs = array();
		
	$fichierA = basename(strtolower($fichierA));
	$fichierA = strtr($fichierA, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
	$fichierA = preg_replace('/([^.a-z0-9]+)/i', '-', $fichierA);
	
	$fichierB = basename(strtolower($fichierB));
	$fichierB = strtr($fichierB, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
	$fichierB = preg_replace('/([^.a-z0-9]+)/i', '-', $fichierB);	
	
	// Vérification de l'existence d'un document
	if($fichierA=='' AND $fichierB=='') { 
	    $erreurs['absence_fichier'] = 'Vous devez envoyer au moins un document.';
	}	
	
	// Vérification de la taille des fichiers
	else if ($taille_fichierA>$taille_maxi OR $taille_fichierB>$taille_maxi) { 
	    $erreurs['taille_fichier'] = 'Vous ne pouvez envoyer que des fichiers ayant une taille inférieure à 5 Mo.';
	}
	
	// Si tout est bon on crée le dossier si nécessaire, copie les fichiers en envoie un email d'avertissement
	else {
		// création du dossier du membre (s'il n'existe pas déjà)
		if (!is_dir($dossier_chemin)) {		
			mkdir ($dossier_chemin, 0700);			
		}
		
		// On envoie le fichier dans le dossier du membre
		copy($fichierA_tmp, $dossier_chemin . '/' . $fichierA);
		
		if ($fichierB!='') {
			copy($fichierB_tmp, $dossier_chemin . '/' . $fichierB);
		} 
			
			
		// On s'envoie un email pour prévenir de l'ajout de document(s)
		$message = 'Bonjour,'."\n";
        $message .= 'Un professeur a ajouté un ou des documents pour valider son compte.'."\n"; 
		$message .= 'Nom d\'utilisateur : '.$pseudomembre."\n";		  
				
		$message .= 'Le ou les documents se trouvent ici : '.$url_base.'/contenu/doc_membre/'.$dossier_pseudo.'/ '."\n\n";
		
		if ($commentaire!='') {
		    $message .= "Commentaire du professeur : ".$commentaire." \n\n"; 
		}
 		  
		$message .= $url_base."\n\n";
		
		envoi_email('contact@20aubac.fr','Validation compte professeur sur 20aubac',$message);
		
		$message_confirmation = 'L\'envoi du (des) document(s) a bien été effectué !<br /> Vous recevrez sous 72h une réponse concernant l\'activation de votre compte.';
	}
}

// Affichage
require '_vue/vue_membres_statut.php';