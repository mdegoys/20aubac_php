﻿<?php
// Avoir la distribution des corrigés selon leurs notes obtenues ou leurs longueurs

include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');

$bdd = getBdd($domaine,'site');

// nb total de ressources internes consultables : en francais et en philo
$reponse0 = $bdd ->query('
SELECT r.id AS ressource_id, r.sujet_id AS sujet_id, r.etat AS ressource_etat, r.niveau_auteur AS niveau_auteur, s.sujet AS sujet, s.motscles AS sujet_motscles, s.type AS sujet_type, s.matiere AS matiere
FROM `sujets` AS s
INNER JOIN `ressources` AS r
ON r.sujet_id = s.id
WHERE r.`ressource`!=\'\' AND r.etat=3 AND s.`matiere`=\'francais\'
');
$count0 = $reponse0->rowCount();

// nb total de ressources internes consultables
$reponse1 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\'');
$count1 = $reponse1->rowCount();

// nb total de ressources internes consultables ayant une note supérieure à 4,5
$reponse2 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND (total_notes/total_votes)>\'4.5\'');
$count2 = $reponse2->rowCount();

// nb total de ressources internes consultables ayant une note supérieure à 4,5
$reponse2a = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND (total_notes/total_votes)>\'4.5\'');
$count2a = $reponse2a->rowCount();


// nb total de ressources internes consultables ayant une note entre 3.5 et 4.5 inclus
$reponse3 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND (total_notes/total_votes)<=\'4.5\' AND (total_notes/total_votes)>\'3.5\'');
$count3 = $reponse3->rowCount();

// nb total de ressources internes consultables ayant une note entre 3 et 3.5 inclus
$reponse4 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND (total_notes/total_votes)<=\'3.5\' AND (total_notes/total_votes)>\'3\'');
$count4 = $reponse4->rowCount();

// nb total de ressources internes consultables ayant une note inférieure ou égale à 3
$reponse5 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND (total_notes/total_votes)<=\'3\'');
$count5 = $reponse5->rowCount();

echo '<p>Il y a '.$count1.' ressources internes consultables au total :<br /><br />';

echo 'Dont '.$count2.' ('.$count0.' selon 2ème décompte) ont une note supérieure à 4.5<br />';
echo 'Dont '.$count3.' ont une note entre 3.5 et 4.5 inclus<br />';	
echo 'Dont '.$count4.' ont une note entre 3 et 3.5 inclus<br />';	
echo 'Dont '.$count5.' ont une note inférieure ou égal à 3<br /></p>';	

// nb total de ressources internes consultables ayant plus de 2000 mots
$reponse6 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND `nb_mots`>\'2000\'');
$count6 = $reponse6->rowCount();

// nb total de ressources internes consultables ayant entre 1500 et 2000 mots inclus
$reponse7 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND `nb_mots`<=\'2000\' AND `nb_mots`>\'1500\'');
$count7 = $reponse7->rowCount();

// nb total de ressources internes consultables ayant entre 1000 et 1500 mots inclus
$reponse8 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND `nb_mots`<=\'1500\' AND `nb_mots`>\'1000\'');
$count8 = $reponse8->rowCount();

// nb total de ressources internes consultables ayant moins ou 1000 mots exactement
$reponse9 = $bdd ->query('SELECT * FROM `ressources` WHERE `ressource`!=\'\' AND `etat`=\'3\' AND `nb_mots`<=\'1000\'');
$count9 = $reponse9->rowCount();

	
echo '<p>Concernant la longueur :<br /><br />';

echo $count6.' ont plus de 2000 mots<br />';
echo $count7.' ont entre 1500 et 2000 mots inclus<br />';	
echo $count8.' ont entre 1000 et 1500 mots inclus<br />';	
echo $count9.' moins ou 1000 mots exactement<br /></p>';

?>