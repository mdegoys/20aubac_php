﻿<?php
// Vérification des ressources pour lesquelles le compte de l'auteur aurait été supprimé => on le récrée avec une fausse adresse email pour pas qu'il puisse être récupéré (et ressources modifiées)

// HTTP_HOST = Contenu de l'en-tête Host: de la requête courante, si elle existe (Source : php.net)
$tab_host = explode (".", $_SERVER['HTTP_HOST']);
$site = $tab_host[1];
$domaine = $tab_host[0];
include ("../config.php");
include ("../modele/modele.php"); 
include ("../modele/modele_membres.php");

// Création compte phpbb 
require($phpbb_root_path . "includes/functions_user." . $phpEx);
			
$user->session_begin();
$auth->acl($user->data);
$user->setup("ucp");

$bdd=getBdd($domaine,"site");

$reponse1 = $bdd ->query("
SELECT r.auteur AS auteur, m.email AS email, r.niveau_auteur AS niveau_auteur, r.id AS ressource_id
FROM `ressources` AS r
LEFT JOIN `membres` AS m
ON auteur = m.pseudo
WHERE `email` IS NULL AND r.ressource!=''
GROUP BY `auteur`
");

$count1 = $reponse1->rowCount();
$i=0;

if ($count1>0) {
	echo "Il y a ".$count1." membres ayant créé des ressources mais pour lesquels le compte est introuvable :<br /><br />";
	while ($val = $reponse1 ->fetch()) { 
				
		// Création d'un compte à partir des infos données et déduites
		echo $pseudo = $val["auteur"];
		echo $mdp = genmdp();
		echo $mdp_hash = phpbb_hash($mdp);
		echo $date_ins = time();
		echo $email = strtolower($val["auteur"]."_753951@yopmail.com");
		
		$reponse2 = $bdd ->query("
		SELECT `site`
		FROM `sujets`
		WHERE `ressources` LIKE '%".$val["niveau_auteur"]."_".$val["ressource_id"]."%'");
		
		while ($val2 = $reponse2 ->fetch()) { 
		   echo $site=$val2["site"];
		}
		
		if ($site=="20enphilo" AND $val["niveau_auteur"]=="eleve") { $niveaumembre="eleve_terminale"; }
		elseif ($site=="20enfrancais" AND $val["niveau_auteur"]=="eleve") { $niveaumembre="eleve_premiere"; }
		else { $niveaumembre="professeur"; }
		
		echo $niveaumembre;
		
		// on crée le compte phpbb associé	
		$error=array();
			
	    $data = array(
	    "username" => utf8_normalize_nfc($pseudo),
	    "password" => $mdp,
	    "email" => $email
	    ); 
		 
	    $user_row = array(
		"username" => $data["username"],
		"user_password" => phpbb_hash($data["password"]),
		"user_email" => $data["email"],
		"group_id" => 2,
		"user_timezone" => (float) $config["board_timezone"],
		"user_lang" => basename($user->lang_name),
		"user_type" => USER_NORMAL,
		"user_actkey" => "",
		"user_ip" => $user->ip,
		"user_regdate" => time(),
		"user_inactive_reason" => 0,
		"user_inactive_time" => 0,
	    );
	    $user_id = user_add($user_row);
	    if ($user_id === false)
	    {
		    trigger_error("NO_USER", E_USER_ERROR);			
	    }
		else {
			echo "membre créé : ".$user_id." <br />";
			
			$req = $bdd->prepare("INSERT INTO membres (pseudo,mdp,email,nom,prenom,telephone,presentation,niveaumembre,emailing_".$site.",site,date_ins) 
			VALUES (:pseudo,:mdp,:email,:nom,:prenom,:telephone,:presentation,:niveaumembre,:emailing,:site,:date_ins)");
			$req->execute(array(
				"pseudo" => $pseudo,
				"mdp" => $mdp_hash,
				"email" => $email,
				"nom" => "",
				"prenom" => "",
				"telephone" => "",
				"presentation" => "",
				"niveaumembre" => $niveaumembre,
				"emailing" => 0,
				"site" => $site,
				"date_ins" => $date_ins
				));
		}
		
			  
	    // infos sur la fonction : http://wiki.phpbb.com/display/DEV/Function.group+user+add
	    if ($val["niveau_auteur"]=="eleve") {	$user_group = group_user_add(10, $user_id, false, false, true); }
		elseif ($val["niveau_auteur"]=="professeur") { $user_group = group_user_add(9, $user_id, false, false, true); } 	 
		
	}
} else {
	echo "Il n'y a pas de membre ayant créé des ressources mais pour lesquels le compte est introuvable.<br /><br />";
}	
?>