<?php
// Import données fichier import_ressources_ext.csv
// HTTP_HOST = Contenu de l'en-tête Host: de la requête courante, si elle existe (Source : php.net)
$tab_host = explode (".", $_SERVER['HTTP_HOST']);
$site = $tab_host[1];
$domaine = $tab_host[0];

include ('../config.php');
include ('../_modele/modele.php');
include ('../_modele/modele_ressources.php');

$bdd = getBdd($domaine,'site');

$row = 1;
if (($handle = fopen("import_ressources_ext.csv", "r")) !== FALSE) {
	while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		$num = count($data);
		echo "<p>";
		for ($c=0; $c < $num; $c++) {
			
			// Soit c'est la première ligne et on reprend les valeurs pour l'intitulé des variables...
			if ($row==1) {
				$nom_var[$c] = $data[$c];
			}
			// ... autrement on affiche simplement les valeurs
			else {
				
				echo $nom_var[$c]." : ".$data[$c];								
				if ($c<$num-1) { echo " - "; } else { echo "<br />"; }
			}			
		}
		// 0. On vérifie que l'URL n'est pas déjà dans la base de donnée - sinon on ignore
		$reponse = $bdd->prepare("SELECT * FROM `ressources` WHERE `url`=:url");
	    $reponse->execute(array("url" => $data[array_search("url",$nom_var)]));  
		
		$total_url = $reponse->rowCount(); 
			  
		if ($total_url >=1) {
				echo "URL déjà présente => ignorée";
		}
		else {
			
			print_r($data);
		
			// 1. On vérifie qu'un sujet_id différent de 0 est renseigné - sinon on le créé
			if ($data[array_search("sujet_id",$nom_var)]==0 AND $row!=1) {
				
				
				
				echo "=> création sujet :"; 
				
				$date_ajout= time();
							 
				// Dans le cas d'un commentaire, on constitue le titre du sujet 
				if ($data[array_search("type",$nom_var)]=="commentaire") {	
					// Si thèmes non indiqués, on prend les premiers mots du texte étudié
					if ($data[array_search("themes",$nom_var)]=="") {
					    $themes="«".resume_xmots($data[array_search("textesource",$nom_var)],"5")."...»"; 
					} else {
						$themes = $data[array_search("themes",$nom_var)];
					}
					
					$sujet = creation_sujet($data[array_search("rubrique",$nom_var)],"",$data[array_search("oeuvre",$nom_var)],$data[array_search("passage",$nom_var)],$themes,$data[array_search("textesource",$nom_var)]);
					$motscles = motscles($sujet);
					
					// On créé le sujet dans la base de données...
					$req = $bdd->prepare('INSERT INTO sujets (sujet,matiere,motscles,annale,type,date_ajout,textesource,oeuvre,passage,themes,edition,rubrique) 
					VALUES (:sujet,:matiere,:motscles,:annale,:type,:date_ajout,:textesource,:oeuvre,:passage,:themes,:edition,:rubrique)');
					$req->execute(array(
						'sujet' => $sujet,
						'matiere' => $data[array_search('matiere',$nom_var)],
						'motscles' => $motscles,
						'annale' => $data[array_search('annale',$nom_var)],
						'type' => 'commentaire',
						'date_ajout' => $date_ajout,
						'textesource' => $data[array_search('textesource',$nom_var)],
						'oeuvre' => $data[array_search('oeuvre',$nom_var)],
						'passage' => $data[array_search('passage',$nom_var)],
						'themes' => $themes,
						'edition' => $data[array_search('edition',$nom_var)],
						'rubrique' => $data[array_search('rubrique',$nom_var)]								
						));
				} else if ($data[array_search('type',$nom_var)]=='dissertation') { 
				
				    $motscles = motscles($data[array_search('sujet',$nom_var)]);
					
					// On créé le sujet dans la base de données...
					$req = $bdd->prepare('INSERT INTO sujets (sujet,matiere,motscles,annale,type,date_ajout,rubrique) 
					VALUES (:sujet,:matiere,:motscles,:annale,:type,:date_ajout,:rubrique)');
					$req->execute(array(
						'sujet' => $data[array_search('sujet',$nom_var)],
						'matiere' => $data[array_search('matiere',$nom_var)],
						'motscles' => $motscles,
						'annale' => $data[array_search('annale',$nom_var)],
						'type' => 'dissertation',
						'date_ajout' => $date_ajout,
						'rubrique' => $data[array_search('rubrique',$nom_var)]								
						));
				    
				}		
					
				echo $sujet_id = $bdd->lastInsertId();		
			}
			
			// 2. Autrement Si sujet_id renseigné on vérifie qu'il correspond à quelque chose 
			elseif ($row!=1) {		
				$reponse = $bdd->query("SELECT * FROM `sujets` WHERE `id`='".$data[array_search("sujet_id",$nom_var)]."'");
				$row_count = $reponse->rowCount();
				
				if ($row_count!=1) { echo "=> ERREUR : id sujet non retrouvé"; }
				else { while ($val = $reponse->fetch()) { $sujet_id=$val["id"]; echo  " => id sujet retrouvé : ".$sujet_id; }}
			}
			
			// 3. Si c'est ok on créé ressource avec URL et autres infos au sujet_id renseigné
			if (isset($sujet_id)) {
				$etat=2;
				$date_ajout= time();
				
				$req = $bdd->prepare("INSERT INTO `ressources` (sujet_id,url,date_ajout,auteur,niveau_auteur,etat) 
				VALUES (:sujet_id,:url,:date_ajout,:auteur,:niveau_auteur,:etat)");
				$req->execute(array(
					'sujet_id' => $sujet_id,
					'url' => $data[array_search('url',$nom_var)],
					'date_ajout' => $date_ajout,
					'auteur' => $data[array_search('auteur',$nom_var)],
					'niveau_auteur' => $data[array_search('niveau_auteur',$nom_var)],
					'etat' => $etat
					));
					
				// ... et enfin on récupère l'id de la ressource 
				echo " + ressource créée : ".$ressource_id = $bdd->lastInsertId();			
			}
		}
	$row++;
	echo "</p>";	
	}
	fclose($handle);
}
?>