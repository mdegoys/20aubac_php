﻿<?php
// Liste tous les sujets vides (dissertations sans ressource)

include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');

$bdd = getBdd($domaine,'site');

// Liste tous les sujets vides (dissertations sans ressource)
$reponse0 = $bdd ->query('SELECT * FROM `sujets` WHERE `ressources`=\'\' AND `ressources_ext`=\'\' AND `type`=\'dissertation\' AND `topic_id`!=\'0\'');
$count0 = $reponse0->rowCount();

echo '<p>Il y a '.$count0.' sujets sans ressources au total :<br /><br />';
$i = 0;

while ($val0 = $reponse0->fetch()) {
	$sujet_id = $val0['id'];
	$sujet = $val0['sujet'];

  $i++;
  echo $i.'. '.$sujet. ': sujet vide<br />';

  $bdd->exec("DELETE FROM `sujets` WHERE id='".$sujet_id."'");
}
?>
