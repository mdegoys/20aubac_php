﻿<?php
// Réécrire les topics d'aide pour éviter les problèmes d'accents et avoir les quotes pour les textes commentés

include ('../config.php');
include (CHEMIN_SCRIPT . '/_modele/modele.php');
include (CHEMIN_SCRIPT . '/_modele/modele_ressources.php');

$user->session_begin();
$auth->acl($user->data);
$user->setup();	

$bdd = getBdd($domaine,'site');

$reponse1 = $bdd ->query('SELECT * FROM `sujets` WHERE `topic_id`!=\'\'');

$count1 = $reponse1->rowCount();
$i = 0;

if ($count1>0) {
	echo 'Il y a '.$count1.' sujets avec un topic d\'aide :<br /><br />';
	while ($val = $reponse1 ->fetch()) { 
		
		$id = $val['id'];
		$type = $val['type'];
		$matiere = $val['site'];
		$rubrique = $val['rubrique'];
		$sujet = $val['sujet'];
		$motscles = $val['motscles'];
		$ressources = unserialize($val['ressources']);
		$ressources_ext = unserialize($val['ressources_ext']);
		$topic_id = $val['topic_id'];
		$textesource = $val['textesource'];
		$oeuvre = $val['oeuvre'];
		$passage = $val['passage'];
		$edition = $val['edition'];
		echo $id.'.'.$sujet.'<br />';
		
		if ($type=='commentaire') {			
			post_topicaide ($id,$type,$rubrique,$sujet,$motscles,$ressources,$ressources_ext,'edit',$topic_id,$textesource,$oeuvre,$passage,$edition);
		} 
		elseif ($type=='dissertation') {
			post_topicaide ($id,$type,$rubrique,$sujet,$motscles,$ressources,$ressources_ext,'edit',$topic_id);
		}
		
		echo ' -> topic modifié : '.$topic_id.'<br /><br />';
					
	}
} else {
	echo 'Pas de sujets trouvés<br /><br />';
}	
?>